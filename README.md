# Hydrant
 High performance Minecraft server

# Why Hydrant?
Hydrant is completely self written and not based on the minecraft server jar.
Hydrant has a big developer API.

# Downloads (Server)
Pre-compiled builds can be found here:

* Jenkins: https://ci.jumpy91.de/job/Hydrant/

# Downloads (API) #
You can use maven to include our API:

```
<repositories>
  <repository>
    <id>jumpys-repo</id>
    <url>https://repo.jumpy91.de/repository/snapshots/</url>
  </repository>
</repositories>
```
```
<dependency>
  <groupId>org.hydrantmc</groupId>
  <artifactId>hydrantmc-api</artifactId>
  <version>1.8.9-R0.2-SNAPSHOT</version>
</dependency>
```