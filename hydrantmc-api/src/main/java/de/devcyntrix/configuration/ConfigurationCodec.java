package de.devcyntrix.configuration;

import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Map;

public class ConfigurationCodec implements JsonSerializer<Configuration>, JsonDeserializer<Configuration> {

    @Override
    public Configuration deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
        if (!json.isJsonObject())
            return null;
        context.deserialize(json, Map.class);

        return null;
    }

    @Override
    public JsonElement serialize(Configuration src, Type typeOfSrc, JsonSerializationContext context) {
        return null;
    }
}
