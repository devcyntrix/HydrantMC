package de.devcyntrix.configuration;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public abstract class ConfigurationProvider {

    private static final Map<Class<? extends ConfigurationProvider>, ConfigurationProvider> providers = new HashMap<>();

    static {
        providers.put(YamlConfiguration.class, new YamlConfiguration());
        providers.put(JsonConfiguration.class, new JsonConfiguration());
        providers.put(PropertiesConfiguration.class, new PropertiesConfiguration());
    }

    public static <T extends ConfigurationProvider> T getProvider(Class<T> provider) {
        return (T) providers.get(provider);
    }

    /*------------------------------------------------------------------------*/
    public abstract void save(Configuration config, Writer writer);

    public void save(Configuration config, File file) throws IOException {
        save(config, new FileWriter(file));
    }

    public void save(Configuration config, OutputStream stream) {
        save(config, new OutputStreamWriter(stream));
    }

    public Configuration load(Reader reader) {
        return load(reader, null);
    }

    public abstract Configuration load(Reader reader, Configuration defaults);

    public Configuration load(File file) throws IOException {
        return load(new FileReader(file));
    }

    public Configuration load(File file, Configuration defaults) throws IOException {
        return load(new FileReader(file), defaults);
    }

    public Configuration load(InputStream is) {
        return load(new InputStreamReader(is));
    }

    public Configuration load(InputStream is, Configuration defaults) {
        return load(is, defaults);
    }

    public Configuration load(String string) {
        return load(new StringReader(string));
    }

    public Configuration load(String string, Configuration defaults) {
        return load(new StringReader(string), defaults);
    }

}
