package de.devcyntrix.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import de.devcyntrix.configuration.adapter.ConfigurationSerializer;
import de.devcyntrix.configuration.adapter.FileAdapter;
import de.devcyntrix.configuration.adapter.InetSocketAddressAdapter;
import de.devcyntrix.configuration.adapter.UUIDAdapter;

import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.net.InetSocketAddress;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.UUID;

public class JsonConfiguration extends ConfigurationProvider {

    private final Gson gson = new GsonBuilder()
            .setPrettyPrinting()
            .registerTypeHierarchyAdapter(InetSocketAddress.class, new InetSocketAddressAdapter())
            .registerTypeHierarchyAdapter(File.class, new FileAdapter())
            .registerTypeHierarchyAdapter(UUID.class, new UUIDAdapter())
            .registerTypeHierarchyAdapter(Configuration.class, new ConfigurationSerializer())
            .create();

    @Override
    public void save(Configuration config, Writer writer) {
        gson.toJson(config.self, writer);
        try {
            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Configuration load(Reader reader, Configuration defaults) {
        Map<String, Object> map = gson.fromJson(reader, LinkedHashMap.class);
        if (map == null) {
            map = new LinkedHashMap<>();
        }
        return new Configuration(map, defaults);
    }
}
