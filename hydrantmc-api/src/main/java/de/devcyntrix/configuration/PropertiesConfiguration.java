package de.devcyntrix.configuration;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Properties;
import java.util.function.BiConsumer;

public class PropertiesConfiguration extends ConfigurationProvider {

    private void readData(String keyNameStart, Configuration configuration, BiConsumer<String, Object> consumer) {
        for (Map.Entry<String, Object> entry : configuration.getSelf().entrySet()) {
            if (entry.getValue() instanceof Configuration) {
                readData(entry.getKey() + ".", (Configuration) entry.getValue(), consumer);
                return;
            }
            consumer.accept(keyNameStart + entry.getKey(), entry.getValue());
        }
    }

    @Override
    public void save(Configuration config, Writer writer) {
        Properties properties = new Properties();
        readData("", config, (s, o) -> properties.put(s, o.toString()));

        try {
            properties.store(writer, null);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Configuration load(Reader reader, Configuration defaults) {
        Properties properties = new Properties();
        try {
            properties.load(reader);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Map<String, Object> map = new LinkedHashMap<>(properties.size());
        properties.forEach((key, value) -> map.put(key.toString(), value));
        return new Configuration(map, defaults);
    }
}
