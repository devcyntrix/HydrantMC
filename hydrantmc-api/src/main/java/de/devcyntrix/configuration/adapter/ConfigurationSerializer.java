package de.devcyntrix.configuration.adapter;

import com.google.gson.JsonElement;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import de.devcyntrix.configuration.Configuration;

import java.lang.reflect.Type;
import java.util.Map;

public class ConfigurationSerializer implements JsonSerializer<Configuration> {
    @Override
    public JsonElement serialize(Configuration src, Type typeOfSrc, JsonSerializationContext context) {
        return context.serialize(src.getSelf(), Map.class);
    }
}
