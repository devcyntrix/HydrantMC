package de.devcyntrix.configuration.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.net.InetSocketAddress;

public class InetSocketAddressAdapter extends TypeAdapter<InetSocketAddress> {

    @Override
    public void write(JsonWriter out, InetSocketAddress value) throws IOException {
        out.value(value == null ? null : value.getAddress().getHostAddress() + ":" + value.getPort());
    }

    @Override
    public InetSocketAddress read(JsonReader in) throws IOException {
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        }
        String address = in.nextString();

        String hostname = address.substring(0, address.lastIndexOf(':'));
        String port = address.substring(address.lastIndexOf(':') + 1);

        return new InetSocketAddress(hostname, Integer.parseInt(port));
    }
}
