package de.devcyntrix.configuration.annotation;

import com.google.common.io.Files;
import com.google.gson.*;
import de.devcyntrix.configuration.adapter.FileAdapter;
import de.devcyntrix.configuration.adapter.InetSocketAddressAdapter;
import de.devcyntrix.configuration.adapter.UUIDAdapter;

import java.io.*;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.function.BiConsumer;

public abstract class AbstractClassConfiguration implements ConfigurationSerializable {

    boolean missingValues = false;
    private Gson gson;

    public AbstractClassConfiguration() {
        this.gson = new GsonBuilder().setPrettyPrinting()
                .registerTypeAdapter(InetSocketAddress.class, new InetSocketAddressAdapter())
                .registerTypeAdapter(File.class, new FileAdapter())
                .registerTypeAdapter(UUID.class, new UUIDAdapter())
                .create();
    }

    public AbstractClassConfiguration(Gson gson) {
        this.gson = gson;
    }

    public static void walkPropertyFields(Field[] fields, BiConsumer<Field, ConfigurationProperty> consumer) {
        for (Field field : fields) {

            /* Gives access to the field */
            field.setAccessible(true);
            /* Skips field without annotation */
            if (!field.isAnnotationPresent(ConfigurationProperty.class))
                continue;
            ConfigurationProperty property = field.getDeclaredAnnotation(ConfigurationProperty.class);
            consumer.accept(field, property);
        }
    }

    /**
     * Loads the configuration file and needs the {@link ConfigurationInfo}
     */
    public void loadFromInfo() {
        ConfigurationInfo info = checkConfigurationInfo();
        File file = new File(info.filename());
        loadFromFile(file);
    }

    public synchronized void loadFromFile(File file) {
        if (!file.isFile()) {
            saveDefaults(file);
            return;
        }

        try {
            JsonObject fileObject = gson.fromJson(new FileReader(file), JsonObject.class);
            List<String> missing = loadToObject(fileObject, this);

            if (!missing.isEmpty()) {
                System.out.println("Missing fields in \"" + file + "\": " + missing);
                save(file);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public <T> T parse(JsonElement element, Class<T> typeClass) {

        if (typeClass.isArray() && ConfigurationSerializable.class.isAssignableFrom(typeClass.getComponentType())) {
            JsonArray array = element.getAsJsonArray();
            Object o = Array.newInstance(typeClass.getComponentType(), array.size());
            for (int i = 0; i < array.size(); i++) {
                Array.set(o, i, parse(array.get(i), typeClass.getComponentType()));
            }
            return (T) o;
        }

        if (ConfigurationSerializable.class.isAssignableFrom(typeClass)) {
            if (!element.isJsonObject())
                throw new Error("ConfigurationSerializable element must be a json object!");
            JsonObject object = element.getAsJsonObject();

            Constructor<? extends T> constructor;
            try {
                constructor = typeClass.getConstructor();
            } catch (NoSuchMethodException e) {
                throw new Error("ConfigurationSerializable class needs an empty constructor! In class " + typeClass.getCanonicalName());
            }

            try {
                T value = constructor.newInstance();

                boolean missingValues = !loadToObject(object, value).isEmpty();
                if (missingValues) {
                    return null;
                }
                return value;
            } catch (IllegalAccessException | InstantiationException | InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }
        return gson.fromJson(element, typeClass);
    }

    public synchronized List<String> loadToObject(JsonObject jsonObject, Object object) {
        Field[] fields = object.getClass().getDeclaredFields();

        ArrayList<String> missingFields = new ArrayList<>();

        walkPropertyFields(fields, (field, property) -> {
            JsonElement element = jsonObject.get(property.name());
            if (element == null || element.isJsonNull()) {
                if (!property.optional()) {
                    missingFields.add(property.name());
                }
                return;
            }

            Object fieldValue = parse(element, field.getType());
            if(fieldValue == null) {
                missingFields.add(property.name());
                return;
            }

            try {
                field.set(object, fieldValue);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        });
        return missingFields;
    }

    public Set<String> getProperties() {
        Field[] fields = getClass().getDeclaredFields();
        Set<String> set = new HashSet<>();
        for (Field field : fields) {
            field.setAccessible(true);
            if (!field.isAnnotationPresent(ConfigurationProperty.class))
                continue;
            ConfigurationProperty property = field.getAnnotation(ConfigurationProperty.class);
            set.add(property.name());
        }
        return set;
    }

    public JsonElement convert(Object object) {

        if (object instanceof ConfigurationSerializable) {

            JsonObject jsonObject = new JsonObject();

            Class<?> clazz = object.getClass();
            Field[] fields = clazz.getDeclaredFields();

            walkPropertyFields(fields, (field, property) -> {
                try {
                    jsonObject.add(property.name(), convert(field.get(object)));
                } catch (IllegalAccessException e) {
                    e.printStackTrace();
                }
            });

            return jsonObject;
        }

        if (object instanceof ConfigurationSerializable[]) {
            JsonArray array = new JsonArray();

            ConfigurationSerializable[] serializables = (ConfigurationSerializable[]) object;
            for (ConfigurationSerializable serializable : serializables) {
                array.add(convert(serializable));
            }

            return array;
        }

        return gson.toJsonTree(object);
    }

    private void saveDefaults(File file) {
        try {
            Files.createParentDirs(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        JsonObject object = convert(this).getAsJsonObject();

        try {
            PrintWriter writer = new PrintWriter(file);
            gson.toJson(object, writer);
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Saves the configuration file.
     */
    public synchronized void save() {
        ConfigurationInfo info = checkConfigurationInfo();

        File file = new File(info.filename());
        if (!file.isFile()) {
            saveDefaults(file);
            return;
        }

        save(file);
    }

    public synchronized void save(File file) {
        try {
            JsonObject object = convert(this).getAsJsonObject();

            PrintWriter writer = new PrintWriter(file);
            gson.toJson(object, writer);
            writer.flush();
            writer.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    private ConfigurationInfo checkConfigurationInfo() {
        ConfigurationInfo info = getClass().getAnnotation(ConfigurationInfo.class);
        if (info == null) {
            throw new NullPointerException("The configuration info is missing.");
        }
        return info;
    }

}
