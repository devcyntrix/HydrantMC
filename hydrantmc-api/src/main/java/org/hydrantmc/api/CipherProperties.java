package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;

@Data
public class CipherProperties implements ConfigurationSerializable {

    @ConfigurationProperty(name = "enabled")
    private boolean enabled = true;
    @ConfigurationProperty(name = "keySize")
    private int keySize = 512;

}
