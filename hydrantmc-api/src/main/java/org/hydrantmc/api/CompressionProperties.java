package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;

@Data
public class CompressionProperties implements ConfigurationSerializable {

    @ConfigurationProperty(name = "threshold")
    private int threshold = 256;

}
