package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;

@Data
public class KickMessages implements ConfigurationSerializable {

    @ConfigurationProperty(name = "outdatedClient")
    private String outdatedClientKickMessage = "Outdated Client. Please use 1.8.8";
    @ConfigurationProperty(name = "outdatedServer")
    private String outdatedServerKickMessage = "Outdated Server. Please use 1.8.8";
    @ConfigurationProperty(name = "serverIsFull")
    private String serverIsFullKickMessage = "Server is full.";
    @ConfigurationProperty(name = "nameAlreadyInUse")
    private String nameAlreadyInUseKickMessage = "Your username is already in use.";

}