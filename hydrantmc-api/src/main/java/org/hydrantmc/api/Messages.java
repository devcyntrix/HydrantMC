package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;

@Data
public class Messages implements ConfigurationSerializable {

    @ConfigurationProperty(name = "unknownCommand")
    private String unknownCommandMessage = "§5Hydrant §7» §cUnknown command.";
    @ConfigurationProperty(name = "noPermission")
    private String noPermissionMessage = "§5Hydrant §7» §cYou don't have permission to execute this command.";

}

