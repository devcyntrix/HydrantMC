package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import io.netty.channel.epoll.Epoll;
import lombok.Data;

import java.net.InetSocketAddress;

@Data
public class NetworkProperties implements ConfigurationSerializable {

    @ConfigurationProperty(name = "address")
    private InetSocketAddress address = new InetSocketAddress(25565);

    @ConfigurationProperty(name = "maxAcceptorThreads")
    private int maxAcceptorThreads = 1;
    @ConfigurationProperty(name = "maxWorkerThreads")
    private int maxWorkerThreads = 4;
    @ConfigurationProperty(name = "allowNativeTransport")
    private boolean allowNativeTransport = Epoll.isAvailable();

    @ConfigurationProperty(name = "compression")
    private CompressionProperties compressionProperties = new CompressionProperties();

    @ConfigurationProperty(name = "cipher")
    private CipherProperties cipherProperties = new CipherProperties();


}
