package org.hydrantmc.api;

import org.hydrantmc.api.block.BlockRegistry;
import org.hydrantmc.api.command.CommandRegistry;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.listener.ListenerRegistry;
import org.hydrantmc.api.plugin.PluginManager;
import org.hydrantmc.api.scoreboard.ScoreboardManager;
import org.hydrantmc.api.tablist.AbstractTabList;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.status.ServerStatus;
import org.hydrantmc.api.world.World;

import java.io.File;
import java.security.KeyPair;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;

public interface Server {

    /**
     * Gets the version string of this server implementation.
     *
     * @return version of this server implementation
     */
    String getVersion();

    /**
     * Gets the protocol string of this server.
     *
     * @return protocol version of this server
     */
    int getProtocolVersion();

    /**
     * Returns the primary logger associated with this server instance.
     *
     * @return Logger associated with this server
     */
    Logger getLogger();

    /**
     * Shutdowns the server, stopping everything.
     */
    void shutdown();

    /**
     * Shutdowns the server, stopping everything.
     *
     * @param exit exit code to stop the process with
     */
    void shutdown(int exit);

    File getPluginsFolder();
    /**
     * For the encryption
     */
    KeyPair getKeyPair();

    /**
     * Gets whether the Server is in online mode or not.
     *
     * @return true if the server authenticates clients, false otherwise
     */
    boolean isOnlineMode();

    ServerConfiguration getConfiguration();

    NetworkProperties getNetworkProperties();

    /**
     * Get the maximum amount of players which can login to this server.
     *
     * @return the amount of players this server allows
     */
    int getMaxPlayers();

    /**
     * Gets a ConsoleCommandSender that may be used as an input source
     * for this server.
     *
     * @return a console command sender
     */
    CommandSender getConsoleCommandSender();

    ServerStatus getServerStatus();

    /**
     * Gets the world with the given name.
     *
     * @param name the name of the world to retrieve
     * @return a world with the given name, or null if none exists
     */
    World getWorld(String name);


    /**
     * Gets a map of all worlds on this server.
     *
     * @return a map of worlds
     */
    Map<String, World> getWorlds();

    Location getSpawnLocation();

    void setSpawnLocation(Location location);

    void addPlayer(Player player);

    /**
     * Gets a player object by the given username.
     *
     * @param username the name to look up
     * @return a player if one was found, null otherwise
     */
    Player getPlayer(String username);

    /**
     * Gets the player with the given UUID.
     *
     * @param uniqueId UUID of the player to retrieve
     * @return a player object if one was found, null otherwise
     */
    Player getPlayer(UUID uniqueId);

    /**
     * Gets a view of all currently logged in players.
     *
     * @return a view of currently online players.
     */
    Collection<Player> getOnlinePlayers();

    CommandRegistry getCommandRegistry();

    ListenerRegistry getListenerRegistry();

    /**
     * Gets the plugin manager for interfacing with plugins.
     *
     * @return a plugin manager for this Server instance
     */
    PluginManager getPluginManager();

    BlockRegistry getBlockRegistry();

    /**
     * Gets the instance of the scoreboard manager.
     *
     * @return the scoreboard manager
     */
    ScoreboardManager getScoreboardManager();

    AbstractTabList getTabList();

    void broadcast(Predicate<Player> predicate, Consumer<Player> consumer);

    void broadcast(Consumer<Player> consumer);

    void reload();

}
