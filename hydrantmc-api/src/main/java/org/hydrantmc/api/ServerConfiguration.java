package org.hydrantmc.api;

import org.hydrantmc.api.utils.player.GameMode;
import org.hydrantmc.api.world.Difficulty;

public interface ServerConfiguration {

    ServerInfoProperties getServerInfoProperties();

    NetworkProperties getNetworkProperties();

    GameMode getDefaultGameMode();

    KickMessages getKickMessages();

    Messages getMessages();

    TabListProperties getTabListProperties();

}
