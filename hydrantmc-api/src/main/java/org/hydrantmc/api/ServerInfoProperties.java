package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;

@Data
public class ServerInfoProperties implements ConfigurationSerializable {

    @ConfigurationProperty(name = "max-players")
    private int maxPlayers = 20;

    @ConfigurationProperty(name = "motd")
    private String[] motd = {"A Minecraft Server", "§7Powered by §5Hydrant"};

    @ConfigurationProperty(name = "protocolName")
    private String protocolName = "Hydrant 1.8.9";
    @ConfigurationProperty(name = "protocolId")
    private int protocolId = 47;

    @ConfigurationProperty(name = "playerInfo")
    private String[] playerInfo = {};
}
