package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;

@Data
public class TabListProperties implements ConfigurationSerializable {

    @ConfigurationProperty(name = "size")
    private int size = 60;
    @ConfigurationProperty(name = "header")
    private String[] header = {};
    @ConfigurationProperty(name = "footer")
    private String[] footer = {};

}
