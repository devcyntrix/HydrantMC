package org.hydrantmc.api;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;
import org.hydrantmc.api.world.Difficulty;
import org.hydrantmc.api.world.TerrainType;
import org.hydrantmc.api.world.WorldType;

@Data
public class WorldProperties implements ConfigurationSerializable {

    @ConfigurationProperty(name = "name")
    private String name;
    @ConfigurationProperty(name = "worldType")
    private WorldType worldType;
    @ConfigurationProperty(name = "seed")
    private long seed;
    @ConfigurationProperty(name = "terrainType")
    private TerrainType terrainType;
    @ConfigurationProperty(name = "difficulty")
    private Difficulty difficulty;
}
