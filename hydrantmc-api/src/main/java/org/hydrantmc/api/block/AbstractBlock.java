package org.hydrantmc.api.block;

public class AbstractBlock {

    private final byte blockId;
    private final byte metaId;

    public AbstractBlock(byte blockId) {
        this(blockId, (byte) 0);
    }

    public AbstractBlock(byte blockId, byte metaId) {
        this.blockId = blockId;
        this.metaId = metaId;
    }

    public byte getBlockId() {
        return blockId;
    }

    public byte getMetaId() {
        return metaId;
    }
}
