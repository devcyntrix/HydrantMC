package org.hydrantmc.api.block;

import java.util.LinkedHashMap;
import java.util.Map;

public class BlockRegistry {

    private final Map<Integer, AbstractBlock> knownBlocks;

    public BlockRegistry() {
        this.knownBlocks = new LinkedHashMap<>();
    }

    public void registerBlock(int id, AbstractBlock block) {
        if(!this.knownBlocks.containsKey(id)) this.knownBlocks.put(id, block);
    }

    public AbstractBlock getBlock(int id) {
        return this.knownBlocks.get(id);
    }

    public boolean unregisterBlock(int id) {
        return this.knownBlocks.remove(id) != null;
    }
}
