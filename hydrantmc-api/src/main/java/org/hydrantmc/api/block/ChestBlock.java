package org.hydrantmc.api.block;

public class ChestBlock extends AbstractBlock {

    private int playersViewingInventory;

    public ChestBlock() {
        super((byte) 54);
    }

    public int getPlayersViewingInventory() {
        return playersViewingInventory;
    }

    public void setPlayersViewingInventory(int playersViewingInventory) {
        this.playersViewingInventory = playersViewingInventory;
    }

}
