package org.hydrantmc.api.chat;

import com.google.common.base.Preconditions;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public enum ChatColor {

    BLACK('0', "black", 0, 0, 0),
    DARK_BLUE('1', "dark_blue", 0, 0, 170),
    DARK_GREEN('2', "dark_green", 0, 170, 0),
    DARK_AQUA('3', "dark_aqua", 0, 170, 170),
    DARK_RED('4', "dark_red", 170, 0, 0),
    DARK_PURPLE('5', "dark_purple", 170, 0, 170),
    GOLD('6', "gold", 255, 170, 0),
    GRAY('7', "gray", 170, 170, 170),
    DARK_GRAY('8', "dark_gray", 85, 85, 85),
    BLUE('9', "blue", 85, 85, 255),
    GREEN('a', "green", 85, 255, 85),
    AQUA('b', "aqua", 85, 255, 255),
    RED('c', "red", 255, 85, 85),
    LIGHT_PURPLE('d', "light_purple", 255, 85, 85),
    YELLOW('e', "yellow", 255, 255, 85),
    WHITE('f', "white", 255, 255, 255);

    public static final char COLOR_CHAR = '§';
    public static final String ALL_CODES = "1234567890AaBbCcDdEeFf";
    public static final Map<Character, ChatColor> COLOR_BY_CODE = new HashMap<>();
    public static final Pattern STRIP_COLOR_PATTERN = Pattern.compile("(?i)" + COLOR_CHAR + "[0-9A-FK-OR]");

    static {
        for(ChatColor chatColor : values()) {
            COLOR_BY_CODE.put(chatColor.code, chatColor);
        }
    }

    private final char code;
    private final String name;
    private final String toString;

    private final int red;
    private final int green;
    private final int blue;

    ChatColor(Character code, String name, int red, int green, int blue) {
        this.code = code;
        this.name = name;
        this.toString = String.valueOf( new char[] { COLOR_CHAR, code });
        this.red = red;
        this.green = green;
        this.blue = blue;
    }

    public char getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return this.toString;
    }

    public int getRed() {
        return red;
    }

    public int getGreen() {
        return green;
    }

    public int getBlue() {
        return blue;
    }

    public Color asColor() {
        return new Color(this.red, this.green, this.blue);
    }

    public static boolean isColorCode(char code) {
        return ALL_CODES.indexOf(code) != -1;
    }

    public static ChatColor getColorByCode(char code) {
        return COLOR_BY_CODE.get(code);
    }

    public static String stripColor(String input) {
        Preconditions.checkNotNull(input);
        return STRIP_COLOR_PATTERN.matcher(input).replaceAll("");
    }

    public static String stripColorAndFormat(String input) {
        Preconditions.checkNotNull(input);
        return ChatFormat.stripFormat(stripColor(input));
    }

    public static String translateAlternateColorCodes(char altColorChar, String textToTranslate) {
        char[] chain = textToTranslate.toCharArray();
        for (int index = 0; index < chain.length - 1; index++) {
            if (chain[index] == altColorChar && ALL_CODES.indexOf(chain[index + 1] ) > -1) {
                chain[index] = ChatColor.COLOR_CHAR;
                chain[index + 1] = Character.toLowerCase(chain[index + 1]);
            }
        }
        return new String(chain);
    }
}
