package org.hydrantmc.api.chat;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import org.hydrantmc.api.chat.event.ClickEvent;
import org.hydrantmc.api.chat.event.HoverEvent;

import java.util.LinkedList;
import java.util.List;

public abstract class ChatComponent {

    private static final ChatColor DEFAULT_COLOR = ChatColor.WHITE;
    private ChatComponent parent;

    private ChatColor color;

    private Boolean bold;
    private Boolean italic;
    private Boolean underlined;
    private Boolean strikeThrough;
    private Boolean obfuscated;

    private ClickEvent clickEvent;
    private HoverEvent hoverEvent;

    private String insertion;

    private LinkedList<ChatComponent> extra;

    public ChatComponent() {
        this.extra = new LinkedList<>();
    }

    public ChatComponent getParent() {
        return this.parent;
    }

    public boolean hasParent() {
        return parent != null;
    }

    public void setParent(ChatComponent parent) {
        this.parent = parent;
    }

    public ChatColor getColor() {
        if (this.parent == null && this.color == null) return DEFAULT_COLOR;
        if(this.color == null) {
            return this.parent.getColor();
        }
        return color;
    }

    public boolean hasColor() {
        return getColor() != null;
    }

    public ChatColor getRawColor() {
        return this.color;
    }

    public void setColor(ChatColor color) {
        this.color = color;
    }

    public boolean isBold() {
        if (this.parent == null && this.bold == null) return false;
        if(this.bold == null) return this.parent.isBold();
        return bold;
    }

    public void setBold(boolean bold) {
        this.bold = bold;
    }

    public boolean isItalic() {
        if (this.parent == null && this.italic == null) return false;
        if(this.italic == null) return this.parent.isItalic();
        return italic;
    }

    public void setItalic(boolean italic) {
        this.italic = italic;
    }

    public boolean isUnderlined() {
        if (this.parent == null && this.underlined == null) return false;
        if(this.underlined == null) return this.parent.isUnderlined();
        return underlined;
    }

    public void setUnderlined(boolean underlined) {
        this.underlined = underlined;
    }

    public boolean isStrikeThrough() {
        if (this.parent == null && this.strikeThrough == null) return false;
        if(this.strikeThrough == null) return this.parent.isStrikeThrough();
        return strikeThrough;
    }

    public void setStrikeThrough(boolean strikeThrough) {
        this.strikeThrough = strikeThrough;
    }

    public boolean isObfuscated() {
        if (this.parent == null && this.obfuscated == null) return false;
        if(this.obfuscated == null) return this.parent.isObfuscated();
        return obfuscated;
    }

    public void setObfuscated(boolean obfuscated) {
        this.obfuscated = obfuscated;
    }

    public ClickEvent getClickEvent() {
        return clickEvent;
    }

    public void setClickEvent(ClickEvent clickEvent) {
        this.clickEvent = clickEvent;
    }

    public HoverEvent getHoverEvent() {
        return hoverEvent;
    }

    public void setHoverEvent(HoverEvent hoverEvent) {
        this.hoverEvent = hoverEvent;
    }

    public Iterable<ChatComponent> getExtra() {
        return extra;
    }

    public void addExtra(ChatComponent baseComponent) {
        if(baseComponent.hasParent()) {
            try {
                throw new DoubleParentException("The BaseComponent can only have one parent. (" + baseComponent + ")");
            } catch (DoubleParentException e) {
                e.printStackTrace();
            }
            return;
        }
        if(this.extra.isEmpty()) baseComponent.setParent(this); else baseComponent.setParent(this.extra.getLast());
        extra.add(baseComponent);
    }

    public void setExtra(List<ChatComponent> extra) {
        ChatComponent last = null;

        for(ChatComponent component : extra) {
            if(component.hasParent()) {
                try {
                    throw new DoubleParentException("The BaseComponent can only have one parent. (" + component + ")");
                } catch (DoubleParentException e) {
                    e.printStackTrace();
                }
                extra.remove(component);
                continue;
            }
            if(last == null) component.setParent(this); else component.setParent(last);
            last = component;
        }
        this.extra = new LinkedList<>(extra);
    }

    public String getInsertion() {
        return insertion;
    }

    public void setInsertion(String insertion) {
        this.insertion = insertion;
    }

    public void reset() {
        this.color = DEFAULT_COLOR;
        this.bold = null;
        this.italic = null;
        this.underlined = null;
        this.strikeThrough = null;
        this.obfuscated = null;
        this.insertion = null;
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("parent", parent)
                .add("color", color)
                .add("bold", bold)
                .add("italic", italic)
                .add("underlined", underlined)
                .add("strikeThrough", strikeThrough)
                .add("obfuscated", obfuscated)
                .add("clickEvent", clickEvent)
                .add("hoverEvent", hoverEvent)
                .add("extra", extra)
                .toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatComponent that = (ChatComponent) o;
        return Objects.equal(parent, that.parent) &&
                color == that.color &&
                Objects.equal(bold, that.bold) &&
                Objects.equal(italic, that.italic) &&
                Objects.equal(underlined, that.underlined) &&
                Objects.equal(strikeThrough, that.strikeThrough) &&
                Objects.equal(obfuscated, that.obfuscated) &&
                Objects.equal(clickEvent, that.clickEvent) &&
                Objects.equal(hoverEvent, that.hoverEvent) &&
                Objects.equal(extra, that.extra);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(parent, color, bold, italic, underlined, strikeThrough, obfuscated, clickEvent, hoverEvent, extra);
    }
}
