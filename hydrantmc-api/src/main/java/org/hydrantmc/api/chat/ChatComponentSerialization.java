package org.hydrantmc.api.chat;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.hydrantmc.api.chat.event.ClickEvent;
import org.hydrantmc.api.chat.event.HoverEvent;
import org.hydrantmc.api.utils.serializer.ChatComponentSerializer;
import org.hydrantmc.api.utils.serializer.ClickEventSerializer;
import org.hydrantmc.api.utils.serializer.HoverEventSerializer;

public class ChatComponentSerialization {

    private static final Gson gson;

    static {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeHierarchyAdapter(ChatComponent.class, new ChatComponentSerializer());
        builder.registerTypeHierarchyAdapter(ClickEvent.class, new ClickEventSerializer());
        builder.registerTypeHierarchyAdapter(HoverEvent.class, new HoverEventSerializer());
        gson = builder.create();
    }

    public static Gson getGson() {
        return gson;
    }


}
