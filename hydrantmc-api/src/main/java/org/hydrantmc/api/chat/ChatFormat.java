package org.hydrantmc.api.chat;

import com.google.common.base.Preconditions;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public enum ChatFormat {

    OBFUSCATED('k', "obfuscated"),
    BOLD('l', "bold"),
    STRIKETHROUGH('m', "strikethrough"),
    UNDERLINE('n', "underline"),
    ITALIC('o', "italic"),

    RESET('r', "reset");

    private static final char FORMAT_CHAR = '§';
    public static final String ALL_FORMATS = "KkLlMmNnOoRr";
    public static final Map<Character, ChatFormat> FORMAT_BY_CODE = new HashMap<>();
    public static final Pattern STRIP_COLOR_PATTERN = Pattern.compile( "(?i)" + String.valueOf(FORMAT_CHAR) + "[K-OR]" );

    static {
        for(ChatFormat chatFormat : values()) {
            FORMAT_BY_CODE.put(chatFormat.code, chatFormat);
        }
    }

    private final char code;
    private final String name;
    private final String toString;

    ChatFormat(char code, String name) {
        this.code = code;
        this.name = name;
        this.toString = new String(new char[] { FORMAT_CHAR, code });
    }

    public char getCode() {
        return this.code;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public String toString() {
        return this.toString;
    }

    public static boolean isFormat(char code) {
        return ALL_FORMATS.indexOf(code) != -1;
    }

    public static ChatFormat getFormatByCode(char code) {
        return FORMAT_BY_CODE.get(code);
    }

    public static String stripFormat(String input) {
        if(input == null) return null;
        return STRIP_COLOR_PATTERN.matcher(input).replaceAll("");
    }

    public static String stripColorAndFormat(String input) {
        Preconditions.checkNotNull(input);
        return stripFormat(ChatColor.stripColor(input));
    }

    public static String translateAlternatsFormatCodes(char altFormatChar, String textToTranslate) {
        char[] cs = textToTranslate.toCharArray();
        for (int index = 0; index < cs.length - 1; index++) {
            if (cs[index] == altFormatChar && ALL_FORMATS.indexOf(cs[index + 1] ) > -1) {
                cs[index] = ChatFormat.FORMAT_CHAR;
                cs[index + 1] = Character.toLowerCase(cs[index + 1]);
            }
        }
        return new String(cs);
    }
}
