package org.hydrantmc.api.chat;

/** @see "http://wiki.vg/Chat#Processing_chat" **/
public enum ChatMessageType {

    CHAT,
    SYSTEM,
    ACTION_BAR

}
