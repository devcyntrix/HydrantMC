package org.hydrantmc.api.chat;

public class DoubleParentException extends Exception {

    public DoubleParentException() {
    }

    public DoubleParentException(String message) {
        super(message);
    }

    public DoubleParentException(String message, Throwable cause) {
        super(message, cause);
    }

    public DoubleParentException(Throwable cause) {
        super(cause);
    }
}
