package org.hydrantmc.api.chat;

public class ScoreComponent extends ChatComponent {

    private String name;
    private String objective;
    private String value;

    public ScoreComponent(String name, String objective) {
        this.name = name;
        this.objective = objective;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getObjective() {
        return objective;
    }

    public void setObjective(String objective) {
        this.objective = objective;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
