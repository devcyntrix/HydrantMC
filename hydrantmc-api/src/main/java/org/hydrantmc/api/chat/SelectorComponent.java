package org.hydrantmc.api.chat;

public class SelectorComponent extends ChatComponent {

    private String selector;

    public SelectorComponent(String selector) {
        this.selector = selector;
    }

    public String getSelector() {
        return selector;
    }

    public void setSelector(String selector) {
        this.selector = selector;
    }

}
