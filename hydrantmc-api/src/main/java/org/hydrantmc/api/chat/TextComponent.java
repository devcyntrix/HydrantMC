package org.hydrantmc.api.chat;

import java.util.LinkedList;

public class TextComponent extends ChatComponent {

    private String text;

    public TextComponent() {}

    public TextComponent(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public static TextComponent fromColoredMessage(String message) {
        LinkedList<ChatComponent> list = new LinkedList<>();
        TextComponent last = new TextComponent();
        StringBuilder builder = new StringBuilder();

        for(int i = 0; i < message.length(); i++) {
            if(message.charAt(i) != '§') {
                builder.append(message.charAt(i));
                continue;
            }
            i++;
            char nextChar = message.charAt(i);

            if (!ChatColor.isColorCode(nextChar) && !ChatFormat.isFormat(nextChar)) {
                builder.append(nextChar);
                continue;
            }

            if (builder.length() > 0) {
                last.setText(builder.toString());
                list.add(last);
                builder = new StringBuilder();
                last = new TextComponent();
            }

            if (ChatColor.isColorCode(nextChar)) {
                last.setColor(ChatColor.getColorByCode(nextChar));
                continue;
            }
            if (ChatFormat.isFormat(nextChar)) {
                switch (ChatFormat.getFormatByCode(nextChar)) {
                    case BOLD:
                        last.setBold(true);
                        break;
                    case ITALIC:
                        last.setItalic(true);
                        break;
                    case UNDERLINE:
                        last.setUnderlined(true);
                        break;
                    case STRIKETHROUGH:
                        last.setStrikeThrough(true);
                        break;
                    case OBFUSCATED:
                        last.setObfuscated(true);
                        break;
                    case RESET:
                        if (builder.length() != 0) {
                            last.setText(builder.toString());
                        }

                        if (last.getText() != null && !last.getText().isEmpty()) {
                            last.setText(builder.toString());
                            builder = new StringBuilder();
                            if (!last.getText().isEmpty()) list.add(last);
                            last = new TextComponent();
                        }

                        last.reset();
                        break;
                }
            }
        }

        if(builder.length() != 0) {
            last.setText(builder.toString());
        }
        list.add(last);

        ChatComponent newComponent = list.removeFirst();
        newComponent.setExtra(list);
        return (TextComponent) newComponent;
    }
}
