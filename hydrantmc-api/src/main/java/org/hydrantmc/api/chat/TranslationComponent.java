package org.hydrantmc.api.chat;

import com.google.common.base.Preconditions;

public class TranslationComponent extends ChatComponent {

    private String translationKey;
    private ChatComponent[] formatArgs;

    public TranslationComponent(String translationKey, ChatComponent... formatArgs) {
        Preconditions.checkNotNull(translationKey, "The translation key cannot be null");
        Preconditions.checkNotNull(translationKey, "The format arguments cannot be null");
        this.translationKey = translationKey;
        this.formatArgs = formatArgs;
    }

    public String getTranslationKey() {
        return translationKey;
    }

    public void setTranslationKey(String translationKey) {
        this.translationKey = translationKey;
    }

    public ChatComponent[] getFormatArgs() {
        return formatArgs;
    }

    public void setFormatArgs(ChatComponent... formatArgs) {
        this.formatArgs = formatArgs;
    }

}
