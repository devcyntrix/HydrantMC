package org.hydrantmc.api.chat.event;

public enum ClickAction {
        OPEN_URL,
        /** Cannot be used within JSON format **/
        OPEN_FILE,
        RUN_COMMAND,
        TWITCH_USER_INFO,
        SUGGEST_COMMAND,
        /** Only usable within written books. **/
        CHANGE_PAGE
}