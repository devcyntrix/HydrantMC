package org.hydrantmc.api.chat.event;

public class ClickEvent {

    private final ClickAction clickAction;
    private final String value;

    public ClickEvent(ClickAction clickAction, String value) {
        this.clickAction = clickAction;
        this.value = value;
    }

    public ClickAction getClickAction() {
        return this.clickAction;
    }

    public String getValue() {
        return this.value;
    }

}
