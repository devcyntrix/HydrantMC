package org.hydrantmc.api.chat.event;

import org.hydrantmc.api.chat.ChatComponent;

public class HoverEvent {

    private final HoverAction hoverAction;
    private final ChatComponent component;

    public HoverEvent(HoverAction hoverAction, ChatComponent component) {
        this.hoverAction = hoverAction;
        this.component = component;
    }

    public HoverAction getHoverAction() {
        return this.hoverAction;
    }

    public ChatComponent getComponents() {
        return this.component;
    }

}
