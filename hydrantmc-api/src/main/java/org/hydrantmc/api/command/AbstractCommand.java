package org.hydrantmc.api.command;

import java.util.Collection;

public abstract class AbstractCommand {

    private final String name;
    protected String description;
    protected String permission;
    protected String[] aliases;

    public AbstractCommand(String name) {
        this.name = name.trim().toLowerCase();
    }

    public abstract void executeCommand(CommandSender sender, String[] args);

    public abstract Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args);

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPermission() {
        return permission;
    }

    public String[] getAliases() {
        return aliases;
    }
}
