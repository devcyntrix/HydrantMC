package org.hydrantmc.api.command;

import com.google.common.base.Function;
import com.google.common.base.Preconditions;
import com.google.common.base.Predicate;
import com.google.common.base.Splitter;

import java.util.*;
import java.util.stream.Collectors;

public class CommandRegistry {

    private final Map<String, AbstractCommand> registeredCommands;

    public CommandRegistry() {
        this.registeredCommands = new HashMap<>();
    }

    public void registerCommand(AbstractCommand command) {
        Preconditions.checkNotNull(command, "command can not be null.");
        registeredCommands.put(command.getName().trim().toLowerCase(), command);
    }

    public void unregisterCommand(AbstractCommand command) {
        Preconditions.checkNotNull(command, "command can not be null.");
        registeredCommands.remove(command.getName().trim().toLowerCase());
    }

    public void unregisterAll() {
        for(AbstractCommand command : registeredCommands.values()) {
            unregisterCommand(command);
        }
    }

    public AbstractCommand getCommand(String name) {
        Preconditions.checkNotNull(name, "name can not be null.");
        return registeredCommands.get(name.trim().toLowerCase());
    }

    public boolean executeCommand(CommandSender sender, String command) {
        Preconditions.checkNotNull(sender, "sender can not be null.");
        Preconditions.checkNotNull(command, "command can not be null.");

        String[] cmd = command.split(" ");
        String name = cmd[0];

        AbstractCommand abstractCommand = getCommand(name);
        if(abstractCommand == null) return false;

        if(!sender.hasPermission(abstractCommand.getPermission())) {
            sender.sendMessage(sender.getServer().getConfiguration().getMessages().getNoPermissionMessage());
            return true;
        }

        String[] args = Arrays.copyOfRange(cmd, 1, cmd.length);

        abstractCommand.executeCommand(sender, args);
        return true;
    }

    public Collection<String> executeTabComplete(CommandSender sender, String command) {
        Preconditions.checkNotNull(sender, "the sender can not be null.");
        Preconditions.checkNotNull(command, "the command name can not be null.");

        String[] cmd = Splitter.on(' ').splitToList(command).toArray(new String[0]);
        String name = cmd[0].toLowerCase();

        AbstractCommand abstractCommand = getCommand(name);
        if(abstractCommand == null || cmd.length < 2) {
            return getRegisteredCommands().values()
                    .stream()
                    .filter((Predicate<AbstractCommand>) abstractCommand1 -> sender.hasPermission(abstractCommand1.getPermission()) && (abstractCommand1.getName().startsWith(name) || name.isEmpty()))
                    .map((Function<AbstractCommand, String>) key -> "/" + key.getName())
                    .collect(Collectors.toSet());
        }

        String[] args = Arrays.copyOfRange(cmd, 1, cmd.length);
        return Objects.requireNonNull(abstractCommand).tabCompleteCommand(sender, args);
    }

    public Map<String, AbstractCommand> getRegisteredCommands() {
        return registeredCommands;
    }
}
