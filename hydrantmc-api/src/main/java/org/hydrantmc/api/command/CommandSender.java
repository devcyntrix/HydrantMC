package org.hydrantmc.api.command;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.permission.Permissible;
import org.hydrantmc.api.utils.UniqueIdentifier;

public interface CommandSender extends UniqueIdentifier, Permissible {

    Server getServer();

    String getName();

    void sendMessage(String message);

}
