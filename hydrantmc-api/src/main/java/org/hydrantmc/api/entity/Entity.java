package org.hydrantmc.api.entity;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.event.entity.EntityTeleportEvent;
import org.hydrantmc.api.network.play.out.entity.PacketOutEntityTeleport;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.Pulseable;
import org.hydrantmc.api.world.World;

import java.util.concurrent.atomic.AtomicInteger;

public abstract class Entity implements Pulseable {

    public static final AtomicInteger ENTITY_ID = new AtomicInteger(0);

    private final int entityId;

    private final Server server;

    private Location previousLocation;
    private Location location;

    private volatile boolean moved;
    private volatile boolean rotated;

    private boolean onGround;

    protected boolean spawned;

    private float health;
    private float maxHealth;

    public Entity(Server server, Location location) {
        this.entityId = ENTITY_ID.getAndIncrement();
        this.server = server;
        this.location = location;

        this.health = 6;
        this.maxHealth = 6;
    }

    public abstract void initEntity();

    public abstract void initToPlayer(Player player);


    public int getEntityId() {
        return this.entityId;
    }

    public abstract String getName();

    public Server getServer() {
        return this.server;
    }

    public World getWorld() {
        return getLocation().getWorld();
    }

    public Location getPreviousLocation() {
        return previousLocation;
    }

    public void setPreviousLocation(Location previousLocation) {
        this.previousLocation = previousLocation;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public boolean isMoved() {
        return getLocation().equals(getPreviousLocation());
    }

    public boolean isRotated() {
        return rotated;
    }

    public void setRotated(boolean rotated) {
        this.rotated = rotated;
    }

    public boolean isOnGround() {
        return onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    public void teleport(Location location) {
        EntityTeleportEvent event = new EntityTeleportEvent(this, location);
        getServer().getPluginManager().getListenerRegistry().callEvent(event);

        if(event.isCancelled()) return;
        this.location = event.getDestination();

        for(Player player : server.getOnlinePlayers()) {
            player.sendPacket(new PacketOutEntityTeleport(entityId, this.location, onGround));
        }
    }

    public void teleport(Entity entity) {
        teleport(entity.getLocation());
    }

    public float getHealth() {
        return health;
    }

    public void setHealth(float health) {
        this.health = health;
    }

    public float getMaxHealth() {
        return maxHealth;
    }

    public void setMaxHealth(float maxHealth) {
        this.maxHealth = maxHealth;
    }

}
