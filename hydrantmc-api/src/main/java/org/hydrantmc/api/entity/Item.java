package org.hydrantmc.api.entity;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.item.ItemStack;
import org.hydrantmc.api.utils.Location;

public class Item extends Entity {

    private ItemStack itemStack;

    public Item(Server server, Location location) {
        super(server, location);
    }

    @Override
    public void initEntity() {

    }

    @Override
    public void initToPlayer(Player player) {

    }

    public Item(Server server, Location location, ItemStack itemStack) {
        super(server, location);

        this.itemStack = itemStack;
    }

    @Override
    public void pulse() {

    }

    @Override
    public String getName() {
        return "Item";
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

}
