package org.hydrantmc.api.entity;

import com.google.common.base.Objects;
import io.netty.channel.ChannelFuture;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.chat.ChatMessageType;
import org.hydrantmc.api.chat.TextComponent;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.event.player.PlayerKickEvent;
import org.hydrantmc.api.inventory.InventoryHolder;
import org.hydrantmc.api.inventory.PlayerInventory;
import org.hydrantmc.api.network.Connection;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.network.play.out.*;
import org.hydrantmc.api.network.play.out.entity.*;
import org.hydrantmc.api.network.play.out.player.PacketOutPlayerAbilities;
import org.hydrantmc.api.network.play.out.player.PacketOutUpdateHealth;
import org.hydrantmc.api.network.play.out.scoreboard.PacketOutDisplayScoreboard;
import org.hydrantmc.api.scoreboard.Scoreboard;
import org.hydrantmc.api.tablist.TabListItem;
import org.hydrantmc.api.utils.*;
import org.hydrantmc.api.utils.player.*;

import java.net.InetSocketAddress;
import java.util.UUID;

public class Player extends Entity implements CommandSender, InventoryHolder {

    private final PendingConnection connection;

    private boolean isForgeUser;

    private ClientSettings clientSettings;

    private short ping = 0;

    private GameMode gameMode;

    private final TabListItem tabListItem;

    private boolean sprinting;
    private boolean sneaking;

    private PlayerAbilities playerAbilities;

    private int heldItemSlot;

    private Scoreboard scoreboard;

    private final PlayerInventory playerInventory;

    private int foodLevel;
    private float foodSaturation;

    public Player(Location location, PendingConnection connection) {
        super(connection.getServer(), location);
        this.connection = connection;

        this.gameMode = getServer().getConfiguration().getDefaultGameMode();

        this.tabListItem = new TabListItem(getPing(), getGameMode(), getGameProfile(), null);
        this.playerAbilities = new PlayerAbilities();
        this.playerInventory = new PlayerInventory(this);

        //TODO: Save health & food level
        setFoodLevel(20);
        setFoodSaturation(5);
        setMaxHealth(20);
        setHealth(20);
    }

    @Override
    public void pulse() {

    }

    @Override
    public void initEntity() {

    }

    @Override
    public void initToPlayer(Player player) {

    }

    @Override
    public String getName() {
        return this.connection.getGameProfile().getUsername();
    }

    public UUID getUniqueId() {
        return this.connection.getGameProfile().getUniqueId();
    }

    public InetSocketAddress getAddress() {
        return connection.getAddress();
    }

    public ChannelFuture sendPacket(OutgoingPacket packet) {
        return connection.sendPacketAndFlush(packet);
    }

    public float getYaw() {
        return getLocation().getYaw();
    }

    public float getPitch() {
        return getLocation().getPitch();
    }

    public TabListItem getTabListItem() {
        return tabListItem;
    }

    public boolean hasPermission(String permission) {
        if(permission == null || permission.isEmpty()) return true;

        /* TODO: Write a permission system and check op */

//        return getServer().getPermissionsFile().getPermissions(getUniqueId()).contains(permission.toLowerCase());
        return true;
    }

    public void sendMessage(String message) {
        sendMessage(ChatMessageType.SYSTEM, message);
    }

    public void sendMessage(ChatComponent component) {
        sendMessage(ChatMessageType.SYSTEM, component);
    }

    public void sendMessage(ChatMessageType type, String message) {
        ChatComponent component;

        if(type != ChatMessageType.ACTION_BAR)
            component = TextComponent.fromColoredMessage(message);
            else
            component = new TextComponent(message);

        sendMessage(type, component);
    }

    public void sendMessage(ChatMessageType type, ChatComponent component) {
        if(getClientSettings() != null && getClientSettings().getChatMode() == ChatMode.DISABLED && type != ChatMessageType.ACTION_BAR) return;
        if(getClientSettings() != null && getClientSettings().getChatMode() == ChatMode.COMMANDS_ONLY && type == ChatMessageType.CHAT) return;
        connection.sendPacket(new PacketOutChatMessage(component, type));
    }

    public void sendMessage(ChatMessageType type, ChatComponent... components) {
        for(ChatComponent component : components) {
            sendMessage(type, component);
        }
    }

    public void sendTitle(ChatComponent title, ChatComponent subTitle) {
        sendPacket(new PacketOutTitle(TitleAction.TITLE, title));
        sendPacket(new PacketOutTitle(TitleAction.SUB_TITLE, subTitle));
    }

    public void sendTitle(int fadeIn, int stay, int fadeOut) {
        sendPacket(new PacketOutTitle(fadeIn, stay, fadeOut));
    }

    public void hideTitle() {
        sendPacket(new PacketOutTitle(TitleAction.HIDE));
    }

    public void resetTitle() {
        sendPacket(new PacketOutTitle(TitleAction.RESET));
    }

    public void kickPlayer(String reason) {
        kickPlayer(null, reason);
    }

    public void kickPlayer(CommandSender sender, String reason) {
        PlayerKickEvent playerKickEvent = new PlayerKickEvent(sender, this, reason);
        getServer().getListenerRegistry().callEvent(playerKickEvent);
        this.connection.disconnect(playerKickEvent.getKickMessage());
    }

    public Connection getConnection() {
        return connection;
    }

    public int getProtocolVersion() {
        return this.connection.getProtocolVersion();
    }

    public boolean isForgeUser() {
        return this.isForgeUser;
    }

    public void setForgeUser(boolean forgeUser) {
        this.isForgeUser = forgeUser;
    }

    public GameProfile getGameProfile() {
        return this.connection.getGameProfile();
    }

    public ClientSettings getClientSettings() {
        return this.clientSettings;
    }

    public void setClientSettings(ClientSettings clientSettings) {
        this.clientSettings = clientSettings;
    }

    public short getPing() {
        return ping;
    }

    public void setPing(short ping) {
        this.ping = ping;
    }

    public void sendResourcePack(String url) {
        sendResourcePack(url, "");
    }

    public void sendResourcePack(String url, String hash) {
        connection.sendPacket(new PacketOutSendResourcePack(url, hash));
    }

    public void setPlayerListHeaderAndFooter(ChatComponent header, ChatComponent footer) {
        connection.sendPacket(new PacketOutPlayerListHeaderAndFooter(header, footer));
    }

    public void setPlayerListHeaderAndFooter(String header, String footer) {
        setPlayerListHeaderAndFooter(TextComponent.fromColoredMessage(header), TextComponent.fromColoredMessage(footer));
    }

    public void resetPlayerListHeaderAndFooter() {
        connection.sendPacket(new PacketOutPlayerListHeaderAndFooter(new TextComponent(""), new TextComponent("")));
    }

    public boolean isSprinting() {
        return sprinting;
    }

    public void setSprinting(boolean sprinting) {
        this.sprinting = sprinting;
    }

    public boolean isSneaking() {
        return sneaking;
    }

    public void setSneaking(boolean sneaking) {
        this.sneaking = sneaking;
    }

    public PlayerAbilities getPlayerAbilities() {
        return playerAbilities;
    }

    public void setPlayerAbilities(PlayerAbilities playerAbilities, boolean send) {
        this.playerAbilities = playerAbilities;
        if(send) {
            sendPacket(new PacketOutPlayerAbilities(playerAbilities));
        }
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
        sendPacket(new PacketOutGameStateChange(GameStateChangeReason.CHANGE_GAME_MODE, gameMode.ordinal()));

        if(gameMode.equals(GameMode.CREATIVE))
            getPlayerAbilities().setAllowFlying(true);
        else
            getPlayerAbilities().setAllowFlying(false);
    }

    public int getHeldItemSlot() {
        return heldItemSlot;
    }

    public void setHeldItemSlot(int slot, boolean send) {
        if(send) {
            sendPacket(new PacketOutHeldItemChange(slot));
        }
        this.heldItemSlot = slot;
    }

    public void setHeldItemSlot(int slot) {
        setHeldItemSlot(slot, true);
    }

    public Scoreboard getScoreboard() {
        return this.scoreboard;
    }

    public void setScoreboard(Scoreboard scoreboard) {
        this.scoreboard = scoreboard;
        this.scoreboard.sendToPlayer(this);
    }

    public void doSwing() {
        getServer().broadcast(player -> player != this, player -> player.sendPacket(new PacketOutAnimation(getEntityId(), (byte) 0)));
    }

    @Override
    public PlayerInventory getInventory() {
        return playerInventory;
    }

    @Override
    public boolean hasOp() {
        return false;
    }

    @Override
    public void setOp(boolean op) {
        /* TODO: Write op */
    }

    @Override
    public void setHealth(float health) {
        super.setHealth(health);

        sendPacket(new PacketOutUpdateHealth(health, getFoodLevel(), getFoodSaturation()));
    }

    public int getFoodLevel() {
        return foodLevel;
    }

    public void setFoodLevel(int foodLevel) {
        this.foodLevel = foodLevel;

        sendPacket(new PacketOutUpdateHealth(getHealth(), foodLevel, getFoodSaturation()));
    }

    public float getFoodSaturation() {
        return foodSaturation;
    }

    public void setFoodSaturation(float foodSaturation) {
        this.foodSaturation = foodSaturation;

        sendPacket(new PacketOutUpdateHealth(getHealth(), getFoodLevel(), foodSaturation));
    }

    @Override
    public void setMaxHealth(float maxHealth) {
        super.setMaxHealth(maxHealth);

        sendPacket(new PacketOutEntityProperties(getEntityId(), new EntityProperty("generic.maxHealth", maxHealth)));
    }

    public void kill() {
        setHealth(0);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Player that = (Player) o;
        return Objects.equal(getConnection(), that.getConnection());
    }

}
