package org.hydrantmc.api.event;

public abstract class AbstractEvent {

    private final boolean asynchronous;

    public AbstractEvent() {
        this(false);
    }

    public AbstractEvent(boolean asynchronous) {
        this.asynchronous = asynchronous;
    }

    public boolean isAsynchronous() {
        return asynchronous;
    }
}
