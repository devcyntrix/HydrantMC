package org.hydrantmc.api.event;

/**
 * Created by Cyntrix on 08.07.2017.
 */
public interface Cancelable {

    boolean isCancelled();

    void setCancelled(boolean cancel);
}