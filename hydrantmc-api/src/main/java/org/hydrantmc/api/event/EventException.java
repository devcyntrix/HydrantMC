package org.hydrantmc.api.event;

import org.hydrantmc.api.listener.RegisteredListener;

/**
 * Created by Cyntrix on 14.07.2017.
 */
public class EventException extends Exception {

    public EventException(RegisteredListener listener, Throwable cause) {
        super(listener.getListener().getClass().getSimpleName() + ": " + listener.getMethod().getName(), cause);
    }

}
