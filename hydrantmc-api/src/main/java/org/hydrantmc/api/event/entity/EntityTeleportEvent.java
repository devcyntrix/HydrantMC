package org.hydrantmc.api.event.entity;

import org.hydrantmc.api.entity.Entity;
import org.hydrantmc.api.event.AbstractEvent;
import org.hydrantmc.api.event.Cancelable;
import org.hydrantmc.api.utils.Location;

public class EntityTeleportEvent extends AbstractEvent implements Cancelable {

    private boolean cancelled;
    private final Entity entity;
    private Location destination;

    public EntityTeleportEvent(Entity entity, Location destination) {
        super(false);
        this.entity = entity;
        this.destination = destination;
    }

    public Entity getEntity() {
        return entity;
    }

    public Location getDestination() {
        return destination;
    }

    public void setDestination(Location destination) {
        this.destination = destination;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

}
