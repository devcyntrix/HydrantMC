package org.hydrantmc.api.event.player;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;
import org.hydrantmc.api.utils.player.ClientSettings;

public class PlayerClientSettingsUpdateEvent extends AbstractEvent {

    private final Player player;
    private final ClientSettings clientSettings;

    public PlayerClientSettingsUpdateEvent(Player player, ClientSettings clientSettings) {
        super(false);

        this.player = player;
        this.clientSettings = clientSettings;
    }

    public Player getPlayer() {
        return player;
    }

    public ClientSettings getClientSettings() {
        return clientSettings;
    }

}
