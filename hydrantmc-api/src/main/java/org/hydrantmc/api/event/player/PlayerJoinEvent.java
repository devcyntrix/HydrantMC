package org.hydrantmc.api.event.player;

import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;

public class PlayerJoinEvent extends AbstractEvent {

    private final Player player;
    private ChatComponent joinMessage;

    public PlayerJoinEvent(Player player, ChatComponent joinMessage) {
        super(false);

        this.player = player;
        this.joinMessage = joinMessage;
    }

    public Player getPlayer() {
        return player;
    }

    public ChatComponent getJoinMessage() {
        return joinMessage;
    }

    public void setJoinMessage(ChatComponent joinMessage) {
        this.joinMessage = joinMessage;
    }

}
