package org.hydrantmc.api.event.player;

import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;

public class PlayerKickEvent extends AbstractEvent {

    private final CommandSender sender;
    private final Player player;
    private String kickMessage;

    public PlayerKickEvent(CommandSender sender, Player player, String kickMessage) {
        super(false);
        this.sender = sender;
        this.player = player;
        this.kickMessage = kickMessage;
    }

    public CommandSender getSender() {
        return sender;
    }

    public Player getPlayer() {
        return player;
    }

    public String getKickMessage() {
        return kickMessage;
    }

    public void setKickMessage(String kickMessage) {
        this.kickMessage = kickMessage;
    }

}
