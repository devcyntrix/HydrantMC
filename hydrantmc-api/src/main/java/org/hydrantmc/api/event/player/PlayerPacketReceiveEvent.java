package org.hydrantmc.api.event.player;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;
import org.hydrantmc.api.network.Packet;

public class PlayerPacketReceiveEvent extends AbstractEvent {

    private final Player player;
    private final Packet<?> packet;

    public PlayerPacketReceiveEvent(Player player, Packet<?> packet) {
        super(false);
        this.player = player;
        this.packet = packet;
    }

    public Player getPlayer() {
        return player;
    }

    public Packet<?> getPacket() {
        return packet;
    }

}
