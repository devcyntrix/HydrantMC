package org.hydrantmc.api.event.player;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;
import org.hydrantmc.api.event.Cancelable;
import org.hydrantmc.api.network.Packet;

public class PlayerPacketSendEvent extends AbstractEvent implements Cancelable {

    private boolean cancelled;
    private final Player player;
    private final Packet<?> packet;

    public PlayerPacketSendEvent(Player player, Packet<?> packet) {
        super(false);
        this.player = player;
        this.packet = packet;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Player getPlayer() {
        return player;
    }

    public Packet<?> getPacket() {
        return packet;
    }

}
