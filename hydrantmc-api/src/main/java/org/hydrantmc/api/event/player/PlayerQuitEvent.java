package org.hydrantmc.api.event.player;

import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;

public class PlayerQuitEvent extends AbstractEvent {

    private final Player player;
    private ChatComponent quitMessage;

    public PlayerQuitEvent(Player player, ChatComponent quitMessage) {
        super(false);

        this.player = player;
        this.quitMessage = quitMessage;
    }

    public Player getPlayer() {
        return player;
    }

    public ChatComponent getQuitMessage() {
        return quitMessage;
    }

    public void setQuitMessage(ChatComponent quitMessage) {
        this.quitMessage = quitMessage;
    }

}
