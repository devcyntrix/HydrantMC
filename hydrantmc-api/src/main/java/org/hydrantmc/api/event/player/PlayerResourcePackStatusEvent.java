package org.hydrantmc.api.event.player;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;
import org.hydrantmc.api.utils.ResourcePackStatus;

public class PlayerResourcePackStatusEvent extends AbstractEvent {

    private final Player player;
    private final ResourcePackStatus status;

    public PlayerResourcePackStatusEvent(Player player, ResourcePackStatus status) {
        super(false);
        this.player = player;
        this.status = status;
    }

    public Player getPlayer() {
        return player;
    }

    public ResourcePackStatus getStatus() {
        return status;
    }

}
