package org.hydrantmc.api.event.player;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;

public class PlayerToggleSneakingEvent extends AbstractEvent {

    private final Player player;
    private final boolean isSneaking;

    public PlayerToggleSneakingEvent(Player player, boolean isSneaking) {
        super(false);
        this.player = player;
        this.isSneaking = isSneaking;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isSneaking() {
        return isSneaking;
    }

}
