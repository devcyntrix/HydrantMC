package org.hydrantmc.api.event.player;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.AbstractEvent;

public class PlayerToggleSprintingEvent extends AbstractEvent {

    private final Player player;
    private final boolean isSprinting;

    public PlayerToggleSprintingEvent(Player player, boolean isSprinting) {
        super(false);
        this.player = player;
        this.isSprinting = isSprinting;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isSprinting() {
        return isSprinting;
    }


}
