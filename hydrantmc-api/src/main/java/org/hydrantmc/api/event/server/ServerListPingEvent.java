package org.hydrantmc.api.event.server;

import org.hydrantmc.api.event.AbstractEvent;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.utils.status.ServerStatus;

public class ServerListPingEvent extends AbstractEvent {

    private final boolean legacy;
    private final PendingConnection pendingConnection;
    private ServerStatus serverStatus;

    public ServerListPingEvent(boolean legacy, PendingConnection pendingConnection, ServerStatus serverStatus) {
        // TODO: Check async
        super(false);
        this.legacy = legacy;
        this.pendingConnection = pendingConnection;
        this.serverStatus = serverStatus;
    }

    /**
     * @return true if the ping is from a legacy client
     */
    public boolean isLegacy() {
        return legacy;
    }

    /**
     * @return null if the ping is from a legacy client
     */
    public PendingConnection getPendingConnection() {
        return pendingConnection;
    }

    public ServerStatus getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(ServerStatus serverStatus) {
        this.serverStatus = serverStatus;
    }
}
