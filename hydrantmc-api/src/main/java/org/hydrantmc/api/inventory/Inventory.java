package org.hydrantmc.api.inventory;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.item.ItemStack;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Spliterator;
import java.util.function.Consumer;

public class Inventory {

    private final InventoryHolder holder;

    private ItemStack[] contents;

    public Inventory(InventoryHolder holder, int size) {
        this.holder = holder;
        this.contents = new ItemStack[size];
    }

    public void clear() {
        for (int i = 0; i < contents.length; i++) {
            contents[i] = null;
        }
    }

    public void setItem(int index, ItemStack item) {
        contents[index] = item;
    }

    public ItemStack getItem(int index) {
        return contents[index];
    }

    public ItemStack[] getContents() {
        return contents;
    }

    public void setContents(ItemStack[] contents) {
        Preconditions.checkArgument(contents.length == 45, "the contents array must be 45 size large");
        this.contents = contents;
    }

    public InventoryHolder getHolder() {
        return holder;
    }

    public Iterator<ItemStack> iterator() {
        return Arrays.stream(this.contents).iterator();
    }

    public void forEach(Consumer<? super ItemStack> action) {
        Arrays.stream(this.contents).forEach(action);
    }

    public Spliterator<ItemStack> spliterator() {
        return Arrays.stream(this.contents).spliterator();
    }

}
