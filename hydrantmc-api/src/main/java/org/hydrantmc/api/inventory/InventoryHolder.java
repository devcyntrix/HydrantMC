package org.hydrantmc.api.inventory;

public interface InventoryHolder {

    Inventory getInventory();

}
