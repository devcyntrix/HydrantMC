package org.hydrantmc.api.inventory;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.item.ItemStack;

public class PlayerInventory extends Inventory {

    private final Player holder;

    private ItemStack[] armorContents;

    public PlayerInventory(Player holder) {
        super(holder, 45);
        this.holder = holder;

        this.armorContents = new ItemStack[4];
    }

    /**
     * Get all ItemStacks from the armor slots
     *
     * @return All the ItemStacks from the armor slots
     */
    public ItemStack[] getArmorContents() {
        return armorContents;
    }

    /**
     * Return the ItemStack from the helmet slot
     *
     * @return The ItemStack in the helmet slot
     */
    public ItemStack getHelmet() {
        return armorContents[3];
    }

    /**
     * Return the ItemStack from the chestplate slot
     *
     * @return The ItemStack in the chestplate slot
     */
    public ItemStack getChestplate() {
        return armorContents[2];
    }

    /**
     * Return the ItemStack from the leg slot
     *
     * @return The ItemStack in the leg slot
     */
    public ItemStack getLeggings() {
        return armorContents[1];
    }

    /**
     * Return the ItemStack from the boots slot
     *
     * @return The ItemStack in the boots slot
     */
    public ItemStack getBoots() {
        return armorContents[0];
    }


    /**
     * Stores the ItemStack at the given index of the inventory.
     * <p>
     * Indexes 0 through 8 refer to the hotbar. 9 through 35 refer to the main inventory, counting up from 9 at the top
     * left corner of the inventory, moving to the right, and moving to the row below it back on the left side when it
     * reaches the end of the row. It follows the same path in the inventory like you would read a book.
     * <p>
     * Indexes 36 through 39 refer to the armor slots. Though you can set armor with this method using these indexes,
     * you are encouraged to use the provided methods for those slots.
     * <p>
     * If you attempt to use this method with an index less than 0 or greater than 39, an ArrayIndexOutOfBounds
     * exception will be thrown.
     *
     * @param index The index where to put the ItemStack
     * @param item The ItemStack to set
     * @throws ArrayIndexOutOfBoundsException when index &lt; 0 || index &gt; 39
     * @see #setBoots(ItemStack)
     * @see #setChestplate(ItemStack)
     * @see #setHelmet(ItemStack)
     * @see #setLeggings(ItemStack)
     */
    public void setItem(int index, ItemStack item) {
        super.setItem(index, item);
    }

    /**
     * Put the given ItemStacks into the armor slots
     *
     * @param items The ItemStacks to use as armour
     */
    public void setArmorContents(ItemStack[] items) {
        this.armorContents = items;
    }

    /**
     * Put the given ItemStack into the helmet slot. This does not check if
     * the ItemStack is a helmet
     *
     * @param helmet The ItemStack to use as helmet
     */
    public void setHelmet(ItemStack helmet) {
        armorContents[3] = helmet;
    }

    /**
     * Put the given ItemStack into the chestplate slot. This does not check
     * if the ItemStack is a chestplate
     *
     * @param chestplate The ItemStack to use as chestplate
     */
    public void setChestplate(ItemStack chestplate) {
        armorContents[2] = chestplate;
    }

    /**
     * Put the given ItemStack into the leg slot. This does not check if the
     * ItemStack is a pair of leggings
     *
     * @param leggings The ItemStack to use as leggings
     */
    public void setLeggings(ItemStack leggings) {
        armorContents[1] = leggings;
    }

    /**
     * Put the given ItemStack into the boots slot. This does not check if the
     * ItemStack is a boots
     *
     * @param boots The ItemStack to use as boots
     */
    public void setBoots(ItemStack boots) {
        armorContents[0] = boots;
    }

    /**
     * Gets a copy of the item the player is currently holding
     *
     * @return the currently held item
     */
    public ItemStack getItemInHand() {
        return getItem(getHeldItemSlot());
    }

    /**
     * Sets the item the player is holding
     *
     * @param stack The item to put into the player's hand
     */
    public void setItemInHand(ItemStack stack) {
        setItem(getHeldItemSlot(), stack);
    }

    /**
     * Get the slot number of the currently held item
     *
     * @return Held item slot number
     */
    public int getHeldItemSlot() {
        return holder.getHeldItemSlot();
    }

    /**
     * Set the slot number of the currently held item.
     * <p>
     * This validates whether the slot is between 0 and 8 inclusive.
     *
     * @param slot The new slot number
     * @throws IllegalArgumentException Thrown if slot is not between 0 and 8
     *     inclusive
     */
    public void setHeldItemSlot(int slot) {
        Preconditions.checkState(slot < 9 && slot > 0, "Slot must be between 0 and 8");
        holder.setHeldItemSlot(slot);
    }

}
