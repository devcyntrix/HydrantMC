package org.hydrantmc.api.item;

import org.hydrantmc.api.item.meta.ItemMeta;
import org.hydrantmc.nbt.NBTTagCompound;

public class ItemStack {

    private Material material;
    private final byte amount;
    private short damage;
    private final NBTTagCompound tagCompound;
    private ItemMeta itemMeta;

    public ItemStack(Material material) {
        this(material, (byte) 0);
    }

    public ItemStack(Material material, byte amount) {
        this(material, amount, (short) 0);
    }

    public ItemStack(Material material, byte amount, short damage) {
        this(material, amount, damage, null);
    }

    public ItemStack(Material material, byte amount, short damage, NBTTagCompound tagCompound) {
        this.material = material;
        this.amount = amount;
        this.damage = damage;
        this.tagCompound = tagCompound;
    }

    public Material getMaterial() {
        return material;
    }

    public void setMaterial(Material material) {
        this.material = material;
    }

    public short getDamage() {
        return damage;
    }

    public void setDamage(short damage) {
        this.damage = damage;
    }

    public ItemMeta getItemMeta() {
        return itemMeta;
    }

    public void setItemMeta(ItemMeta itemMeta) {
        this.itemMeta = itemMeta;
    }

    public byte getAmount() {
        return amount;
    }

    public NBTTagCompound getTagCompound() {
        return tagCompound;
    }

    @Override
    public String toString() {
        return "ItemStack{" +
                "material=" + material +
                ", amount=" + amount +
                ", damage=" + damage +
                ", tagCompound=" + tagCompound +
                ", itemMeta=" + itemMeta +
                '}';
    }

}
