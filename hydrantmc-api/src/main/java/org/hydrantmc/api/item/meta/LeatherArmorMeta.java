package org.hydrantmc.api.item.meta;

import org.hydrantmc.api.item.Material;
import org.hydrantmc.api.utils.inventory.Color;

/**
 * Represents leather armor ({@link Material#LEATHER_BOOTS}, {@link
 * Material#LEATHER_CHESTPLATE}, {@link Material#LEATHER_HELMET}, or {@link
 * Material#LEATHER_LEGGINGS}) that can be colored.
 */
public interface LeatherArmorMeta extends ItemMeta {

    /**
     * Gets the color of the armor.
     *
     * @return the color of the armor, never null
     */
    Color getColor();

    /**
     * Sets the color of the armor.
     *
     * @param color the color to set. Setting it to null will reset it.
     */
    void setColor(Color color);

}