package org.hydrantmc.api.listener;

import com.google.common.collect.Multimap;
import com.google.common.collect.TreeMultimap;
import org.hydrantmc.api.event.AbstractEvent;
import org.hydrantmc.api.event.Cancelable;
import org.hydrantmc.api.event.EventException;
import org.hydrantmc.api.event.EventHandler;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

public class ListenerRegistry {

    private final Map<Class<? extends Listener>, Listener> registeredListeners;
    private final Multimap<Class<? extends AbstractEvent>, RegisteredListener> listeners;

    public ListenerRegistry() {
        this.registeredListeners = new HashMap<>();
        this.listeners = TreeMultimap.create((o1, o2) -> 0, new RegisteredListenerComparator());
    }

    @SuppressWarnings("unchecked")
    public void registerListener(Listener listener) {
        this.registeredListeners.put(listener.getClass(), listener);
        try {
            Method listenMethod = null;
            for (Method method : listener.getClass().getMethods()) {
                if (!method.isAnnotationPresent(EventHandler.class)) continue;
                if (method.getParameterCount() != 1) continue;
                if (!AbstractEvent.class.isAssignableFrom(method.getParameterTypes()[0])) continue;
                method.setAccessible(true);

                EventHandler eventHandler = method.getAnnotation(EventHandler.class);
                Class<? extends AbstractEvent> eventClass = (Class<? extends AbstractEvent>) method.getParameterTypes()[0];
                this.listeners.put(eventClass, new RegisteredListener(eventHandler, listener, method));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Listener getListener(Class<? extends Listener> listenerClass) {
        return this.registeredListeners.get(listenerClass);
    }

    public void unregisterListener(Class<? extends Listener> listenerClass) {
        this.registeredListeners.remove(listenerClass);
        for (Map.Entry<Class<? extends AbstractEvent>, RegisteredListener> entry : this.listeners.entries()) {
            if (entry.getValue().getListener().getClass() != listenerClass) continue;
            this.listeners.remove(entry.getKey(), entry.getValue());
        }
    }

    public AbstractEvent callEvent(AbstractEvent event) {
        for (RegisteredListener listener : this.listeners.get(event.getClass())) {
            if (event instanceof Cancelable) {
                Cancelable cancellable = (Cancelable) event;
                if (cancellable.isCancelled() && listener.getEventHandler().ignoreCancelled()) continue;
            }
            try {
                listener.getMethod().invoke(listener.getListener(), event);
            } catch (Exception exception) {
                try {
                    throw new EventException(listener, exception);
                } catch (Exception eventException) {
                    exception.printStackTrace();
                }
            }
        }
        return event;
    }

}
