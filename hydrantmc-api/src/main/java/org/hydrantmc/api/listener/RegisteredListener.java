package org.hydrantmc.api.listener;

import org.hydrantmc.api.event.EventHandler;

import java.lang.reflect.Method;

/**
 * Created by Cyntrix on 14.07.2017.
 */
public class RegisteredListener {

    private EventHandler eventHandler;
    private Listener listener;
    private Method method;

    public RegisteredListener(EventHandler eventHandler, Listener listener, Method method) {
        this.eventHandler = eventHandler;
        this.listener = listener;
        this.method = method;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public Listener getListener() {
        return listener;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }
}
