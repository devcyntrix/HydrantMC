package org.hydrantmc.api.listener;

import java.util.Comparator;

/**
 * Created by Cyntrix on 14.07.2017.
 */
public class RegisteredListenerComparator implements Comparator<RegisteredListener> {

    @Override
    public int compare(RegisteredListener o1, RegisteredListener o2) {
        return Integer.compare(o1.getEventHandler().priority().getSlot(), o2.getEventHandler().priority().getSlot());
    }
}
