package org.hydrantmc.api.network;

import org.hydrantmc.api.Server;

public abstract class AbstractPacketHandler {

    private final Server server;

    protected AbstractPacketHandler(Server server) {
        this.server = server;
    }

    public abstract void register(PendingConnection connection);

    public abstract void unregister();

    public abstract void caughtException(Throwable cause);

    protected Server getServer() {
        return server;
    }

}
