package org.hydrantmc.api.network;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import org.hydrantmc.api.Server;

import java.net.InetSocketAddress;

public interface Connection {

    Server getServer();

    Channel getChannel();

    InetSocketAddress getAddress();

    ConnectionState getConnectionState();

    void setConnectionState(ConnectionState state);

    ChannelFuture sendPacket(OutgoingPacket packet);

    ChannelFuture sendPacketAndFlush(OutgoingPacket packet);

    void flush();

    void disconnect();

}
