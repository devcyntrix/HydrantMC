package org.hydrantmc.api.network;

import com.google.common.collect.BiMap;
import com.google.common.collect.HashBiMap;
import org.hydrantmc.api.network.handshake.in.PacketInHandshake;
import org.hydrantmc.api.network.login.in.PacketInEncryptionResponse;
import org.hydrantmc.api.network.login.in.PacketInLoginRequest;
import org.hydrantmc.api.network.login.out.PacketOutCompression;
import org.hydrantmc.api.network.login.out.PacketOutEncryptionRequest;
import org.hydrantmc.api.network.login.out.PacketOutLoginDisconnect;
import org.hydrantmc.api.network.login.out.PacketOutLoginResponse;
import org.hydrantmc.api.network.play.in.*;
import org.hydrantmc.api.network.play.out.*;
import org.hydrantmc.api.network.play.out.entity.*;
import org.hydrantmc.api.network.play.out.player.PacketOutPlayerAbilities;
import org.hydrantmc.api.network.play.out.player.PacketOutPlayerPosLook;
import org.hydrantmc.api.network.play.out.player.PacketOutSpawnPlayer;
import org.hydrantmc.api.network.play.out.player.PacketOutUpdateHealth;
import org.hydrantmc.api.network.play.out.window.PacketOutWindowSetSlot;
import org.hydrantmc.api.network.play.out.world.PacketOutBlockChange;
import org.hydrantmc.api.network.play.out.world.PacketOutChunkColumnData;
import org.hydrantmc.api.network.play.out.world.PacketOutMapChunkBulk;
import org.hydrantmc.api.network.status.in.PacketInPing;
import org.hydrantmc.api.network.status.in.PacketInStatusRequest;
import org.hydrantmc.api.network.status.out.PacketOutPong;
import org.hydrantmc.api.network.status.out.PacketOutStatusResponse;

import java.util.HashMap;
import java.util.Map;

public enum ConnectionState {

    HANDSHAKE() {
        {
            registerPacket(PacketDirection.SERVER, 0x00, PacketInHandshake.class);
        }
    },
    STATUS() {
        {
            registerPacket(PacketDirection.SERVER, 0x00, PacketInStatusRequest.class);
            registerPacket(PacketDirection.CLIENT, 0x00, PacketOutStatusResponse.class);
            registerPacket(PacketDirection.SERVER, 0x01, PacketInPing.class);
            registerPacket(PacketDirection.CLIENT, 0x01, PacketOutPong.class);
        }
    },
    LOGIN() {
        {
            registerPacket(PacketDirection.CLIENT, 0x00, PacketOutLoginDisconnect.class);
            registerPacket(PacketDirection.SERVER, 0x00, PacketInLoginRequest.class);
            registerPacket(PacketDirection.CLIENT, 0x01, PacketOutEncryptionRequest.class);
            registerPacket(PacketDirection.SERVER, 0x01, PacketInEncryptionResponse.class);
            registerPacket(PacketDirection.CLIENT, 0x02, PacketOutLoginResponse.class);
            registerPacket(PacketDirection.CLIENT, 0x03, PacketOutCompression.class);
        }
    },
    PLAY() {
        {
            registerPacket(PacketDirection.CLIENT, 0x00, PacketOutKeepAlive.class);
            registerPacket(PacketDirection.SERVER, 0x00, PacketInKeepAlive.class);
            registerPacket(PacketDirection.CLIENT, 0x01, PacketOutJoinGame.class);
            registerPacket(PacketDirection.SERVER, 0x01, PacketInChatMessage.class);
            registerPacket(PacketDirection.CLIENT, 0x02, PacketOutChatMessage.class);
            registerPacket(PacketDirection.SERVER, 0x02, PacketInUseEntity.class);

            registerPacket(PacketDirection.SERVER, 0x03, PacketInPlayerOnGround.class);

            registerPacket(PacketDirection.SERVER, 0x04, PacketInPlayerPosition.class);
            registerPacket(PacketDirection.CLIENT, 0x05, PacketOutSpawnPosition.class);
            registerPacket(PacketDirection.SERVER, 0x05, PacketInPlayerLook.class);
            registerPacket(PacketDirection.CLIENT, 0x06, PacketOutUpdateHealth.class);
            registerPacket(PacketDirection.SERVER, 0x06, PacketInPlayerPosLook.class);

            registerPacket(PacketDirection.SERVER, 0x07, PacketInPlayerDigging.class);
            registerPacket(PacketDirection.CLIENT, 0x08, PacketOutPlayerPosLook.class);
            registerPacket(PacketDirection.SERVER, 0x08, PacketInPlaceBlock.class);
            registerPacket(PacketDirection.CLIENT, 0x09, PacketOutHeldItemChange.class);
            registerPacket(PacketDirection.SERVER, 0x09, PacketInHeldItemChange.class);

            registerPacket(PacketDirection.SERVER, 0x0A, PacketInAnimation.class);
            registerPacket(PacketDirection.CLIENT, 0x0B, PacketOutAnimation.class);
            registerPacket(PacketDirection.SERVER, 0x0B, PacketInEntityAction.class);
            registerPacket(PacketDirection.CLIENT, 0x0C, PacketOutSpawnPlayer.class);

            registerPacket(PacketDirection.CLIENT, 0x0E, PacketOutSpawnObject.class);

            registerPacket(PacketDirection.CLIENT, 0x0F, PacketOutSpawnEntity.class);

            registerPacket(PacketDirection.SERVER, 0x10, PacketInCreativeInventoryAction.class);
            registerPacket(PacketDirection.CLIENT, 0x13, PacketOutDestroyEntities.class);
            registerPacket(PacketDirection.SERVER, 0x13, PacketInPlayerAbilities.class);

            registerPacket(PacketDirection.SERVER, 0x14, PacketInTabCompleteRequest.class);

            registerPacket(PacketDirection.CLIENT, 0x15, PacketOutEntityRelativeMove.class);
            registerPacket(PacketDirection.SERVER, 0x15, PacketInClientSettings.class);
            registerPacket(PacketDirection.CLIENT, 0x16, PacketOutEntityLook.class);

            registerPacket(PacketDirection.CLIENT, 0x17, PacketOutEntityLookAndMove.class);
            registerPacket(PacketDirection.SERVER, 0x17, PacketInCustomPayload.class);
            registerPacket(PacketDirection.CLIENT, 0x18, PacketOutEntityTeleport.class);

            registerPacket(PacketDirection.CLIENT, 0x19, PacketOutEntityHeadRotation.class);
            registerPacket(PacketDirection.SERVER, 0x19, PacketInResourcePackStatus.class);
            registerPacket(PacketDirection.CLIENT, 0x20, PacketOutEntityProperties.class);

            registerPacket(PacketDirection.CLIENT, 0x21, PacketOutChunkColumnData.class);

            registerPacket(PacketDirection.CLIENT, 0x23, PacketOutBlockChange.class);

            registerPacket(PacketDirection.CLIENT, 0x24, PacketOutBlockAction.class);

            registerPacket(PacketDirection.CLIENT, 0x26, PacketOutMapChunkBulk.class);

            registerPacket(PacketDirection.CLIENT, 0x29, PacketOutSoundEffect.class);

            registerPacket(PacketDirection.CLIENT, 0x2B, PacketOutGameStateChange.class);

            registerPacket(PacketDirection.CLIENT, 0x2F, PacketOutWindowSetSlot.class);

            registerPacket(PacketDirection.CLIENT, 0x38, PacketOutPlayerListItem.class);

            registerPacket(PacketDirection.CLIENT, 0x39, PacketOutPlayerAbilities.class);

            registerPacket(PacketDirection.CLIENT, 0x3A, PacketOutTabCompleteResponse.class);

            registerPacket(PacketDirection.CLIENT, 0x3F, PacketOutCustomPayload.class);

            registerPacket(PacketDirection.CLIENT, 0x40, PacketOutDisconnect.class);

            registerPacket(PacketDirection.CLIENT, 0x41, PacketOutServerDifficulty.class);

            registerPacket(PacketDirection.CLIENT, 0x45, PacketOutTitle.class);

            registerPacket(PacketDirection.CLIENT, 0x47, PacketOutPlayerListHeaderAndFooter.class);

            registerPacket(PacketDirection.CLIENT, 0x48, PacketOutSendResourcePack.class);
        }
    };

    private final Map<PacketDirection, BiMap<Integer, Class<? extends Packet>>> map;
    private Class<? extends AbstractPacketHandler> packetHandler;

    ConnectionState(Class<? extends AbstractPacketHandler> packetHandler) {
        this.map = new HashMap<>();
        this.packetHandler = packetHandler;
    }

    ConnectionState() {
        this(null);
    }

    public void registerPacket(PacketDirection direction, int packetId, Class<? extends Packet> packetClass) {
        BiMap<Integer, Class<? extends Packet>> map = this.map.computeIfAbsent(direction, k -> HashBiMap.create());

        if (map.containsValue(packetClass)) {
            System.err.println("Can't register 2 Packets with the same Id: " + packetId);
            return;
        }
        map.put(packetId, packetClass);
    }

    public Class<? extends Packet> getPacketClassById(PacketDirection direction, int id) {
        BiMap<Integer, Class<? extends Packet>> map = this.map.get(direction);
        if (map == null) return null;
        return map.get(id);
    }

    public int getIdByPacketClass(PacketDirection direction, Class<? extends Packet> packetClass) {
        BiMap<Integer, Class<? extends Packet>> map = this.map.get(direction);
        if (map == null) return -1;

        return map.inverse().get(packetClass);
    }

    public void unregisterPacket(PacketDirection direction, int id) {
        this.map.get(direction).remove(id);
    }

    public void unregisterPacket(PacketDirection direction, Class packetClass) {
        this.map.get(direction).inverse().remove(packetClass);
    }

    public Class<? extends AbstractPacketHandler> getPacketHandler() {
        return packetHandler;
    }

    public void setPacketHandler(Class<? extends AbstractPacketHandler> packetHandler) {
        this.packetHandler = packetHandler;
    }
}
