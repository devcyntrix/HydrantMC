package org.hydrantmc.api.network;

public interface HandshakeConnection extends Connection {

    int getProtocolVersion();

    void setProtocolVersion(int version);

}
