package org.hydrantmc.api.network;

/**
 * Created by DevCyntrix on Feb, 2018
 */
public interface IncomingPacket<T extends AbstractPacketHandler> extends Packet<T> {

    void readPacket(PacketBuffer buffer);

}
