package org.hydrantmc.api.network;

import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.utils.player.GameProfile;

import java.security.Key;

public interface LoginConnection extends HandshakeConnection {

    /**
     * Der Name, der der Client beim Login Packet an den Server sendet
     */
    String getLoginName();

    /**
     * Schließt die Verbindung mit dem Client
     * @param reason der Grund warum die Verbindung geschlossen wurde
     */
    void disconnect(String reason);

    /**
     * Schließt die Verbindung mit dem Client
     * @param reason der Grund warum die Verbindung geschlossen wurde
     */
    void disconnect(ChatComponent reason);

    void enableCompressionThreshold(int threshold);

    void enableEncryption(Key key);

    void disableEncryption();

    GameProfile getGameProfile();

    void setGameProfile(GameProfile profile);
}
