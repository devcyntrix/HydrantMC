package org.hydrantmc.api.network;

/**
 * Created by DevCyntrix on Feb, 2018
 */
public interface OutgoingPacket<T extends AbstractPacketHandler> extends Packet<T> {

    void writePacket(PacketBuffer buffer);

}
