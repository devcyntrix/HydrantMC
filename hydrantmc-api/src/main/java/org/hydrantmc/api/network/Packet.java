package org.hydrantmc.api.network;

public interface Packet<T extends AbstractPacketHandler> {

    void handlePacket(T handler);

}
