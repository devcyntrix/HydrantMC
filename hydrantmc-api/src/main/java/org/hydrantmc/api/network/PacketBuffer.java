package org.hydrantmc.api.network;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.ByteBufInputStream;
import io.netty.buffer.ByteBufOutputStream;
import io.netty.buffer.Unpooled;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.chat.ChatComponentSerialization;
import org.hydrantmc.api.utils.MathUtils;
import org.hydrantmc.nbt.NBTBase;
import org.hydrantmc.nbt.NBTCompressedStreamTools;
import org.hydrantmc.nbt.NBTTagCompound;

import java.io.IOException;
import java.util.UUID;

/**
 * Created by Cyntrix on 18.07.2017.
 */
public class PacketBuffer {

    private final ByteBuf byteBuf;

    public PacketBuffer() {
        this(Unpooled.buffer());
    }

    public PacketBuffer(ByteBuf byteBuf) {
        this.byteBuf = byteBuf;
    }

    public void writeFixedPointNumberAsInt(double value) {
        writeInt(MathUtils.floor_double(value * 32));
    }

    public void writeFixedPointNumberAsByte(double value) {
        writeByte(MathUtils.floor_double(value * 32));
    }

    public double readFixedPointNumber() {
        return ((double)readInt()) / 32.0;
    }

    public void writeAngle(float value) {
        writeByte((int) (value * 256 / 360));
    }

    public float readAngle() {
        return ((float)readByte()) * 360 / 256;
    }

    public boolean readBoolean() {
        return byteBuf.readBoolean();
    }

    public byte readByte() {
        return byteBuf.readByte();
    }

    public short readUnsignedByte() {
        return byteBuf.readUnsignedByte();
    }

    public short readShort() {
        return byteBuf.readShort();
    }

    public int readUnsignedShort() {
        return byteBuf.readUnsignedShort();
    }

    public int readInt() {
        return byteBuf.readInt();
    }

    public long readUnsignedInt() {
        return byteBuf.readUnsignedInt();
    }

    public long readLong() {
        return byteBuf.readLong();
    }

    public char readChar() {
        return byteBuf.readChar();
    }

    public float readFloat() {
        return byteBuf.readFloat();
    }

    public double readDouble() {
        return byteBuf.readDouble();
    }

    public void readBytes(byte[] bytes) {
        byteBuf.readBytes(bytes);
    }

    public void writeBoolean(boolean b) {
        byteBuf.writeBoolean(b);
    }

    public void writeByte(int i) {
        byteBuf.writeByte(i);
    }

    public void writeShort(int i) {
        byteBuf.writeShort(i);
    }

    public void writeInt(int i) {
        byteBuf.writeInt(i);
    }

    public void writeLong(long l) {
        byteBuf.writeLong(l);
    }

    public void writeChar(int i) {
        byteBuf.writeChar(i);
    }

    public void writeFloat(float v) {
        byteBuf.writeFloat(v);
    }

    public void writeDouble(double v) {
        byteBuf.writeDouble(v);
    }

    public void writeBytes(byte[] bytes) {
        byteBuf.writeBytes(bytes);
    }

    public void writeBytes(byte[] bytes, int i, int i1) {
        byteBuf.writeBytes(bytes, i, i1);
    }

    public void writeChatComponent(ChatComponent component) {
        writeString(ChatComponentSerialization.getGson().toJson(component, component.getClass()));
    }

    public int refCnt() {
        return byteBuf.refCnt();
    }

    public boolean release() {
        return byteBuf.release();
    }

    public boolean release(int i) {
        return byteBuf.release(i);
    }

    public int readVarInteger() {
        int numRead = 0;
        int result = 0;
        byte read;
        do {
            read = readByte();
            int value = (read & 0b01111111);
            result |= (value << (7 * numRead));

            numRead++;
            if (numRead > 5) {
                throw new RuntimeException("VarInt is too big");
            }
        } while ((read & 0b10000000) != 0);

        return result;
    }

    public void writeVarInteger(int value) {
        do {
            byte temp = (byte) (value & 0b01111111);
            // Note: >>> means that the sign bit is shifted with the rest of the number rather than being left alone
            value >>>= 7;
            if (value != 0) {
                temp |= 0b10000000;
            }
            writeByte(temp);
        } while (value != 0);
    }

    public int getVarIntegerSize(int value) {
        for (int i = 1; i < 5; ++i) {
            if ((value & -1 << i * 7) == 0) {
                return i;
            }
        }
        return 5;
    }

    public void writeString(String value) {
        if (value == null) value = "null";
        byte[] bytes = value.getBytes();
        writeVarInteger(bytes.length);
        byteBuf.writeBytes(bytes);
    }

    public String readString() {
        int length = readVarInteger();
        byte[] bytes = new byte[length];
        byteBuf.readBytes(bytes);
        return new String(bytes);
    }

    public void writeByteArray(byte[] value) {
        writeVarInteger(value.length);
        writeBytes(value);
    }

    public byte[] readByteArray() {
        byte[] output = new byte[readVarInteger()];
        readBytes(output);
        return output;
    }

    public UUID readUniqueId() {
        long mostLong = readLong();
        long leastLong = readLong();
        return new UUID(mostLong, leastLong);
    }

    public void writeUniqueId(UUID uniqueId) {
        writeLong(uniqueId.getMostSignificantBits());
        writeLong(uniqueId.getLeastSignificantBits());
    }

    public void writeNBTTagCompound(NBTTagCompound compound) {
        if (compound == null) {
            writeByte(0);
            return;
        }
        try {
            NBTCompressedStreamTools.writeCompressedStream(compound, new ByteBufOutputStream(this.byteBuf));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    public NBTTagCompound readNBTTagCompound() {
        int index = this.byteBuf.readerIndex();
        byte firstByte = readByte();
        if (firstByte == 0) return null;
        this.byteBuf.readerIndex(index);

        NBTBase base = null;
        try {
            base = NBTCompressedStreamTools.readCompressedStream(new ByteBufInputStream(this.byteBuf));
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (!(base instanceof NBTTagCompound)) return null;
        return (NBTTagCompound) base;
    }

    public ByteBuf toByteBuf() {
        return byteBuf;
    }

    public byte[] toByteArray() {
        byte[] bytes = new byte[toByteBuf().readableBytes()];
        readBytes(bytes);
        return bytes;
    }
}
