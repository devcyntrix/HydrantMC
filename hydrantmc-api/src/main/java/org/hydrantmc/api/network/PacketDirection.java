package org.hydrantmc.api.network;

public enum PacketDirection {

    SERVER,
    CLIENT

}
