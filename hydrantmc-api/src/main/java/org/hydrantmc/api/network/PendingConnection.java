package org.hydrantmc.api.network;

public interface PendingConnection extends LoginConnection {

    AbstractPacketHandler getHandler();


}
