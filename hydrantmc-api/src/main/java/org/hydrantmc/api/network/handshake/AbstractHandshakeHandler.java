package org.hydrantmc.api.network.handshake;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.handshake.in.PacketInHandshake;

public abstract class AbstractHandshakeHandler extends AbstractPacketHandler {

    protected AbstractHandshakeHandler(Server server) {
        super(server);
    }

    public abstract void handleIncomingHandshake(PacketInHandshake handshake);

}
