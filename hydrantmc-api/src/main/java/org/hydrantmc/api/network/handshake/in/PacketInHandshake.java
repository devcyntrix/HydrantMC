package org.hydrantmc.api.network.handshake.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.handshake.AbstractHandshakeHandler;

public class PacketInHandshake implements IncomingPacket<AbstractHandshakeHandler> {

    private int protocolId;
    private String hostname;
    private Integer port;
    private Integer nextState;

    public PacketInHandshake() {
    }

    public PacketInHandshake(int protocolId, String hostname, Integer port, Integer nextState) {
        this.protocolId = protocolId;
        this.hostname = hostname;
        this.port = port;
        this.nextState = nextState;
    }

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.protocolId = buffer.readVarInteger();
        this.hostname = buffer.readString();
        this.port = buffer.readUnsignedShort();
        this.nextState = buffer.readVarInteger();
    }

    @Override
    public void handlePacket(AbstractHandshakeHandler handler) {
        handler.handleIncomingHandshake(this);
    }

    public int getProtocolId() {
        return protocolId;
    }

    public void setProtocolId(int protocolId) {
        this.protocolId = protocolId;
    }

    public String getHostname() {
        return hostname;
    }

    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getNextState() {
        return nextState;
    }

    public void setNextState(Integer nextState) {
        this.nextState = nextState;
    }
}
