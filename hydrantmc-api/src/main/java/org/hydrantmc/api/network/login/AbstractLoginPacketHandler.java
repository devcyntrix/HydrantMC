package org.hydrantmc.api.network.login;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.login.in.PacketInEncryptionResponse;
import org.hydrantmc.api.network.login.in.PacketInLoginRequest;
import org.hydrantmc.api.network.login.out.PacketOutCompression;
import org.hydrantmc.api.network.login.out.PacketOutEncryptionRequest;
import org.hydrantmc.api.network.login.out.PacketOutLoginDisconnect;
import org.hydrantmc.api.network.login.out.PacketOutLoginResponse;

public abstract class AbstractLoginPacketHandler extends AbstractPacketHandler {

    protected AbstractLoginPacketHandler(Server server) {
        super(server);
    }

    public abstract void handleOutgoingDisconnect(PacketOutLoginDisconnect disconnect);

    public abstract void handleIncomingLoginRequest(PacketInLoginRequest request);

    public abstract void handleOutgoingEncryptionRequest(PacketOutEncryptionRequest request);

    public abstract void handleIncomingEncryptionResponse(PacketInEncryptionResponse response);

    public abstract void handleOutgoingPacketCompression(PacketOutCompression compression);

    public abstract void handleOutgoingLoginResponse(PacketOutLoginResponse response);

}
