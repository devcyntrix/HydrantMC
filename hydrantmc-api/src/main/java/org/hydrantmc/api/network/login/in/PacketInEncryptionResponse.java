package org.hydrantmc.api.network.login.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.login.AbstractLoginPacketHandler;

public class PacketInEncryptionResponse implements IncomingPacket<AbstractLoginPacketHandler> {

    private byte[] sharedSecretKey;
    private byte[] verifyToken;

    public PacketInEncryptionResponse() {
    }

    public PacketInEncryptionResponse(byte[] sharedSecretKey, byte[] verifyToken) {
        this.sharedSecretKey = sharedSecretKey;
        this.verifyToken = verifyToken;
    }

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.sharedSecretKey = buffer.readByteArray();
        this.verifyToken = buffer.readByteArray();
    }

    @Override
    public void handlePacket(AbstractLoginPacketHandler handler) {
        handler.handleIncomingEncryptionResponse(this);
    }

    public byte[] getSharedSecretKey() {
        return sharedSecretKey;
    }

    public void setSharedSecretKey(byte[] sharedSecretKey) {
        this.sharedSecretKey = sharedSecretKey;
    }

    public byte[] getVerifyToken() {
        return verifyToken;
    }

    public void setVerifyToken(byte[] verifyToken) {
        this.verifyToken = verifyToken;
    }
}
