package org.hydrantmc.api.network.login.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.login.AbstractLoginPacketHandler;

public class PacketInLoginRequest implements IncomingPacket<AbstractLoginPacketHandler> {

    private String username;

    public PacketInLoginRequest() {
    }

    public PacketInLoginRequest(String username) {
        this.username = username;
    }

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.username = buffer.readString();
    }

    @Override
    public void handlePacket(AbstractLoginPacketHandler handler) {
        handler.handleIncomingLoginRequest(this);
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
