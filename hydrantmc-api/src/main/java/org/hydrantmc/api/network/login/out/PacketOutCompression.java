package org.hydrantmc.api.network.login.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.login.AbstractLoginPacketHandler;

public class PacketOutCompression implements OutgoingPacket<AbstractLoginPacketHandler> {

    private int threshold;

    public PacketOutCompression() {
    }

    public PacketOutCompression(int threshold) {
        this.threshold = threshold;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(threshold);
    }

    @Override
    public void handlePacket(AbstractLoginPacketHandler handler) {
        handler.handleOutgoingPacketCompression(this);
    }

    public int getThreshold() {
        return threshold;
    }

    public void setThreshold(int threshold) {
        this.threshold = threshold;
    }
}
