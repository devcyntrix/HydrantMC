package org.hydrantmc.api.network.login.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.login.AbstractLoginPacketHandler;

public class PacketOutEncryptionRequest implements OutgoingPacket<AbstractLoginPacketHandler> {

    private final String serverHash;
    private final byte[] publicKey;
    private final byte[] verifyToken;

    public PacketOutEncryptionRequest(String serverHash, byte[] publicKey, byte[] verifyToken) {
        this.serverHash = serverHash;
        this.publicKey = publicKey;
        this.verifyToken = verifyToken;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(serverHash);
        buffer.writeByteArray(publicKey);
        buffer.writeByteArray(verifyToken);
    }

    @Override
    public void handlePacket(AbstractLoginPacketHandler handler) {
        handler.handleOutgoingEncryptionRequest(this);
    }
}
