package org.hydrantmc.api.network.login.out;

import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.login.AbstractLoginPacketHandler;

public class PacketOutLoginDisconnect implements OutgoingPacket<AbstractLoginPacketHandler> {

    private final ChatComponent kickReason;

    public PacketOutLoginDisconnect(ChatComponent kickReason) {
        this.kickReason = kickReason;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeChatComponent(kickReason);
    }

    @Override
    public void handlePacket(AbstractLoginPacketHandler handler) {
        handler.handleOutgoingDisconnect(this);
    }
}
