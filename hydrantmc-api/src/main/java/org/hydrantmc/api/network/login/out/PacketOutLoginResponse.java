package org.hydrantmc.api.network.login.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.login.AbstractLoginPacketHandler;

import java.util.UUID;

public class PacketOutLoginResponse implements OutgoingPacket<AbstractLoginPacketHandler> {

    private final UUID uniqueId;
    private final String username;

    public PacketOutLoginResponse(UUID uniqueId, String username) {
        this.uniqueId = uniqueId;
        this.username = username;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(uniqueId.toString());
        buffer.writeString(username);
    }

    @Override
    public void handlePacket(AbstractLoginPacketHandler handler) {
        handler.handleOutgoingLoginResponse(this);
    }
}
