package org.hydrantmc.api.network.play;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.play.in.*;
import org.hydrantmc.api.network.play.out.*;
import org.hydrantmc.api.network.play.out.entity.PacketOutEntityRelativeMove;
import org.hydrantmc.api.network.play.out.entity.PacketOutEntityStatus;
import org.hydrantmc.api.network.play.out.entity.PacketOutEntityTeleport;
import org.hydrantmc.api.network.play.out.entity.PacketOutSpawnEntity;
import org.hydrantmc.api.network.play.out.player.PacketOutPlayerAbilities;
import org.hydrantmc.api.network.play.out.player.PacketOutSpawnPlayer;
import org.hydrantmc.api.network.play.out.world.PacketOutBlockChange;
import org.hydrantmc.api.network.play.out.world.PacketOutChunkColumnData;
import org.hydrantmc.api.network.play.out.world.PacketOutMapChunkBulk;

public abstract class AbstractPlayPacketHandler extends AbstractPacketHandler {

    public AbstractPlayPacketHandler(Server server) {
        super(server);
    }

    public abstract void handleOutgoingDisconnect(PacketOutDisconnect disconnect);

    public abstract void handleOutgoingKeepAlive(PacketOutKeepAlive packetOutKeepAlive);

    public abstract void handleIncomingKeepAlive(PacketInKeepAlive keepAlive);

    public abstract void handleOutgoingPlayerJoinGame(PacketOutJoinGame joinGame);

    public abstract void handleOutgoingServerDifficulty(PacketOutServerDifficulty serverDifficulty);

    public abstract void handleOutgoingSpawnPosition(PacketOutSpawnPosition spawnPosition);

    public abstract void handleIncomingClientSettings(PacketInClientSettings clientSettings);

    public abstract void handleOutgoingHeldItemChange(PacketOutHeldItemChange packetOutHeldItemChange);

    public abstract void handleIncomingHeldItemChange(PacketInHeldItemChange packetInHeldItemChange);

    public abstract void handleIncomingPlayerLook(PacketInPlayerLook playerLook);

    public abstract void handleOutgoingPlayerListHeaderAndFooter(PacketOutPlayerListHeaderAndFooter headerAndFooter);


    public abstract void handleOutgoingCustomPayload(PacketOutCustomPayload pluginMessage);

    public abstract void handleIncomingCustomPayload(PacketInCustomPayload pluginMessage);


    public abstract void handleOutgoingMapChunkBulk(PacketOutMapChunkBulk mapChunkBulk);


    public abstract void handleIncomingChatMessage(PacketInChatMessage chatMessage);

    public abstract void handleOutgoingChatMessage(PacketOutChatMessage chatMessage);

    public abstract void handleIncomingPlayerPosLook(PacketInPlayerPosLook packetInPlayerPosLook);

    public abstract void handleIncomingPlayerPosition(PacketInPlayerPosition packetInPlayerPosition);

    public abstract void handleIncomingEntityAction(PacketInEntityAction packetInEntityAction);

    public abstract void handleOutgoingEntityStatus(PacketOutEntityStatus packetOutEntityStatus);

    public abstract void handleOutgoingEntitySpawn(PacketOutSpawnEntity packetOutSpawnEntity);

    public abstract void handleOutgoingPlayerSpawn(PacketOutSpawnPlayer packetOutSpawnPlayer);

    public abstract void handleOutgoingPlayerListItem(PacketOutPlayerListItem packetOutPlayerListItem);

    public abstract void handleOutgoingChunkColumnData(PacketOutChunkColumnData packetOutChunkColumnData);

    public abstract void handleIncomingBlockPlace(PacketInPlaceBlock packetInPlaceBlock);

    public abstract void handleOutgoingBlockChange(PacketOutBlockChange packetOutBlockChange);


    public abstract void handleOutgoingEntityTeleport(PacketOutEntityTeleport packetOutEntityTeleport);

    public abstract void handleOutgoingEntityRelativeMove(PacketOutEntityRelativeMove packetOutEntityRelativeMove);


    public abstract void handleIncomingTabCompleteRequest(PacketInTabCompleteRequest packetInTabCompleteRequest);

    public abstract void handleOutgoingTabCompleteResponse(PacketOutTabCompleteResponse packetOutTabCompleteResponse);


    public abstract void handleOutgoingGameStateChange(PacketOutGameStateChange packetOutGameStateChange);


    public abstract void handleIncomingPlayerDigging(PacketInPlayerDigging packetInPlayerDigging);


    public abstract void handleIncomingAnimation(PacketInAnimation packetInAnimation);

    public abstract void handleOutgoingAnimation(PacketOutAnimation packetOutAnimation);


    public abstract void handleIncomingResourcePackStatus(PacketInResourcePackStatus packetInResourcePackStatus);

    public abstract void handleOutgoingSendResourcePack(PacketOutSendResourcePack packetOutSendResourcePack);


    public abstract void handleIncomingCreativeInventoryAction(PacketInCreativeInventoryAction packetInCreativeInventoryAction);


    public abstract void handleOutgoingPlayerAbilities(PacketOutPlayerAbilities packetOutPlayerAbilities);

    public abstract void handleIncomingPlayerAbilities(PacketInPlayerAbilities packetInPlayerAbilities);

    public abstract void handleIncomingEntityUse(PacketInUseEntity packetInUseEntity);

}
