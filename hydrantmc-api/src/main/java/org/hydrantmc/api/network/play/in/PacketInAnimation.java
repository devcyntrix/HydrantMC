package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInAnimation implements IncomingPacket<AbstractPlayPacketHandler> {

    @Override
    public void readPacket(PacketBuffer buffer) {}

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingAnimation(this);
    }

}
