package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInChatMessage implements IncomingPacket<AbstractPlayPacketHandler> {

    private String message;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.message = buffer.readString();
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingChatMessage(this);
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
