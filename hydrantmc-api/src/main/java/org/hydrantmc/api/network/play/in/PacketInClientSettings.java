package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.player.ChatMode;
import org.hydrantmc.api.utils.player.ClientSettings;
import org.hydrantmc.api.utils.player.SkinParts;

import java.util.LinkedList;
import java.util.List;

public class PacketInClientSettings implements IncomingPacket<AbstractPlayPacketHandler> {

    private ClientSettings clientSettings;

    @Override
    public void readPacket(PacketBuffer buffer) {
        String locale = buffer.readString();
        byte viewDistance = buffer.readByte();
        ChatMode chatMode = ChatMode.values()[buffer.readByte()];
        boolean chatColorsEnabled = buffer.readBoolean();

        short skinParts = buffer.readUnsignedByte();

        List<SkinParts> list = new LinkedList<>();

        for (int part = 0; part < SkinParts.values().length; part++) {
            SkinParts parts = SkinParts.values()[part];
            if ((skinParts & parts.getBitMask()) == 0) continue;
            list.add(parts);
        }
        this.clientSettings = new ClientSettings(locale, viewDistance, chatMode, chatColorsEnabled, list.toArray(new SkinParts[0]));
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingClientSettings(this);
    }

    public ClientSettings getClientSettings() {
        return clientSettings;
    }

    public void setClientSettings(ClientSettings clientSettings) {
        this.clientSettings = clientSettings;
    }
}
