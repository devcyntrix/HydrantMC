package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.item.ItemStack;
import org.hydrantmc.api.item.Material;
import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.nbt.NBTTagCompound;

public class PacketInCreativeInventoryAction implements IncomingPacket<AbstractPlayPacketHandler> {

    private short slot;
    private ItemStack itemStack;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.slot = buffer.readShort();

        short itemId = buffer.readShort();

        if(!(itemId == -1)) {
            byte amount = buffer.readByte();
            short damage = buffer.readShort();
            NBTTagCompound nbt = buffer.readNBTTagCompound();

            this.itemStack = new ItemStack(Material.getById(itemId), amount, damage, nbt);
            return;
        }

        this.itemStack = new ItemStack(null);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingCreativeInventoryAction(this);
    }

    public short getSlot() {
        return slot;
    }

    public void setSlot(short slot) {
        this.slot = slot;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }
}
