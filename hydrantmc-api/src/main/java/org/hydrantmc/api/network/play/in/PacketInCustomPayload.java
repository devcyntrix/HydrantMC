package org.hydrantmc.api.network.play.in;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInCustomPayload implements IncomingPacket<AbstractPlayPacketHandler> {

    private String channel;
    private PacketBuffer data;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.channel = buffer.readString();

        ByteBuf dataAsBuf = buffer.toByteBuf();

        byte[] bytes = new byte[dataAsBuf.readableBytes()];
        dataAsBuf.readBytes(bytes);
        this.data = new PacketBuffer(Unpooled.wrappedBuffer(bytes));
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingCustomPayload(this);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public PacketBuffer getData() {
        return data;
    }

    public void setData(PacketBuffer data) {
        this.data = data;
    }
}
