package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInEntityAction implements IncomingPacket<AbstractPlayPacketHandler> {

    private int entityId;
    private int actionId;
    private int actionParameter;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.entityId = buffer.readVarInteger();
        this.actionId = buffer.readVarInteger();
        this.actionParameter = buffer.readVarInteger();
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingEntityAction(this);
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public int getActionParameter() {
        return actionParameter;
    }

    public void setActionParameter(int actionParameter) {
        this.actionParameter = actionParameter;
    }
}
