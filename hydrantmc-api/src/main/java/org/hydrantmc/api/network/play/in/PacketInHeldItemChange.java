package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInHeldItemChange implements IncomingPacket<AbstractPlayPacketHandler> {

    private short slot;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.slot = buffer.readShort();
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingHeldItemChange(this);
    }

    public short getSlot() {
        return slot;
    }

    public void setSlot(short slot) {
        this.slot = slot;
    }
}
