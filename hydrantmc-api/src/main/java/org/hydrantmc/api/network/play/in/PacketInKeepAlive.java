package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInKeepAlive implements IncomingPacket<AbstractPlayPacketHandler> {

    private int keepAliveId;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.keepAliveId = buffer.readVarInteger();
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingKeepAlive(this);
    }

    public int getKeepAliveId() {
        return keepAliveId;
    }

    public void setKeepAliveId(int keepAliveId) {
        this.keepAliveId = keepAliveId;
    }
}
