package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.item.ItemStack;
import org.hydrantmc.api.item.Material;
import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Facing;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.nbt.NBTTagCompound;

public class PacketInPlaceBlock implements IncomingPacket<AbstractPlayPacketHandler> {

    private Location location;
    private Facing facing;

    private ItemStack itemStack;

    private byte cursorX;
    private byte cursorY;
    private byte cursorZ;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.location = Location.fromLong(buffer.readLong());

        byte facingByte = buffer.readByte();

        if (facingByte != -1) {
            this.facing = Facing.values()[facingByte];
        }

        short itemId = buffer.readShort();

        byte amount = 0;
        short damage = 0;
        NBTTagCompound nbt = null;

        if (itemId > 0) {
            amount = buffer.readByte();
            damage = buffer.readShort();
            nbt = buffer.readNBTTagCompound();
        }

        this.itemStack = new ItemStack(Material.getById(itemId), amount, damage, nbt);

        //if(itemId <= -1) return;
        this.cursorX = buffer.readByte();
        this.cursorY = buffer.readByte();
        this.cursorZ = buffer.readByte();
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingBlockPlace(this);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Facing getFacing() {
        return facing;
    }

    public void setFacing(Facing facing) {
        this.facing = facing;
    }

    public ItemStack getItemStack() {
        return itemStack;
    }

    public void setItemStack(ItemStack itemStack) {
        this.itemStack = itemStack;
    }

    public byte getCursorX() {
        return cursorX;
    }

    public void setCursorX(byte cursorX) {
        this.cursorX = cursorX;
    }

    public byte getCursorY() {
        return cursorY;
    }

    public void setCursorY(byte cursorY) {
        this.cursorY = cursorY;
    }

    public byte getCursorZ() {
        return cursorZ;
    }

    public void setCursorZ(byte cursorZ) {
        this.cursorZ = cursorZ;
    }
}
