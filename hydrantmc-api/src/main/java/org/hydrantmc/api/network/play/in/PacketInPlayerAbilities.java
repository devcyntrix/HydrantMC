package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.player.PlayerAbilities;

public class PacketInPlayerAbilities implements IncomingPacket<AbstractPlayPacketHandler> {

    private PlayerAbilities playerAbilities;

    @Override
    public void readPacket(PacketBuffer buffer) {
        byte bitMask = buffer.readByte();

        this.playerAbilities = new PlayerAbilities(false, false, false, false, buffer.readFloat(), buffer.readFloat());
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingPlayerAbilities(this);
    }

    public PlayerAbilities getPlayerAbilities() {
        return playerAbilities;
    }

    public void setPlayerAbilities(PlayerAbilities playerAbilities) {
        this.playerAbilities = playerAbilities;
    }

}
