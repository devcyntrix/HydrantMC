package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Facing;
import org.hydrantmc.api.utils.Location;

public class PacketInPlayerDigging implements IncomingPacket<AbstractPlayPacketHandler> {

    private byte status;
    private Location location;
    private Facing facing;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.status = buffer.readByte();
        long location = buffer.readLong();
        byte facingValue = buffer.readByte();

        switch (status) {
            case 3:
            case 4:
            case 5:
                this.location = null;
                this.facing = null;
                return;
            default:
                this.location = Location.fromLong(location);
                this.facing = Facing.values()[facingValue];
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingPlayerDigging(this);
    }

    public byte getStatus() {
        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Facing getFacing() {
        return facing;
    }

    public void setFacing(Facing facing) {
        this.facing = facing;
    }
}
