package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInPlayerLook extends PacketInPlayerOnGround {

    private float yaw;
    private float pitch;

    private boolean onGround;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.yaw = buffer.readFloat();
        this.pitch = buffer.readFloat();

        this.onGround = buffer.readBoolean();
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingPlayerLook(this);
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public boolean isOnGround() {
        return onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }
}
