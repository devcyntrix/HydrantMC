package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInPlayerOnGround implements IncomingPacket<AbstractPlayPacketHandler> {

    private boolean onGround;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.onGround = buffer.readBoolean();
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

    public boolean isOnGround() {
        return onGround;
    }

    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }
}
