package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInPlayerPosLook extends PacketInPlayerOnGround {

    private double x;
    private double y;
    private double z;

    private float yaw;
    private float pitch;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.x = buffer.readDouble();
        this.y = buffer.readDouble();
        this.z = buffer.readDouble();
        this.yaw = buffer.readFloat();
        this.pitch = buffer.readFloat();

        super.readPacket(buffer);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingPlayerPosLook(this);
    }

    public double getX() {
        return x;
    }

    public void setX(double x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public void setY(double y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public void setZ(double z) {
        this.z = z;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }
}
