package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.ResourcePackStatus;

public class PacketInResourcePackStatus implements IncomingPacket<AbstractPlayPacketHandler> {

    private String hash;
    private ResourcePackStatus result;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.hash = buffer.readString();
        this.result = ResourcePackStatus.values()[buffer.readVarInteger()];
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingResourcePackStatus(this);
    }

    public String getHash() {
        return hash;
    }

    public ResourcePackStatus getResult() {
        return result;
    }

}
