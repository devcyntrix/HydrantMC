package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

public class PacketInTabCompleteRequest implements IncomingPacket<AbstractPlayPacketHandler> {

    private String text;
    private boolean hasPosition;
    private Location blockLocation;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.text = buffer.readString();
        this.hasPosition = buffer.readBoolean();
        if (hasPosition) this.blockLocation = Location.fromLong(buffer.readLong());
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleIncomingTabCompleteRequest(this);
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean hasPosition() {
        return hasPosition;
    }

    public void setHasPosition(boolean hasPosition) {
        this.hasPosition = hasPosition;
    }

    public Location getBlockLocation() {
        return blockLocation;
    }

    public void setBlockLocation(Location blockLocation) {
        this.blockLocation = blockLocation;
    }
}
