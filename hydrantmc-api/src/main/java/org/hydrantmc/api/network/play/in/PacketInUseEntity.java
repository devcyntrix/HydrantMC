package org.hydrantmc.api.network.play.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketInUseEntity implements IncomingPacket<AbstractPlayPacketHandler> {

    private int target;
    private int type;
    private float x;
    private float y;
    private float z;

    public PacketInUseEntity() {
    }

    public PacketInUseEntity(int target, int type, float x, float y, float z) {
        this.target = target;
        this.type = type;
        this.x = x;
        this.y = y;
        this.z = z;
    }

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.target = buffer.readVarInteger();
        this.type = buffer.readVarInteger();
        if(type == 2) {
            this.x = buffer.readFloat();
            this.y = buffer.readFloat();
            this.z = buffer.readFloat();
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {

    }

    public int getTarget() {
        return target;
    }

    public int getType() {
        return type;
    }

    public float getX() {
        return x;
    }

    public float getY() {
        return y;
    }

    public float getZ() {
        return z;
    }

}
