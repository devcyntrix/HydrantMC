package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutAnimation implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entityId;
    private byte animation;

    public PacketOutAnimation(int entityId, byte animation) {
        this.entityId = entityId;
        this.animation = animation;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityId);
        buffer.writeByte(this.animation);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingAnimation(this);
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public byte getAnimation() {
        return animation;
    }

    public void setAnimation(byte animation) {
        this.animation = animation;
    }
}
