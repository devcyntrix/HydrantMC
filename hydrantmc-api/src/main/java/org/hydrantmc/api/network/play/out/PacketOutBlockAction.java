package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

public class PacketOutBlockAction implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final Location location;
    private final byte byte1;
    private final byte byte2;
    private final int blockId;

    public PacketOutBlockAction(Location location, byte byte1, byte byte2, int blockId) {
        this.location = location;
        this.byte1 = byte1;
        this.byte2 = byte2;
        this.blockId = blockId;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeLong(location.toLong());
        buffer.writeByte(byte1);
        buffer.writeByte(byte2);
        buffer.writeVarInteger(blockId);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {

    }

}
