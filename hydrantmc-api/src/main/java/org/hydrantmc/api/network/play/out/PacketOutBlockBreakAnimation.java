package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

public class PacketOutBlockBreakAnimation implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entityID;
    private Location location;
    private byte destroyStage;

    public PacketOutBlockBreakAnimation(int entityID, Location location, byte destroyStage) {
        this.entityID = entityID;
        this.location = location;
        this.destroyStage = destroyStage;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityID);
        buffer.writeLong(this.location.toLong());
        buffer.writeByte(this.destroyStage);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        // FIXME
    }

    public int getEntityID() {
        return entityID;
    }

    public void setEntityID(int entityID) {
        this.entityID = entityID;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public byte getDestroyStage() {
        return destroyStage;
    }

    public void setDestroyStage(byte destroyStage) {
        this.destroyStage = destroyStage;
    }
}
