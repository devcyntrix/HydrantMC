package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.chat.ChatMessageType;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutChatMessage implements OutgoingPacket<AbstractPlayPacketHandler> {

    private ChatComponent component;
    private ChatMessageType messageType;

    public PacketOutChatMessage(ChatComponent component, ChatMessageType messageType) {
        this.component = component;
        this.messageType = messageType;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeChatComponent(this.component);
        buffer.writeByte(this.messageType.ordinal());
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingChatMessage(this);
    }

    public ChatComponent getComponent() {
        return component;
    }

    public void setComponent(ChatComponent component) {
        this.component = component;
    }

    public ChatMessageType getMessageType() {
        return messageType;
    }

    public void setMessageType(ChatMessageType messageType) {
        this.messageType = messageType;
    }
}
