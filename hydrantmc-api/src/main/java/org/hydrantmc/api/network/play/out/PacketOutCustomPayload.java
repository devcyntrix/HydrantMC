package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutCustomPayload implements OutgoingPacket<AbstractPlayPacketHandler> {

    private String channel;
    private byte[] data;

    public PacketOutCustomPayload(String channel, byte[] data) {
        this.channel = channel;
        this.data = data;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(this.channel);
        buffer.writeByteArray(this.data);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingCustomPayload(this);
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}
