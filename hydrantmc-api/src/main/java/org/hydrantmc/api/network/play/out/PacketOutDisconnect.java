package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutDisconnect implements OutgoingPacket<AbstractPlayPacketHandler> {

    private ChatComponent kickReason;

    public PacketOutDisconnect(ChatComponent kickReason) {
        this.kickReason = kickReason;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeChatComponent(this.kickReason);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingDisconnect(this);
    }

    public ChatComponent getKickReason() {
        return kickReason;
    }

    public void setKickReason(ChatComponent kickReason) {
        this.kickReason = kickReason;
    }
}
