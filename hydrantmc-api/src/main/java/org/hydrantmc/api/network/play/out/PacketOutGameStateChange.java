package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.GameStateChangeReason;
import org.hydrantmc.api.utils.GameStateChangeValue;

public class PacketOutGameStateChange implements OutgoingPacket<AbstractPlayPacketHandler> {

    private byte reason;
    private float value;

    public PacketOutGameStateChange(byte reason, float value) {
        this.reason = reason;
        this.value = value;
    }

    public PacketOutGameStateChange(GameStateChangeReason reason, float value) {
        this(reason.getReasonCode(), value);
    }

    public PacketOutGameStateChange(GameStateChangeReason reason, GameStateChangeValue value) {
        this(reason, value.getValue());
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeByte(this.reason);
        buffer.writeFloat(this.value);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingGameStateChange(this);
    }

    public byte getReason() {
        return reason;
    }

    public void setReason(byte reason) {
        this.reason = reason;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

}
