package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutHeldItemChange implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int index;

    public PacketOutHeldItemChange(int index) {
        if (index < 0 || index > 8)
            throw new IndexOutOfBoundsException("Can not be higher then 8 and not lower then 0");
        this.index = index;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeByte((byte) this.index);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingHeldItemChange(this);
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }
}
