package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.world.TerrainType;
import org.hydrantmc.api.world.WorldType;

public class PacketOutJoinGame implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entityId;
    private byte gameMode;
    private WorldType worldType;
    private byte difficulty;
    private byte tabSize;
    private TerrainType terrainType;
    private boolean reducedDebug;

    public PacketOutJoinGame(int entityId, byte gameMode, WorldType worldType, byte difficulty, byte tabSize, TerrainType terrainType, boolean reducedDebug) {
        this.entityId = entityId;
        this.gameMode = gameMode;
        this.worldType = worldType;
        this.difficulty = difficulty;
        this.tabSize = tabSize;
        this.terrainType = terrainType;
        this.reducedDebug = reducedDebug;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeInt(this.entityId);
        buffer.writeByte(this.gameMode);
        buffer.writeVarInteger(this.worldType.getId());
        buffer.writeByte(this.difficulty);
        buffer.writeByte(this.tabSize);
        buffer.writeString(this.terrainType.name().toLowerCase());
        buffer.writeBoolean(this.reducedDebug);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingPlayerJoinGame(this);
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public byte getGameMode() {
        return gameMode;
    }

    public void setGameMode(byte gameMode) {
        this.gameMode = gameMode;
    }

    public WorldType getWorldType() {
        return worldType;
    }

    public void setWorldType(WorldType worldType) {
        this.worldType = worldType;
    }

    public byte getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(byte difficulty) {
        this.difficulty = difficulty;
    }

    public byte getTabSize() {
        return tabSize;
    }

    public void setTabSize(byte tabSize) {
        this.tabSize = tabSize;
    }

    public TerrainType getTerrainType() {
        return terrainType;
    }

    public void setTerrainType(TerrainType terrainType) {
        this.terrainType = terrainType;
    }

    public boolean isReducedDebug() {
        return reducedDebug;
    }

    public void setReducedDebug(boolean reducedDebug) {
        this.reducedDebug = reducedDebug;
    }
}
