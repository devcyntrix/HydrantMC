package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutKeepAlive implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int keepAliveId;

    public PacketOutKeepAlive(int keepAliveId) {
        this.keepAliveId = keepAliveId;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.keepAliveId);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingKeepAlive(this);
    }

    public int getKeepAliveId() {
        return keepAliveId;
    }

    public void setKeepAliveId(int keepAliveId) {
        this.keepAliveId = keepAliveId;
    }
}
