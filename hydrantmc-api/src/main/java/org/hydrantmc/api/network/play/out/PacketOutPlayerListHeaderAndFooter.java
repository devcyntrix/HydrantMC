package org.hydrantmc.api.network.play.out;

import com.google.gson.Gson;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutPlayerListHeaderAndFooter implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final Gson gson = new Gson();

    private ChatComponent header;
    private ChatComponent footer;

    public PacketOutPlayerListHeaderAndFooter(ChatComponent header, ChatComponent footer) {
        this.header = header;
        this.footer = footer;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeChatComponent(this.header);
        buffer.writeChatComponent(this.footer);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingPlayerListHeaderAndFooter(this);
    }

    public ChatComponent getHeader() {
        return header;
    }

    public void setHeader(ChatComponent header) {
        this.header = header;
    }

    public ChatComponent getFooter() {
        return footer;
    }

    public void setFooter(ChatComponent footer) {
        this.footer = footer;
    }
}
