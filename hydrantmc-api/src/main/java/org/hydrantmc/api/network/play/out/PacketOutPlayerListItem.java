package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.tablist.TabListItem;
import org.hydrantmc.api.utils.player.GameProfile;
import org.hydrantmc.api.utils.player.Property;
import org.hydrantmc.api.utils.player.PropertyMap;

public class PacketOutPlayerListItem implements OutgoingPacket<AbstractPlayPacketHandler> {

    private Action action;
    private TabListItem[] items;

    public PacketOutPlayerListItem(Action action, TabListItem... playerData) {
        this.action = action;
        this.items = playerData;
    }

    public PacketOutPlayerListItem() {
    }

    public PacketOutPlayerListItem(Action action) {
        this.action = action;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.action.ordinal());
        buffer.writeVarInteger(this.items.length);

        for (TabListItem item : this.items) {
            GameProfile profile = item.getGameProfile();
            switch (this.action) {
                case ADD_PLAYER:
                    PropertyMap propertyMap = profile.getPropertyMap();

                    buffer.writeUniqueId(profile.getUniqueId());
                    buffer.writeString(profile.getUsername());

                    buffer.writeVarInteger(propertyMap.size());

                    for (Property property : propertyMap.values()) {
                        buffer.writeString(property.getName());
                        buffer.writeString(property.getValue());

                        boolean signature = property.hasSignature();
                        buffer.writeBoolean(signature);
                        if (signature) {
                            buffer.writeString(property.getSignature());
                        }
                    }

                    buffer.writeVarInteger(item.getGameMode().ordinal());
                    buffer.writeVarInteger(item.getPing());

                    boolean hasDisplayName = item.getDisplayName() != null;
                    buffer.writeBoolean(hasDisplayName);

                    if (hasDisplayName) {
                        buffer.writeChatComponent(item.getDisplayName());
                    }
                    break;
                case REMOVE_PLAYER:
                    buffer.writeUniqueId(profile.getUniqueId());
                    break;
            }
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingPlayerListItem(this);
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public enum Action {
        ADD_PLAYER, UPDATE_GAME_MODE, UPDATE_LATENCY, UPDATE_DISPLAY_NAME, REMOVE_PLAYER
    }

}
