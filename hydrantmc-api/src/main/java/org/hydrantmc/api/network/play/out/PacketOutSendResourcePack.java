package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutSendResourcePack implements OutgoingPacket<AbstractPlayPacketHandler> {

    private String url;
    private String sha1Hash;

    public PacketOutSendResourcePack(String url, String sha1Hash) {
        this.url = url;
        this.sha1Hash = sha1Hash;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(this.url);
        buffer.writeString(this.sha1Hash);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingSendResourcePack(this);
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSha1Hash() {
        return sha1Hash;
    }

    public void setSha1Hash(String sha1Hash) {
        this.sha1Hash = sha1Hash;
    }

}
