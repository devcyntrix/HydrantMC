package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.world.Difficulty;

public class PacketOutServerDifficulty implements OutgoingPacket<AbstractPlayPacketHandler> {

    private Difficulty difficulty;

    public PacketOutServerDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeByte(this.difficulty.ordinal());
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingServerDifficulty(this);
    }

    public Difficulty getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

}
