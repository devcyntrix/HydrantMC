package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.Sound;

public class PacketOutSoundEffect implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final String sound;
    private final Location location;
    private final float volume;
    private final float pitch;

    public PacketOutSoundEffect(Sound sound, Location location, float volume, float pitch) {
        this(sound.getName(), location, volume, pitch);
    }

    public PacketOutSoundEffect(String sound, Location location, float volume, float pitch) {
        this.sound = sound;
        this.location = location;
        this.volume = volume;
        this.pitch = pitch;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(sound);
        buffer.writeInt(location.getBlockX() * 8);
        buffer.writeInt(location.getBlockY() * 8);
        buffer.writeInt(location.getBlockZ() * 8);
        buffer.writeFloat(volume);
        buffer.writeByte((int) pitch * 63);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {

    }

}
