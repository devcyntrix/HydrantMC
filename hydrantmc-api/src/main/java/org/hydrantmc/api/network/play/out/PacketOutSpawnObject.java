package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.MathUtils;

public class PacketOutSpawnObject implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entityId;
    private byte type;
    private Location location;
    private final int data;

    private short velocityX;
    private short velocityY;
    private short velocityZ;

    public PacketOutSpawnObject(int entityId, byte type, Location location) {
        this.entityId = entityId;
        this.type = type;
        this.location = location;
        this.data = 0;
    }

    public PacketOutSpawnObject(int entityId, byte type, Location location, int data, short velocityX, short velocityY, short velocityZ) {
        this.entityId = entityId;
        this.type = type;
        this.location = location;
        this.data = data;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.velocityZ = velocityZ;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityId);
        buffer.writeByte(this.type);
        buffer.writeInt(MathUtils.floor_double(this.location.getX() * 32));
        buffer.writeInt(MathUtils.floor_double(this.location.getY() * 32));
        buffer.writeInt(MathUtils.floor_double(this.location.getZ() * 32));
        buffer.writeByte((int) (this.location.getYaw() / 256));
        buffer.writeByte((int) (this.location.getPitch() / 256));
        buffer.writeInt(this.data);

        if(this.data > 0) {
            buffer.writeShort(this.velocityX);
            buffer.writeShort(this.velocityY);
            buffer.writeShort(this.velocityZ);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
