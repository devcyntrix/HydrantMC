package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

public class PacketOutSpawnPosition implements OutgoingPacket<AbstractPlayPacketHandler> {

    private Location location;

    public PacketOutSpawnPosition(Location location) {
        this.location = location;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeLong(this.location.toLong());
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingSpawnPosition(this);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
