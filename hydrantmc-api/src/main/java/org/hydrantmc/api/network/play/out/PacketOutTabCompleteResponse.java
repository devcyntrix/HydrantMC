package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutTabCompleteResponse implements OutgoingPacket<AbstractPlayPacketHandler> {

    private String[] suggestions;

    public PacketOutTabCompleteResponse(String... suggestions) {
        this.suggestions = suggestions;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(suggestions.length);

        for (String string : suggestions) {
            buffer.writeString(string);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingTabCompleteResponse(this);
    }

    public String[] getSuggestions() {
        return suggestions;
    }

    public void setSuggestions(String[] suggestions) {
        this.suggestions = suggestions;
    }

}
