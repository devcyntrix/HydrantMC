package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.TitleAction;

public class PacketOutTitle implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final byte action;
    private ChatComponent title;
    private ChatComponent subtitle;
    private int fadeIn;
    private int stay;
    private int fadeOut;

    public PacketOutTitle(TitleAction action, ChatComponent component) {
        this.action = (byte) action.ordinal();

        if(action.ordinal() == 0) this.title = component;
        else this.subtitle = component;
    }

    public PacketOutTitle(int fadeIn, int stay, int fadeOut) {
        this.action = (byte) TitleAction.TIMES.ordinal();

        this.fadeIn = fadeIn;
        this.stay = stay;
        this.fadeOut = fadeOut;
    }

    public PacketOutTitle(TitleAction action) {
        this.action = (byte) action.ordinal();
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeByte(action);

        switch(action) {
            case 0:
                buffer.writeChatComponent(title);
                break;
            case 1:
                buffer.writeChatComponent(subtitle);
                break;
            case 2:
                buffer.writeInt(fadeIn);
                buffer.writeInt(stay);
                buffer.writeInt(fadeOut);
                break;
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

}
