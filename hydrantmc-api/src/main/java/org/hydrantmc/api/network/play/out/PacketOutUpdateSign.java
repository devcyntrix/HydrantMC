package org.hydrantmc.api.network.play.out;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

public class PacketOutUpdateSign implements OutgoingPacket<AbstractPlayPacketHandler> {

    private Location location;
    private ChatComponent[] lines;

    public PacketOutUpdateSign(Location location, ChatComponent[] lines) {
        this.location = location;
        Preconditions.checkArgument(lines.length == 4, "lines array cannot be larger or smaller than 4");
        this.lines = lines;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeLong(this.location.toLong());

        for(ChatComponent component : this.lines) {
            buffer.writeChatComponent(component);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        // FIXME
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public ChatComponent[] getLines() {
        return lines;
    }

    public void setLines(ChatComponent[] lines) {
        this.lines = lines;
    }

}
