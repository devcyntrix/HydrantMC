package org.hydrantmc.api.network.play.out;

import org.hydrantmc.api.item.ItemStack;
import org.hydrantmc.api.item.Material;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutWindowSetSlot implements OutgoingPacket<AbstractPlayPacketHandler> {

    private byte windowId;
    private short slot;
    private ItemStack itemStack;

    public PacketOutWindowSetSlot(byte windowId, short slot, ItemStack itemStack) {
        this.windowId = windowId;
        this.slot = slot;
        this.itemStack = itemStack;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeByte(windowId);
        buffer.writeShort(slot);
        if(itemStack.getMaterial() != null) {
            buffer.writeShort(itemStack.getMaterial().getId());
            buffer.writeByte(itemStack.getAmount());
            buffer.writeShort(itemStack.getDamage());

            if(itemStack.getMaterial() != Material.AIR) buffer.writeNBTTagCompound(itemStack.getTagCompound());
        } else {
            buffer.writeShort(-1);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

}
