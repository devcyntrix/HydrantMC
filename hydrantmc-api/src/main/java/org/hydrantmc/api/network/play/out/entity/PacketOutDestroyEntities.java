package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

/**
 * This packet destroys the entities on client side
 * @author DevCyntrix
 * @packetId 0x13
 */
public class PacketOutDestroyEntities implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int[] entityIds;

    public PacketOutDestroyEntities(int... entityIds) {
        this.entityIds = entityIds;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityIds.length);
        for(int entityId : this.entityIds) {
            buffer.writeVarInteger(entityId);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

    public int[] getEntityIds() {
        return entityIds;
    }

    public void setEntityIds(int[] entityIds) {
        this.entityIds = entityIds;
    }
}
