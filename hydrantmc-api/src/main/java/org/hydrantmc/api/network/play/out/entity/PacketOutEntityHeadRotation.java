package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutEntityHeadRotation implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final int entityId;
    private final float headYaw;

    public PacketOutEntityHeadRotation(int entityId, float headYaw) {
        this.entityId = entityId;
        this.headYaw = headYaw;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(entityId);
        buffer.writeAngle(this.headYaw);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {

    }
}
