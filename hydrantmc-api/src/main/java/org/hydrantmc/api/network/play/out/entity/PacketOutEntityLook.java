package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutEntityLook implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final int entityId;
    private final float yaw;
    private final float pitch;
    private final boolean onGround;

    public PacketOutEntityLook(int entityId, float yaw, float pitch, boolean onGround) {
        this.entityId = entityId;
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityId);
        buffer.writeAngle(this.yaw);
        buffer.writeAngle(this.pitch);
        buffer.writeBoolean(this.onGround);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {

    }
}
