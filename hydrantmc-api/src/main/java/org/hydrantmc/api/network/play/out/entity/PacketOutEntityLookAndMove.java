package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutEntityLookAndMove implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final int entityId;

    private final double deltaX;
    private final double deltaY;
    private final double deltaZ;

    private final float yaw;
    private final float pitch;

    private final boolean onGround;

    public PacketOutEntityLookAndMove(int entityId, double deltaX, double deltaY, double deltaZ, float yaw, float pitch, boolean onGround) {
        this.entityId = entityId;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.deltaZ = deltaZ;
        this.yaw = yaw;
        this.pitch = pitch;
        this.onGround = onGround;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityId);
        buffer.writeFixedPointNumberAsByte(this.deltaX);
        buffer.writeFixedPointNumberAsByte(this.deltaY);
        buffer.writeFixedPointNumberAsByte(this.deltaZ);
        buffer.writeAngle(this.yaw);
        buffer.writeAngle(this.pitch);
        buffer.writeBoolean(this.onGround);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }
}
