package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.EntityProperty;

public class PacketOutEntityProperties implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entityId;
    private EntityProperty[] entityProperties;

    public PacketOutEntityProperties(int entityId, EntityProperty... entityProperties) {
        this.entityId = entityId;
        this.entityProperties = entityProperties;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(entityId);
        buffer.writeInt(entityProperties.length);

        for(EntityProperty property : entityProperties) {
            buffer.writeString(property.getKey());
            buffer.writeDouble(property.getValue());
            buffer.writeVarInteger(property.getModifiers().length);

            for(EntityProperty.AttributeModifier modifier : property.getModifiers()) {
                //TODO: Write Attribute modifiers
            }
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

}
