package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutEntityRelativeMove implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final int entityId;
    private final double deltaX;
    private final double deltaY;
    private final double deltaZ;
    private final boolean onGround;

    public PacketOutEntityRelativeMove(int entityId, double deltaX, double deltaY, double deltaZ, boolean onGround) {
        this.entityId = entityId;
        this.deltaX = deltaX;
        this.deltaY = deltaY;
        this.deltaZ = deltaZ;
        this.onGround = onGround;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(entityId);
        buffer.writeFixedPointNumberAsByte(deltaX);
        buffer.writeFixedPointNumberAsByte(deltaY);
        buffer.writeFixedPointNumberAsByte(deltaZ);
        buffer.writeBoolean(onGround);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingEntityRelativeMove(this);
    }


}
