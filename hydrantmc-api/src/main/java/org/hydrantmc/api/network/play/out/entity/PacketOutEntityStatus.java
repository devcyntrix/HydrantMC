package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutEntityStatus implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entityId;
    private byte entityStatus;

    public PacketOutEntityStatus(int entityId, byte entityStatus) {
        this.entityId = entityId;
        this.entityStatus = entityStatus;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeInt(this.entityId);
        buffer.writeByte(this.entityStatus);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingEntityStatus(this);
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public byte getEntityStatus() {
        return entityStatus;
    }

    public void setEntityStatus(byte entityStatus) {
        this.entityStatus = entityStatus;
    }

}
