package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

public class PacketOutEntityTeleport implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final int entityId;
    private final Location location;
    private final boolean onGround;

    public PacketOutEntityTeleport(int entityId, Location location, boolean onGround) {
        this.entityId = entityId;
        this.location = location;
        this.onGround = onGround;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(entityId);
        buffer.writeFixedPointNumberAsInt(location.getX());
        buffer.writeFixedPointNumberAsInt(location.getY());
        buffer.writeFixedPointNumberAsInt(location.getZ());
        buffer.writeAngle(location.getYaw());
        buffer.writeAngle(location.getPitch());
        buffer.writeBoolean(onGround);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingEntityTeleport(this);
    }

}
