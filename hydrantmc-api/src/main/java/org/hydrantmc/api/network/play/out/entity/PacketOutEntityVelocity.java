package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

/**
 * This packet adds a velocity to an entity
 * @author DevCyntrix
 * @packetId 0x12
 * Here are more descriptions about the packet:
 * @see <a href="http://wiki.vg/index.php?title=Protocol&oldid=7368#Entity_Velocity">http://wiki.vg/index.php?title=Protocol&oldid=7368#Entity_Velocity</a>
 */
public class PacketOutEntityVelocity implements OutgoingPacket<AbstractPlayPacketHandler> {

    private final int entityId;
    private final short velocityX;
    private final short velocityY;
    private final short velocityZ;

    public PacketOutEntityVelocity(int entityId, short velocityX, short velocityY, short velocityZ) {
        this.entityId = entityId;
        this.velocityX = velocityX;
        this.velocityY = velocityY;
        this.velocityZ = velocityZ;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityId);
        buffer.writeShort(this.velocityX);
        buffer.writeShort(this.velocityY);
        buffer.writeShort(this.velocityZ);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

}
