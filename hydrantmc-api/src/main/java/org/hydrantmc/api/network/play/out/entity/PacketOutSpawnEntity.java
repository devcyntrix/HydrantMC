package org.hydrantmc.api.network.play.out.entity;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.MathUtils;

public class PacketOutSpawnEntity implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entitiyId;
    private byte type;
    private Location location;

    public PacketOutSpawnEntity(int entityId, byte type, Location location) {
        this.entitiyId = entityId;
        this.type = type;
        this.location = location;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entitiyId);
        buffer.writeByte(this.type);
        buffer.writeInt(MathUtils.floor_double(this.location.getX() * 32));
        buffer.writeInt(MathUtils.floor_double(this.location.getY() * 32));
        buffer.writeInt(MathUtils.floor_double(this.location.getZ() * 32));
        buffer.writeByte((int) (this.location.getYaw() / 256));
        buffer.writeByte((int) (this.location.getPitch() / 256));
        buffer.writeByte(0 / 256);
        buffer.writeShort(0);
        buffer.writeShort(0);
        buffer.writeShort(0);
        //TODO: Write Metadata

        buffer.writeByte(127);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingEntitySpawn(this);
    }

    public int getEntitiyId() {
        return entitiyId;
    }

    public void setEntitiyId(int entitiyId) {
        this.entitiyId = entitiyId;
    }

    public byte getType() {
        return type;
    }

    public void setType(byte type) {
        this.type = type;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

}
