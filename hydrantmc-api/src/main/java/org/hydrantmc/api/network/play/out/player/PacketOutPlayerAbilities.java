package org.hydrantmc.api.network.play.out.player;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.player.PlayerAbilities;

public class PacketOutPlayerAbilities implements OutgoingPacket<AbstractPlayPacketHandler> {

    private PlayerAbilities playerAbilities;

    public PacketOutPlayerAbilities(PlayerAbilities playerAbilities) {
        this.playerAbilities = playerAbilities;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        byte b = 0;

        if (this.playerAbilities.isInvulnerable()) {
            b = (byte) (b | 1);
        }

        if (this.playerAbilities.isFlying()) {
            b = (byte) (b | 2);
        }

        if (this.playerAbilities.isAllowFlying()) {
            b = (byte) (b | 4);
        }

        if (this.playerAbilities.isCreativeMode()) {
            b = (byte) (b | 8);
        }

        buffer.writeByte(b);
        buffer.writeFloat(this.playerAbilities.getFlySpeed());
        buffer.writeFloat(this.playerAbilities.getWalkSpeed());

    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingPlayerAbilities(this);
    }

    public PlayerAbilities getPlayerAbilities() {
        return playerAbilities;
    }

    public void setPlayerAbilities(PlayerAbilities playerAbilities) {
        this.playerAbilities = playerAbilities;
    }

}
