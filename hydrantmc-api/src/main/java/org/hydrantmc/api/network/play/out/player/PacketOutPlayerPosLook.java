package org.hydrantmc.api.network.play.out.player;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

/**
 * Created by Cyntrix on 12/20/2017.
 */
public class PacketOutPlayerPosLook implements OutgoingPacket<AbstractPlayPacketHandler> {

    private Location location;

    /* TODO: Write the Enum Flags */
    public PacketOutPlayerPosLook(Location location) {
        this.location = location;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeDouble(this.location.getX());
        buffer.writeDouble(this.location.getY());
        buffer.writeDouble(this.location.getZ());
        buffer.writeFloat(this.location.getYaw());
        buffer.writeFloat(this.location.getPitch());
        buffer.writeByte((byte) 0);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {}

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
