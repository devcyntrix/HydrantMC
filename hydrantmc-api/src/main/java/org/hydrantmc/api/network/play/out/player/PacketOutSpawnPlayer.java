package org.hydrantmc.api.network.play.out.player;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.MathUtils;

import java.util.UUID;

public class PacketOutSpawnPlayer implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int entityId;
    private UUID uuid;
    private Location location;
    private short currentItem;

    public PacketOutSpawnPlayer(int entityId, UUID uuid, Location location, short currentItem) {
        this.entityId = entityId;
        this.uuid = uuid;
        this.location = location;
        this.currentItem = currentItem;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeVarInteger(this.entityId);
        buffer.writeUniqueId(this.uuid);
        buffer.writeInt(MathUtils.floor_double(this.location.getX() * 32));
        buffer.writeInt(MathUtils.floor_double(this.location.getY() * 32));
        buffer.writeInt(MathUtils.floor_double(this.location.getZ() * 32));
        buffer.writeByte((int) (this.location.getYaw() * 256 / 360));
        buffer.writeByte((int) (this.location.getPitch() * 256 / 360));
        buffer.writeShort(this.currentItem);
        //TODO: WRITE METADATA

        buffer.writeByte(127);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingPlayerSpawn(this);
    }

    public int getEntityId() {
        return entityId;
    }

    public void setEntityId(int entityId) {
        this.entityId = entityId;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public short getCurrentItem() {
        return currentItem;
    }

    public void setCurrentItem(short currentItem) {
        this.currentItem = currentItem;
    }

}
