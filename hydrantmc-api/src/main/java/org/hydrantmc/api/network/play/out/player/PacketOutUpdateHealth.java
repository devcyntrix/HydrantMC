package org.hydrantmc.api.network.play.out.player;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutUpdateHealth implements OutgoingPacket<AbstractPlayPacketHandler> {

    private float health;
    private int food;
    private float saturation;

    public PacketOutUpdateHealth(float health, int food, float saturation) {
        this.health = health;
        this.food = food;
        this.saturation = saturation;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeFloat(health);
        buffer.writeVarInteger(food);
        buffer.writeFloat(saturation);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) { }

}
