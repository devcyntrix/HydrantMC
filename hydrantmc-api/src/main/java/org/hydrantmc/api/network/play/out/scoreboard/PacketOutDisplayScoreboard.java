package org.hydrantmc.api.network.play.out.scoreboard;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.scoreboard.ScoreboardDisplay;

public class PacketOutDisplayScoreboard implements OutgoingPacket<AbstractPlayPacketHandler> {

    private byte position;
    private String scoreboardName;

    public PacketOutDisplayScoreboard(ScoreboardDisplay position, String scoreboardName) {
        this.position = (byte) position.ordinal();
        this.scoreboardName = scoreboardName;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeByte(this.position);
        buffer.writeString(this.scoreboardName);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        // FIXME
    }

    public byte getPosition() {
        return position;
    }

    public void setPosition(byte position) {
        this.position = position;
    }

    public String getScoreboardName() {
        return scoreboardName;
    }

    public void setScoreboardName(String scoreboardName) {
        this.scoreboardName = scoreboardName;
    }
}
