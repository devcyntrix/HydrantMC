package org.hydrantmc.api.network.play.out.scoreboard;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutScoreboardObjective implements OutgoingPacket<AbstractPlayPacketHandler> {

    private String scoreName;
    private byte mode;
    private String objectiveValue;
    private String type;

    public PacketOutScoreboardObjective(String scoreName, byte mode, String objectiveValue, String type) {
        this.scoreName = scoreName;
        this.mode = mode;
        this.objectiveValue = objectiveValue;
        this.type = type;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(this.scoreName);
        buffer.writeByte(this.mode);

        if (mode == 0 || mode == 2) {
            buffer.writeString(this.objectiveValue);
            buffer.writeString(this.type);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        // FIXME
    }

    public String getScoreName() {
        return scoreName;
    }

    public void setScoreName(String scoreName) {
        this.scoreName = scoreName;
    }

    public byte getMode() {
        return mode;
    }

    public void setMode(byte mode) {
        this.mode = mode;
    }

    public String getObjectiveValue() {
        return objectiveValue;
    }

    public void setObjectiveValue(String objectiveValue) {
        this.objectiveValue = objectiveValue;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

}
