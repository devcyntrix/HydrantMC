package org.hydrantmc.api.network.play.out.scoreboard;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutScoreboardTeams implements OutgoingPacket<AbstractPlayPacketHandler> {

    private String teamName;
    private byte mode;
    private String teamDisplayName;
    private String teamPrefix;
    private String teamSuffix;
    private byte friendflyFire;
    private String nameTagVisibility;
    private byte color;
    private String[] players;

    public PacketOutScoreboardTeams(String teamName, byte mode, String teamDisplayName, String teamPrefix, String teamSuffix, byte friendflyFire, String nameTagVisibility, byte color, String... players) {
        this.teamName = teamName;
        this.mode = mode;
        this.teamDisplayName = teamDisplayName;
        this.teamPrefix = teamPrefix;
        this.teamSuffix = teamSuffix;
        this.friendflyFire = friendflyFire;
        this.nameTagVisibility = nameTagVisibility;
        this.color = color;
        this.players = players;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(this.teamName);
        buffer.writeByte(this.mode);

        switch (mode) {
            case 0:
                buffer.writeString(this.teamDisplayName);
                buffer.writeString(this.teamPrefix);
                buffer.writeString(this.teamSuffix);
                buffer.writeByte(this.friendflyFire);
                buffer.writeString(this.nameTagVisibility);
                buffer.writeByte(this.color);
                buffer.writeVarInteger(this.players.length);

                for (String player : this.players) {
                    buffer.writeString(player);
                }
                break;
            case 2:
                buffer.writeString(this.teamDisplayName);
                buffer.writeString(this.teamPrefix);
                buffer.writeString(this.teamSuffix);
                buffer.writeByte(this.friendflyFire);
                buffer.writeString(this.nameTagVisibility);
                buffer.writeByte(this.color);
                break;
            case 3:
            case 4:
                buffer.writeVarInteger(this.players.length);

                for (String player : this.players) {
                    buffer.writeString(player);
                }
                break;
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        // FIXME
    }

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public byte getMode() {
        return mode;
    }

    public void setMode(byte mode) {
        this.mode = mode;
    }

    public String getTeamDisplayName() {
        return teamDisplayName;
    }

    public void setTeamDisplayName(String teamDisplayName) {
        this.teamDisplayName = teamDisplayName;
    }

    public String getTeamPrefix() {
        return teamPrefix;
    }

    public void setTeamPrefix(String teamPrefix) {
        this.teamPrefix = teamPrefix;
    }

    public String getTeamSuffix() {
        return teamSuffix;
    }

    public void setTeamSuffix(String teamSuffix) {
        this.teamSuffix = teamSuffix;
    }

    public byte getFriendflyFire() {
        return friendflyFire;
    }

    public void setFriendflyFire(byte friendflyFire) {
        this.friendflyFire = friendflyFire;
    }

    public String getNameTagVisibility() {
        return nameTagVisibility;
    }

    public void setNameTagVisibility(String nameTagVisibility) {
        this.nameTagVisibility = nameTagVisibility;
    }

    public byte getColor() {
        return color;
    }

    public void setColor(byte color) {
        this.color = color;
    }

    public String[] getPlayers() {
        return players;
    }

    public void setPlayers(String[] players) {
        this.players = players;
    }

}
