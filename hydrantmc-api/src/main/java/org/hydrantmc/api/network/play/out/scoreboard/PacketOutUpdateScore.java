package org.hydrantmc.api.network.play.out.scoreboard;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;

public class PacketOutUpdateScore implements OutgoingPacket<AbstractPlayPacketHandler> {

    private String scoreName;
    private byte action;
    private String objectiveName;
    private int value;

    public PacketOutUpdateScore(String scoreName, byte action, String objectiveName, int value) {
        this.scoreName = scoreName;
        this.action = action;
        this.objectiveName = objectiveName;
        this.value = value;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(this.scoreName);
        buffer.writeByte(this.action);
        buffer.writeString(this.objectiveName);

        if (this.action != 1) {
            buffer.writeVarInteger(this.value);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        // FIXME
    }

    public String getScoreName() {
        return scoreName;
    }

    public void setScoreName(String scoreName) {
        this.scoreName = scoreName;
    }

    public byte getAction() {
        return action;
    }

    public void setAction(byte action) {
        this.action = action;
    }

    public String getObjectiveName() {
        return objectiveName;
    }

    public void setObjectiveName(String objectiveName) {
        this.objectiveName = objectiveName;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

}
