package org.hydrantmc.api.network.play.out.world;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.utils.Location;

public class PacketOutBlockChange implements OutgoingPacket<AbstractPlayPacketHandler> {

    private Location location;
    private short blockId;
    private short blockMeta;

    public PacketOutBlockChange(Location location, short blockId, short blockMeta) {
        this.location = location;
        this.blockId = blockId;
        this.blockMeta = blockMeta;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeLong(location.toLong());
        buffer.writeVarInteger(blockId << 4 | (blockMeta & 15));
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingBlockChange(this);
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public short getBlockId() {
        return blockId;
    }

    public void setBlockId(short blockId) {
        this.blockId = blockId;
    }

    public short getBlockMeta() {
        return blockMeta;
    }

    public void setBlockMeta(short blockMeta) {
        this.blockMeta = blockMeta;
    }
}
