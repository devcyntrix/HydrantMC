package org.hydrantmc.api.network.play.out.world;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.world.chunk.Chunk;
import org.hydrantmc.api.world.chunk.ChunkColumn;
import org.hydrantmc.api.world.chunk.ChunkColumnData;

import java.util.LinkedList;
import java.util.List;

public class PacketOutChunkColumnData implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int chunkX;
    private int chunkZ;

    private boolean overworld;

    private int primaryBitMask;

    private ChunkColumnData data;

    public PacketOutChunkColumnData(ChunkColumn chunkColumn, boolean overworld, boolean load) {
        this.chunkX = chunkColumn.getChunkColumnX();
        this.chunkZ = chunkColumn.getChunkColumnZ();
        this.overworld = overworld;

        Chunk[] chunks = chunkColumn.getChunks();
        ChunkColumnData columnData = new ChunkColumnData(chunkColumn.getChunkColumnX(), chunkColumn.getChunkColumnZ());
        List<Chunk> notEmptyChunks = new LinkedList<>();

        if (load) {
            for (int chunkY = 0; chunkY < chunks.length; chunkY++) {
                Chunk chunk = chunks[chunkY];
                if (chunk != null && !chunk.isEmpty()) {
                    columnData.dataSize |= 1 << chunkY;
                    notEmptyChunks.add(chunk);
                }
            }
        }
        columnData.data = new byte[calculateChunkSize(Integer.bitCount(columnData.dataSize), /* hasSky */ true, true)];

        int count = 0;

        for (Chunk chunk : notEmptyChunks) {
            char[] chars = chunk.getData();

            for (char c : chars) {
                columnData.data[count++] = (byte) (c & 255);
                columnData.data[count++] = (byte) (c >> 8 & 255);
            }
        }

        for (Chunk chunk : notEmptyChunks) {
            count = mergeArray(chunk.getBlockLightArray().getData(), columnData.data, count);
        }

        if(true) {
            for (Chunk chunk : notEmptyChunks) {
                count = mergeArray(chunk.getSkylightArray().getData(), columnData.data, count);
            }
        }

        mergeArray(chunkColumn.getBiomeArray(), columnData.data, count);
        this.data = columnData;
    }

    private int mergeArray(byte[] srcArray, byte[] destArray, int putAfterPos) {
        System.arraycopy(srcArray, 0, destArray, putAfterPos, srcArray.length);
        return putAfterPos + srcArray.length;
    }

    private int calculateChunkSize(int bits, boolean sky, boolean biome) {
        int size = bits * 2 * (16 * 16 * 16) + bits * (16 * 16 * 16) / 2;
        if (sky) size += bits * (16 * 16 * 16) / 2;
        if (biome) size += 256;
        return size;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeInt(this.chunkX);
        buffer.writeInt(this.chunkZ);
        buffer.writeBoolean(this.overworld);
        buffer.writeShort(this.data.dataSize & 65535);
        buffer.writeVarInteger(this.data.data.length);
        buffer.writeBytes(this.data.data);
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingChunkColumnData(this);
    }

    public int getChunkX() {
        return chunkX;
    }

    public void setChunkX(int chunkX) {
        this.chunkX = chunkX;
    }

    public int getChunkZ() {
        return chunkZ;
    }

    public void setChunkZ(int chunkZ) {
        this.chunkZ = chunkZ;
    }

    public boolean isOverworld() {
        return overworld;
    }

    public void setOverworld(boolean overworld) {
        this.overworld = overworld;
    }

    public int getPrimaryBitMask() {
        return primaryBitMask;
    }

    public void setPrimaryBitMask(int primaryBitMask) {
        this.primaryBitMask = primaryBitMask;
    }

    public ChunkColumnData getData() {
        return data;
    }

    public void setData(ChunkColumnData data) {
        this.data = data;
    }

}
