package org.hydrantmc.api.network.play.out.world;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.world.chunk.Chunk;
import org.hydrantmc.api.world.chunk.ChunkColumn;
import org.hydrantmc.api.world.chunk.ChunkColumnData;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Cyntrix on 12/19/2017.
 */
public class PacketOutMapChunkBulk implements OutgoingPacket<AbstractPlayPacketHandler> {

    private int[] xPositions;
    private int[] zPositions;
    private ChunkColumnData[] chunkColumnData;

    private boolean overworld;

    public PacketOutMapChunkBulk(List<ChunkColumn> chunkColumns, boolean overworld) {
        int size = chunkColumns.size();
        this.xPositions = new int[size];
        this.zPositions = new int[size];
        this.chunkColumnData = new ChunkColumnData[size];
        this.overworld = overworld;

        for (int i = 0; i < size; i++) {
            ChunkColumn column = chunkColumns.get(i);

            this.xPositions[i] = column.getChunkColumnX();
            this.zPositions[i] = column.getChunkColumnZ();

            Chunk[] chunks = column.getChunks();
            ChunkColumnData columnData = new ChunkColumnData(column.getChunkColumnX(), column.getChunkColumnZ());
            List<Chunk> notEmptyChunks = new LinkedList<>();

            for (int chunkY = 0; chunkY < chunks.length; chunkY++) {
                Chunk chunk = chunks[chunkY];
                if (chunk != null && !chunk.isEmpty()) {
                    columnData.dataSize |= 1 << chunkY;
                    notEmptyChunks.add(chunk);
                }
            }

            columnData.data = new byte[calculateChunkSize(Integer.bitCount(columnData.dataSize), overworld, true)];

            int count = 0;

            for (Chunk chunk : notEmptyChunks) {
                char[] chars = chunk.getData();

                for (char c : chars) {
                    columnData.data[count++] = (byte) (c & 255);
                    columnData.data[count++] = (byte) (c >> 8 & 255);
                }
            }

            for (Chunk chunk : notEmptyChunks) {
                count = mergeArray(chunk.getBlockLightArray().getData(), columnData.data, count);
            }

            if (overworld) {
                for (Chunk chunk : notEmptyChunks) {
                    count = mergeArray(chunk.getSkylightArray().getData(), columnData.data, count);
                }
            }

            mergeArray(column.getBiomeArray(), columnData.data, count);
            this.chunkColumnData[i] = columnData;
        }

    }

    private int mergeArray(byte[] srcArray, byte[] destArray, int putAfterPos) {
        System.arraycopy(srcArray, 0, destArray, putAfterPos, srcArray.length);
        return putAfterPos + srcArray.length;
    }

    private int calculateChunkSize(int bits, boolean sky, boolean biome) {
        int size = bits * 2 * (16 * 16 * 16) + bits * (16 * 16 * 16) / 2;
        if (sky) size += bits * (16 * 16 * 16) / 2;
        if (biome) size += 256;
        return size;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeBoolean(this.overworld);
        buffer.writeVarInteger(this.chunkColumnData.length);

        for (int i = 0; i < this.xPositions.length; i++) {
            buffer.writeInt(this.xPositions[i]);
            buffer.writeInt(this.zPositions[i]);
            buffer.writeShort((short) (this.chunkColumnData[i].dataSize & 65535));
        }

        for (int i = 0; i < this.xPositions.length; i++) {
            buffer.writeBytes(this.chunkColumnData[i].data);
        }
    }

    @Override
    public void handlePacket(AbstractPlayPacketHandler handler) {
        handler.handleOutgoingMapChunkBulk(this);
    }

    public int[] getxPositions() {
        return xPositions;
    }

    public void setxPositions(int[] xPositions) {
        this.xPositions = xPositions;
    }

    public int[] getzPositions() {
        return zPositions;
    }

    public void setzPositions(int[] zPositions) {
        this.zPositions = zPositions;
    }

    public ChunkColumnData[] getChunkColumnData() {
        return chunkColumnData;
    }

    public void setChunkColumnData(ChunkColumnData[] chunkColumnData) {
        this.chunkColumnData = chunkColumnData;
    }

    public boolean isOverworld() {
        return overworld;
    }

    public void setOverworld(boolean overworld) {
        this.overworld = overworld;
    }

}
