package org.hydrantmc.api.network.status;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.status.in.PacketInPing;
import org.hydrantmc.api.network.status.in.PacketInStatusRequest;

public abstract class AbstractStatusPacketHandler extends AbstractPacketHandler {

    public AbstractStatusPacketHandler(Server server) {
        super(server);
    }

    public abstract void handleIncomingStatusRequest(PacketInStatusRequest request);

    public abstract void handleIncomingPing(PacketInPing ping);
}
