package org.hydrantmc.api.network.status.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.status.AbstractStatusPacketHandler;

public class PacketInPing implements IncomingPacket<AbstractStatusPacketHandler> {

    private long time;

    @Override
    public void readPacket(PacketBuffer buffer) {
        this.time = buffer.toByteBuf().readLong();
    }

    @Override
    public void handlePacket(AbstractStatusPacketHandler handler) {
        handler.handleIncomingPing(this);
    }

    public long getTime() {
        return time;
    }
}
