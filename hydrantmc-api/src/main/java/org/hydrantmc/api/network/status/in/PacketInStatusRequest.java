package org.hydrantmc.api.network.status.in;

import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.status.AbstractStatusPacketHandler;

public class PacketInStatusRequest implements IncomingPacket<AbstractStatusPacketHandler> {

    @Override
    public void readPacket(PacketBuffer buffer) {
    }

    @Override
    public void handlePacket(AbstractStatusPacketHandler handler) {
        handler.handleIncomingStatusRequest(this);
    }
}
