package org.hydrantmc.api.network.status.out;

import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.status.AbstractStatusPacketHandler;

public class PacketOutPong implements OutgoingPacket<AbstractStatusPacketHandler> {

    private final long time;

    public PacketOutPong(long time) {
        this.time = time;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeLong(this.time);
    }

    @Override
    public void handlePacket(AbstractStatusPacketHandler handler) { }
}
