package org.hydrantmc.api.network.status.out;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.status.AbstractStatusPacketHandler;
import org.hydrantmc.api.utils.serializer.ServerStatusPlayersSerializer;
import org.hydrantmc.api.utils.serializer.ServerStatusSerializer;
import org.hydrantmc.api.utils.serializer.ServerStatusVersionSerializer;
import org.hydrantmc.api.utils.status.ServerStatus;
import org.hydrantmc.api.utils.status.ServerStatusPlayers;
import org.hydrantmc.api.utils.status.ServerStatusVersion;

public class PacketOutStatusResponse implements OutgoingPacket<AbstractStatusPacketHandler> {

    private static final Gson gson = new GsonBuilder().registerTypeAdapter(ServerStatus.class, new ServerStatusSerializer())
            .registerTypeAdapter(ServerStatusVersion.class, new ServerStatusVersionSerializer())
            .registerTypeAdapter(ServerStatusPlayers.class, new ServerStatusPlayersSerializer())
            .create();

    private final ServerStatus serverStatus;

    public PacketOutStatusResponse(ServerStatus serverStatus) {
        this.serverStatus = serverStatus;
    }

    @Override
    public void writePacket(PacketBuffer buffer) {
        buffer.writeString(gson.toJson(serverStatus));
    }

    @Override
    public void handlePacket(AbstractStatusPacketHandler handler) { }
}
