package org.hydrantmc.api.permission;

public interface Permissible {

    /**
     * Checks if the command sender has the specified permission
     *
     * @param permission the permission you want to check
     * @return true if the command sender has the permission
     */
    boolean hasPermission(String permission);

    /**
     *
     * @return true if the command sender has op
     */
    boolean hasOp();

    /**
     * Sets the command sender's op status to the specified value
     *
     * @param op the state you want to set
     */
    void setOp(boolean op);

}
