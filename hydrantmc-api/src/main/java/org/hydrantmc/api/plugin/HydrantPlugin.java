package org.hydrantmc.api.plugin;

import org.hydrantmc.api.Server;

import java.io.File;
import java.io.InputStream;

/**
 * Represents any Plugin that may be loaded at runtime to enhance existing
 * functionality.
 */
public class HydrantPlugin {

    private PluginDescription description;
    private Server server;
    private File file;

    /**
     * Called by the loader to initialize the fields in this plugin.
     *
     * @param server current server instance
     * @param description the description that describes this plugin
     */
    final void init(Server server, PluginDescription description) {
        this.server = server;
        this.description = description;
        this.file = description.getFile();
    }

    /**
     * Called when the plugin has just been loaded. Most of the proxy will not
     * be initialized, so only use it for registering predefined behavior.
     */
    public void onLoad() { }

    /**
     * Called when this plugin is enabled.
     */
    public void onEnable() { }

    /**
     * Called when this plugin is disabled.
     */
    public void onDisable() { }

    /**
     * Gets the data folder where this plugin may store arbitrary data. It will
     * be a child of {@link Server#getPluginsFolder()}.
     *
     * @return the data folder of this plugin
     */
    public final File getDataFolder() {
        return new File(getServer().getPluginsFolder(), getDescription().getName());
    }

    /**
     * Get a resource from within this plugins jar or container. Care must be
     * taken to close the returned stream.
     *
     * @param name the full path name of this resource
     * @return the stream for getting this resource, or null if it does not
     * exist
     */
    public final InputStream getResourceAsStream(String name) {
        return getClass().getClassLoader().getResourceAsStream(name);
    }

    public PluginDescription getDescription() {
        return description;
    }

    public Server getServer() {
        return server;
    }

    public File getFile() {
        return file;
    }

}