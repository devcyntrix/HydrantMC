package org.hydrantmc.api.plugin;

import java.io.File;
import java.util.HashSet;
import java.util.Set;

/**
 * POJO representing the plugin.yml file.
 */
public class PluginDescription {

    public PluginDescription() { }

    public PluginDescription(String name, String main) {
        this.name = name;
        this.main = main;
    }

    /**
     * Friendly name of the plugin.
     */
    private String name;
    /**
     * Plugin main class. Needs to extend {@link HydrantPlugin}.
     */
    private String main;
    /**
     * Plugin version.
     */
    private String version;
    /**
     * Plugin author.
     */
    private String[] author;
    /**
     * Plugin hard dependencies.
     */
    private Set<String> depends = new HashSet<>();
    /**
     * Plugin soft dependencies.
     */
    private Set<String> softDepends = new HashSet<>();
    /**
     * File we were loaded from.
     */
    private File file = null;
    /**
     * Optional description.
     */
    private String description = null;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMain() {
        return main;
    }

    public void setMain(String main) {
        this.main = main;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String[] getAuthor() {
        return author;
    }

    public void setAuthor(String[] author) {
        this.author = author;
    }

    public Set<String> getDepends() {
        return depends;
    }

    public void setDepends(Set<String> depends) {
        this.depends = depends;
    }

    public Set<String> getSoftDepends() {
        return softDepends;
    }

    public void setSoftDepends(Set<String> softDepends) {
        this.softDepends = softDepends;
    }

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}