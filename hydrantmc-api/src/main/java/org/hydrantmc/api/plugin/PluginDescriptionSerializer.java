package org.hydrantmc.api.plugin;

import com.google.common.collect.Iterables;
import com.google.gson.*;

import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.HashSet;

/**
 * Created by DevCyntrix on Feb, 2018
 */
public class PluginDescriptionSerializer implements JsonSerializer<PluginDescription>, JsonDeserializer<PluginDescription> {

    @Override
    public PluginDescription deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!(element instanceof JsonObject)) return null;

        JsonObject object = element.getAsJsonObject();
        String name = object.get("name").getAsString();
        String main = object.get("main").getAsString();

        PluginDescription description = new PluginDescription(name, main);

        if(object.has("version"))
            description.setVersion(object.get("version").getAsString());

        boolean author = object.has("author");
        boolean authors = object.has("authors");

        if(author ^ authors) {
            JsonArray jsonArray = author ? object.getAsJsonArray("author") : object.getAsJsonArray("authors");
            description.setAuthor(
                    Iterables.toArray(Iterables.transform(jsonArray, JsonElement::getAsString), String.class)
            );
        }

        if(object.has("depends")) {
            JsonArray jsonArray = object.getAsJsonArray("depends");
            description.setDepends(new HashSet<>(Arrays.asList(Iterables.toArray(Iterables.transform(jsonArray, JsonElement::getAsString), String.class))));
        }

        if(object.has("softdepends")) {
            JsonArray jsonArray = object.getAsJsonArray("softdepends");
            description.setSoftDepends(new HashSet<>(Arrays.asList(Iterables.toArray(Iterables.transform(jsonArray, JsonElement::getAsString), String.class))));
        }

        if(object.has("description")) {
            description.setDescription(object.get("description").getAsString());
        }

        return description;
    }

    @Override
    public JsonElement serialize(PluginDescription description, Type type, JsonSerializationContext context) {
        return null;
    }
}
