package org.hydrantmc.api.plugin;

import com.google.common.base.Preconditions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.CommandRegistry;
import org.hydrantmc.api.listener.ListenerRegistry;

import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.logging.Level;

/**
 * Class to manage bridging between plugin duties and implementation duties, for
 * example event handling and plugin management.
 */
public class PluginManager {

    private final Gson gson;

    private final Server server;
    private final Map<String, HydrantPlugin> plugins = new LinkedHashMap<>();
    private final CommandRegistry commandRegistry;
    private final ListenerRegistry listenerRegistry;
    private Map<String, PluginDescription> toLoad = new HashMap<>();

    public PluginManager(Server server) {
        this.gson = new GsonBuilder().registerTypeHierarchyAdapter(PluginDescription.class, new PluginDescriptionSerializer()).create();

        this.server = server;
        this.commandRegistry = new CommandRegistry();
        this.listenerRegistry = new ListenerRegistry();
    }

    /**
     * Returns the {@link HydrantPlugin} objects corresponding to all loaded plugins.
     *
     * @return the set of loaded plugins
     */
    public Collection<HydrantPlugin> getPlugins()
    {
        return plugins.values();
    }

    /**
     * Returns a loaded plugin identified by the specified name.
     *
     * @param name of the plugin to retrieve
     * @return the retrieved plugin or null if not loaded
     */
    public HydrantPlugin getPlugin(String name)
    {
        return plugins.get(name);
    }

    /**
     * Checks if the given plugin is enabled or not
     * <p>
     * Please note that the name of the plugin is case-sensitive.
     *
     * @param name Name of the plugin to check
     * @return true if the plugin is enabled, otherwise false
     */
    public boolean isPluginEnabled(String name) {
        return plugins.containsKey(name);
    }

    /**
     * Checks if the given plugin is enabled or not
     *
     * @param plugin Plugin to check
     * @return true if the plugin is enabled, otherwise false
     */
    public boolean isPluginEnabled(HydrantPlugin plugin) {
        return isPluginEnabled(plugin.getDescription().getName());
    }

    public void loadPlugins() {
        Map<PluginDescription, Boolean> pluginStatuses = new HashMap<>();

        for(Map.Entry<String, PluginDescription> entry : toLoad.entrySet()) {
            PluginDescription plugin = entry.getValue();
            if(!enablePlugin(pluginStatuses, new Stack<>(), plugin)) {
                server.getLogger().log(Level.WARNING, "Failed to enable {0}", entry.getKey());
            }
        }

        toLoad.clear();
        toLoad = null;
    }

    public void enablePlugins() {
        for(HydrantPlugin plugin : plugins.values()) {
            try {
                plugin.onEnable();
                server.getLogger().log(Level.INFO, "Enabled plugin {0} version {1} by {2}", new String[]{ plugin.getDescription().getName(), plugin.getDescription().getVersion(), String.join(", ", plugin.getDescription().getAuthor()) });
            } catch(Throwable t) {
                server.getLogger().log(Level.WARNING, "Exception encountered when loading plugin: " + plugin.getDescription().getName(), t);
            }
        }
    }

    private boolean enablePlugin(Map<PluginDescription, Boolean> pluginStatuses, Stack<PluginDescription> dependStack, PluginDescription plugin) {
        if(pluginStatuses.containsKey(plugin)) return pluginStatuses.get(plugin);

        // combine all dependencies for 'for loop'
        Set<String> dependencies = new HashSet<>();
        dependencies.addAll(plugin.getDepends());
        dependencies.addAll(plugin.getSoftDepends());

        // success status
        boolean status = true;

        // try to load dependencies first
        for(String dependName : dependencies) {
            PluginDescription depend = toLoad.get(dependName);
            Boolean dependStatus = (depend != null) ? pluginStatuses.get(depend) : Boolean.FALSE;

            if(dependStatus == null) {
                if(dependStack.contains(depend)) {
                    StringBuilder dependencyGraph = new StringBuilder();
                    for(PluginDescription element : dependStack) {
                        dependencyGraph.append(element.getName()).append(" -> ");
                    }
                    dependencyGraph.append(plugin.getName()).append(" -> ").append(dependName);
                    server.getLogger().log(Level.WARNING, "Circular dependency detected: {0}", dependencyGraph);
                    status = false;
                } else {
                    dependStack.push(plugin);
                    dependStatus = this.enablePlugin(pluginStatuses, dependStack, depend);
                    dependStack.pop();
                }
            }

            if(dependStatus == Boolean.FALSE && plugin.getDepends().contains(dependName)) {
                server.getLogger().log(Level.WARNING, "{0} (required by {1}) is unavailable", new Object[] { String.valueOf( dependName ), plugin.getName() });
                status = false;
            }

            if(!status) {
                break;
            }
        }

        // do actual loading
        if(status) {
            try {
                URLClassLoader loader = new PluginClassLoader( new URL[] { plugin.getFile().toURI().toURL() });
                Class<?> main = loader.loadClass(plugin.getMain());
                HydrantPlugin clazz = (HydrantPlugin) main.getDeclaredConstructor().newInstance();

                clazz.init(server, plugin);
                plugins.put(plugin.getName(), clazz);
                clazz.onLoad();
                server.getLogger().log(Level.INFO, "Loaded plugin {0} version {1} by {2}", new Object[] { plugin.getName(), plugin.getVersion(), String.join(", ", plugin.getAuthor()) });
            } catch(Throwable t) {
                server.getLogger().log(Level.WARNING, "Error enabling plugin " + plugin.getName(), t);
            }
        }

        pluginStatuses.put( plugin, status );
        return status;
    }

    /**
     * Disables all the loaded plugins
     */
    public void disablePlugins() {
        for(HydrantPlugin plugin : plugins.values()) {
            try {
                plugin.onDisable();
                server.getLogger().log(Level.INFO, "Disabled plugin {0} version {1} by {2}", new Object[] { plugin.getDescription().getName(), plugin.getDescription().getVersion(), String.join(", ", plugin.getDescription().getAuthor()) });
            } catch(Throwable t) {
                server.getLogger().log(Level.WARNING, "Exception encountered when disabling plugin: " + plugin.getDescription().getName(), t);
            }
        }
    }

    /**
     * Disables and removes all plugins
     */
    public void clearPlugins() {
        disablePlugins();
        plugins.clear();
    }

    /**
     * Load all plugins from the specified folder.
     *
     * @param folder the folder to search for plugins in
     */
    public void detectPlugins(File folder) {
        Preconditions.checkNotNull( folder, "folder" );
        Preconditions.checkArgument( folder.isDirectory(), "Must load from a directory" );

        for(File file : Objects.requireNonNull(folder.listFiles((dir, name) -> name.endsWith(".jar")))) {
            try(JarFile jar = new JarFile(file)) {
                JarEntry entry = jar.getJarEntry( "hydrant.json" );
                Preconditions.checkNotNull( entry, "Plugin must have a hydrant.json" );

                try(InputStream in = jar.getInputStream(entry)) {
                    PluginDescription desc = gson.fromJson(new InputStreamReader(in), PluginDescription.class);
                    Preconditions.checkNotNull(desc.getName(), "Plugin from %s has no name", file);
                    Preconditions.checkNotNull(desc.getMain(), "Plugin from %s has no main", file);

                    desc.setFile(file);
                    toLoad.put(desc.getName(), desc);
                }
            } catch(Exception ex) {
                server.getLogger().log(Level.WARNING, "Could not load plugin from file " + file, ex);
            }
        }
    }

    public CommandRegistry getCommandRegistry() {
        return commandRegistry;
    }

    public ListenerRegistry getListenerRegistry() {
        return listenerRegistry;
    }

}