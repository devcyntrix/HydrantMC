package org.hydrantmc.api.scoreboard;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.utils.Plane;

import java.util.Set;

public interface Scoreboard {

    /**
     * Registers an Objective on this Scoreboard
     *
     * @param name Name of the Objective
     * @return The registered Objective
     * @throws NullPointerException if name is null
     * @throws IllegalArgumentException if an objective by that name already exists
     */
    ScoreboardObjective registerNewObjective(String name) throws NullPointerException, IllegalArgumentException;

    /**
     * Gets an Objective on this Scoreboard by name
     *
     * @param name Name of the Objective
     * @return the Objective or null if it does not exist
     * @throws NullPointerException if name is null
     */
    ScoreboardObjective getObjective(String name) throws NullPointerException;

    /**
     * Gets the Objective currently displayed in a DisplaySlot on this Scoreboard
     *
     * @param display The DisplaySlot
     * @return the Objective currently displayed or null if nothing is displayed in that DisplaySlot
     * @throws NullPointerException if display is null
     */
    ScoreboardObjective getObjective(ScoreboardDisplay display) throws NullPointerException;

    /**
     * Gets all Objectives on this Scoreboard
     *
     * @return An set of all Objectives on this Scoreboard
     */
    Set<ScoreboardObjective> getObjectives();

    /**
     * Gets a Team by name on this Scoreboard
     *
     * @param name Team name
     * @return the matching Team or null if no matches
     * @throws NullPointerException if name is null
     */
    ScoreboardTeam getTeam(String name) throws NullPointerException;

    /**
     * Gets all teams on this Scoreboard
     *
     * @return an set of Teams
     */
    Set<ScoreboardTeam> getTeams();

    /**
     * Registers a Team on this Scoreboard
     *
     * @param name Team name
     * @return registered Team
     * @throws NullPointerException if name is null
     * @throws IllegalArgumentException if team by that name already exists
     */
    ScoreboardTeam registerNewTeam(String name) throws NullPointerException, IllegalArgumentException;

    /**
     * Clears any objective in the specified slot.
     *
     * @param display the slot to remove objectives
     * @throws NullPointerException if display is null
     */
    void clearSlot(ScoreboardDisplay display) throws NullPointerException;

    /**
     * Send scoreboard to player
     *
     * @param player Player
     */
    void sendToPlayer(Player player);

}
