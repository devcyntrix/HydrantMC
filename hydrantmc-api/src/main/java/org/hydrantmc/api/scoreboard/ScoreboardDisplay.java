package org.hydrantmc.api.scoreboard;

public enum ScoreboardDisplay {

    PLAYER_LIST, SIDEBAR, BELOW_NAME

}
