package org.hydrantmc.api.scoreboard;

import org.hydrantmc.api.entity.Player;

public interface ScoreboardObjective {

    /**
     * Gets the name of this Objective
     *
     * @return this objective's name
     * @throws IllegalStateException if this objective has been unregistered
     */
    String getName() throws IllegalStateException;

    /**
     * Gets the display name of this Objective
     *
     * @return this objective's display name
     * @throws IllegalStateException if this objective has been unregistered
     */
    String getDisplayName() throws IllegalStateException;

    /**
     * Sets the name displayed to players for this objective.
     *
     * @param name Display name to set
     * @throws IllegalStateException if this objective has been unregistered
     * @throws NullPointerException if displayName is null
     * @throws IllegalArgumentException if displayName is longer than 32
     *     characters.
     */
    void setDisplayName(String name) throws IllegalStateException, NullPointerException, IllegalArgumentException;

    /**
     * Gets the display slot this objective is displayed at.
     *
     * @return the display slot for this objective, or null if not displayed
     * @throws IllegalStateException if this objective has been unregistered
     */
    ScoreboardDisplay getDisplay() throws IllegalStateException;

    /**
     * Sets this objective to display on the specified slot for the
     * scoreboard, removing it from any other display slot.
     *
     * @param display display slot to change, or null to not display
     * @throws IllegalStateException if this objective has been unregistered
     */
    void setDisplay(ScoreboardDisplay display) throws IllegalStateException;

    /**
     * Gets the scoreboard to which this objective is attached.
     *
     * @return Owning scoreboard, or null if it has been {@link #unregister() unregistered}
     */
    Scoreboard getScoreboard();

    /**
     * Unregisters this objective from the {@link Scoreboard scoreboard.}
     *
     * @throws IllegalStateException if this objective has been unregistered
     */
    void unregister() throws IllegalStateException;

    /**
     * Gets an entry's Score for an Objective on this Scoreboard.
     *
     * @param entry Entry for the Score
     * @return Score tracking the Objective and entry specified
     * @throws NullPointerException if entry is null
     * @throws IllegalStateException if this objective has been unregistered
     */
    ScoreboardScore getScore(String entry) throws NullPointerException, IllegalStateException;

    /**
     * Send objective to player
     *
     * @param player Player
     */
    void sendToPlayer(Player player);

}
