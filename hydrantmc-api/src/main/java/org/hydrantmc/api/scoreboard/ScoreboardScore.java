package org.hydrantmc.api.scoreboard;

import org.hydrantmc.api.entity.Player;

public interface ScoreboardScore {

    /**
     * Gets the entry being tracked by this Score
     *
     * @return this Score's tracked entry
     */
    String getEntry();

    /**
     * Gets the Objective being tracked by this Score
     *
     * @return this Score's tracked objective
     */
    ScoreboardObjective getObjective();

    /**
     * Gets the current score
     *
     * @return the current score
     * @throws IllegalStateException if the associated objective has been unregistered
     */
    int getScore() throws IllegalStateException;

    /**
     * Sets the current score.
     *
     * @param score New score
     * @throws IllegalStateException if the associated objective has been unregistered
     */
    void setScore(int score) throws IllegalStateException;

    /**
     * Shows if this score has been set at any point in time.
     *
     * @return if this score has been set before
     * @throws IllegalStateException if the associated objective has been unregistered
     */
    boolean isScoreSet() throws IllegalStateException;

    /**
     * Gets the scoreboard for the associated objective.
     *
     * @return the owning objective's scoreboard, or null if it has been {@link ScoreboardObjective#unregister() unregistered}
     */
    Scoreboard getScoreboard();

    /**
     * Send score to player
     *
     * @param player Player
     */
    void sendToPlayer(Player player);

}
