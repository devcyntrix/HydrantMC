package org.hydrantmc.api.scoreboard;

import java.util.Set;

public interface ScoreboardTeam {

    /**
     * Gets the name of this Team
     *
     * @return Objective name
     * @throws IllegalStateException if this team has been unregistered
     */
    String getName() throws IllegalStateException;

    /**
     * Gets the name displayed to entries for this team
     *
     * @return Team display name
     * @throws IllegalStateException if this team has been unregistered
     */
    String getDisplayName() throws IllegalStateException;

    /**
     * Sets the name displayed to entries for this team
     *
     * @param displayName New display name
     * @throws IllegalArgumentException if displayName is longer than 32 characters.
     * @throws IllegalStateException if this team has been unregistered
     */
    void setDisplayName(String displayName) throws IllegalStateException, IllegalArgumentException;

    /**
     * Gets the prefix prepended to the display of entries on this team.
     *
     * @return Team prefix
     * @throws IllegalStateException if this team has been unregistered
     */
    String getPrefix() throws IllegalStateException;

    /**
     * Sets the prefix prepended to the display of entries on this team.
     *
     * @param prefix New prefix
     * @throws NullPointerException if prefix is null
     * @throws IllegalArgumentException if prefix is longer than 16 characters
     * @throws IllegalStateException if this team has been unregistered
     */
    void setPrefix(String prefix) throws NullPointerException, IllegalStateException, IllegalArgumentException;

    /**
     * Gets the suffix appended to the display of entries on this team.
     *
     * @return the team's current suffix
     * @throws IllegalStateException if this team has been unregistered
     */
    String getSuffix() throws IllegalStateException;

    /**
     * Sets the suffix appended to the display of entries on this team.
     *
     * @param suffix the new suffix for this team.
     * @throws NullPointerException if suffix is null
     * @throws IllegalArgumentException if suffix is longer than 16 characters
     * @throws IllegalStateException if this team has been unregistered
     */
    void setSuffix(String suffix) throws NullPointerException, IllegalStateException, IllegalArgumentException;

    /**
     * Gets the Set of entries on the team
     *
     * @return entries on the team
     * @throws IllegalStateException if this team has been unregistered
     */
    Set<String> getEntries() throws IllegalStateException;

    /**
     * Gets the size of the team
     *
     * @return number of entries on the team
     * @throws IllegalStateException if this team has been unregistered
     */
    int getSize() throws IllegalStateException;

    /**
     * Gets the Scoreboard to which this team is attached
     *
     * @return Owning scoreboard, or null if this team has been {@link #unregister() unregistered}
     */
    Scoreboard getScoreboard();

    /**
     * This puts the specified entry onto this team for the scoreboard.
     * <p>
     * This will remove the entry from any other team on the scoreboard.
     *
     * @param entry the entry to add
     * @throws NullPointerException if entry is null
     * @throws IllegalStateException if this team has been unregistered
     */
    void addEntry(String entry) throws NullPointerException, IllegalStateException;

    /**
     * Removes the entry from this team.
     *
     * @param entry the entry to remove
     * @throws NullPointerException if entry is null
     * @throws IllegalStateException if this team has been unregistered
     * @return if the entry was a part of this team
     */
    boolean removeEntry(String entry) throws NullPointerException, IllegalStateException;

    /**
     * Unregisters this team from the Scoreboard
     *
     * @throws IllegalStateException if this team has been unregistered
     */
    void unregister() throws IllegalStateException;

    /**
     * Checks to see if the specified entry is a member of this team.
     *
     * @param entry the entry to search for
     * @return true if the entry is a member of this team
     * @throws NullPointerException if entry is null
     * @throws IllegalStateException if this team has been unregistered
     */
    boolean hasEntry(String entry) throws NullPointerException, IllegalStateException;

}
