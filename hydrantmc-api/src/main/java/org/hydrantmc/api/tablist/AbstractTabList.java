package org.hydrantmc.api.tablist;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.entity.Player;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public abstract class AbstractTabList {

    private final List<TabListItem> items;

    public AbstractTabList() {
        this.items = new LinkedList<>();
    }

    public abstract void onPreInsertItems(TabListItem... items);

    public abstract void onPlayerJoin(Player player);

    public abstract void onPostInsertItems(TabListItem... items);

    public abstract void onPreRemoveItems(TabListItem... items);

    public abstract void onPostRemoveItems(TabListItem... items);

    public void insertItems(TabListItem... items) {
        Preconditions.checkNotNull(items, "items cannot be null.");
        Preconditions.checkArgument(items.length > 0, "items cannot be empty.");
        onPreInsertItems(items);
        this.items.addAll(Arrays.asList(items));
        onPostInsertItems(items);
    }

    public void insertPlayer(Player player) {
        Preconditions.checkNotNull(player, "player cannot be null.");
        insertItems(player.getTabListItem());
    }

    public List<TabListItem> getItems() {
        return this.items;
    }

    public void removeItems(TabListItem... items) {
        Preconditions.checkNotNull(items, "item cannot be null.");
        onPreRemoveItems(items);
        this.items.removeAll(Arrays.asList(items));
        onPostRemoveItems(items);
    }

    public void removePlayer(Player player) {
        Preconditions.checkNotNull(player, "player cannot be null.");
        removeItems(player.getTabListItem());
    }
}
