package org.hydrantmc.api.tablist;

import com.google.common.base.Objects;
import org.hydrantmc.api.chat.TextComponent;
import org.hydrantmc.api.utils.player.GameProfile;
import org.hydrantmc.api.utils.player.GameMode;

public class TabListItem {

    private short ping;
    private GameMode gameMode;
    private GameProfile gameProfile;
    private TextComponent displayName;

    public TabListItem(short ping, GameMode gameMode, GameProfile gameProfile, TextComponent displayName) {
        this.ping = ping;
        this.gameMode = gameMode;
        this.gameProfile = gameProfile;
        this.displayName = displayName;
    }

    public short getPing() {
        return ping;
    }

    public void setPing(short ping) {
        this.ping = ping;
    }

    public GameMode getGameMode() {
        return gameMode;
    }

    public void setGameMode(GameMode gameMode) {
        this.gameMode = gameMode;
    }

    public GameProfile getGameProfile() {
        return gameProfile;
    }

    public void setGameProfile(GameProfile gameProfile) {
        this.gameProfile = gameProfile;
    }

    public TextComponent getDisplayName() {
        return displayName;
    }

    public void setDisplayName(TextComponent displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TabListItem item = (TabListItem) o;
        return Objects.equal(getGameProfile().getUniqueId(), item.getGameProfile().getUniqueId());
    }
}
