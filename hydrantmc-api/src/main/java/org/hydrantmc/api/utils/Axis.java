package org.hydrantmc.api.utils;

public enum Axis {

    X(Plane.HORIZONTAL),
    Y(Plane.VERTICAL),
    Z(Plane.HORIZONTAL);

    private final Plane plane;

    Axis(Plane plane) {
            this.plane = plane;
        }

}