package org.hydrantmc.api.utils;

public enum AxisDirection {

    POSITIVE(1, "Towards positive"),
    NEGATIVE(-1, "Towards negative");

    private final int offSet;
    private final String description;

    AxisDirection(int offSet, String description) {
        this.offSet = offSet;
        this.description = description;
    }

    public int getOffSet() {
        return offSet;
    }

    public String getDescription() {
            return description;
        }
}
