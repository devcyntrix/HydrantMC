package org.hydrantmc.api.utils;

public class EntityProperty {

    private String key;
    private double value;
    private AttributeModifier[] modifiers;

    public EntityProperty(String key, double value, AttributeModifier... modifiers) {
        this.key = key;
        this.value = value;
        this.modifiers = modifiers;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public AttributeModifier[] getModifiers() {
        return modifiers;
    }

    public void setModifiers(AttributeModifier[] modifiers) {
        this.modifiers = modifiers;
    }

    public static class AttributeModifier {

    }

}
