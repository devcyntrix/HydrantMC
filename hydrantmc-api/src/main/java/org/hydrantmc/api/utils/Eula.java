package org.hydrantmc.api.utils;

import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import de.devcyntrix.configuration.annotation.ConfigurationSerializable;
import lombok.Data;

@Data
public class Eula implements ConfigurationSerializable {

    @ConfigurationProperty(name = "accepted")
    private boolean accepted = false;

}
