package org.hydrantmc.api.utils;

public enum Facing {

    DOWN(1, AxisDirection.NEGATIVE, Axis.Y, new Vector(0, -1, 0)),
    UP(0, AxisDirection.POSITIVE, Axis.Y, new Vector(0, 1, 0)),
    NORTH(3, AxisDirection.NEGATIVE, Axis.Z, new Vector(0, 0, -1)),
    SOUTH(2, AxisDirection.POSITIVE, Axis.Z, new Vector(0, 0, 1)),
    WEST(5, AxisDirection.NEGATIVE, Axis.X, new Vector(-1, 0, 0)),
    EAST(4, AxisDirection.POSITIVE, Axis.X, new Vector(1, 0, 0));

    private final int opposite;

    private final AxisDirection axisDirection;
    private final Axis axis;
    private final Vector directionVec;

    Facing(int opposite, AxisDirection axisDirection, Axis axis, Vector directionVec) {
        this.opposite = opposite;
        this.axisDirection = axisDirection;
        this.axis = axis;
        this.directionVec = directionVec;
    }

    public Facing getOpposite() {
        return Facing.values()[opposite];
    }

    public AxisDirection getAxisDirection() {
        return axisDirection;
    }

    public Axis getAxis() {
        return axis;
    }

    public Vector getDirectionVec() {
        return directionVec;
    }
}
