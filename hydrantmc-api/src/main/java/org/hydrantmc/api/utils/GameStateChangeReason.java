package org.hydrantmc.api.utils;

public enum GameStateChangeReason {

    INVALID_BED,
    END_RAINING,
    BEGIN_RAINING,
    CHANGE_GAME_MODE,
    ENTER_CREDITS,
    DEMO_MESSAGE,
    ARROW_HITTING_PLAYER,
    FADE_VALUE,
    FADE_TIME,
    PLAY_MOB_APPEARANCE;

    private final byte reasonCode;

    GameStateChangeReason() {
        this.reasonCode = (byte) ordinal();
    }

    GameStateChangeReason(int reasonCode) {
        this.reasonCode = (byte) reasonCode;
    }

    public byte getReasonCode() {
        return reasonCode;
    }

}
