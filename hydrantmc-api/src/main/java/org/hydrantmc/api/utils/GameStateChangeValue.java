package org.hydrantmc.api.utils;

public enum GameStateChangeValue {

    CHANGE_GAME_MODE_SURVIVAL(0),
    CHANGE_GAME_MODE_CREATIVE(1),
    CHANGE_GAME_MODE_ADVENTURE(2),
    CHANGE_GAME_MODE_SPECTATOR(3),

    DEMO_MESSAGE_WELCOME(0),
    DEMO_MESSAGE_MOVEMENT_CONTROLS(101),
    DEMO_MESSAGE_JUMP_CONTROL(102),
    DEMO_MESSAGE_INVENTORY_CONTROL(103),

    FADE_VALUE_BRIGHT(0),
    FADE_VALUE_DARK(1);

    private final float value;

    GameStateChangeValue(float value) {
        this.value = value;
    }

    public float getValue() {
        return value;
    }

    public static GameStateChangeValue byValue(float value) {
        for(GameStateChangeValue values : values()) {
            if(values.getValue() == value) {
                return values;
            }
        }

        return null;
    }

}
