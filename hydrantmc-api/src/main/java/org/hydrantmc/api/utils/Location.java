package org.hydrantmc.api.utils;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.world.World;
import org.hydrantmc.api.world.chunk.Chunk;
import org.hydrantmc.api.world.chunk.ChunkProvider;

public class Location {

    private World world;
    private double x;
    private double y;
    private double z;
    private float yaw;
    private float pitch;

    public Location(World world, double x, double y, double z) {
        this(world, x, y, z, 0F, 0F);
    }

    public Location(World world, double x, double y, double z, float yaw, float pitch) {
        this.world = world;
        this.x = x;
        this.y = y;
        this.z = z;
        this.yaw = yaw;
        this.pitch = pitch;
    }

    @Override
    public String toString() {
        return "Location{" +
                "world=" + world +
                ", x=" + x +
                ", y=" + y +
                ", z=" + z +
                ", yaw=" + yaw +
                ", pitch=" + pitch +
                '}';
    }

    public World getWorld() {
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }

    public Chunk getChunk() {
        Preconditions.checkNotNull(world, "the world cannot be null");

        ChunkProvider provider = world.getChunkProvider();
        Preconditions.checkNotNull(provider, "the chunk provider cannot be null");

        return provider.provideChunk(getChunkX(), getChunkY(), getChunkZ());
    }

    public double distance(Location o) {
        return Math.sqrt(distanceSquared(o));
    }

    public double distanceSquared(Location o) {
        if (o == null) {
            throw new IllegalArgumentException("Cannot measure distance to a null location");
        } else if (o.getWorld() == null || getWorld() == null) {
            throw new IllegalArgumentException("Cannot measure distance to a null world");
        } else if (o.getWorld() != getWorld()) {
            throw new IllegalArgumentException("Cannot measure distance between " + getWorld().getName() + " and " + o.getWorld().getName());
        }

        return Math.pow(x - o.x, 2) + Math.pow(y - o.y, 2) + Math.pow(z - o.z, 2);
    }

    public double getX() {
        return x;
    }

    public int getBlockX() {
        return (int) x;
    }

    public int getChunkX() {
        return (int) (getX() / 16);
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setBlockX(int x) {
        this.x = x;
    }

    public double getY() {
        return y;
    }

    public int getBlockY() {
        return (int) y;
    }

    public int getChunkY() {
        return (int) (getY() / 16);
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setBlockY(int y) {
        this.y = y;
    }

    public double getZ() {
        return z;
    }

    public int getBlockZ() {
        return (int) z;
    }

    public int getChunkZ() {
        return (int) (getZ() / 16);
    }

    public void setZ(double z) {
        this.z = z;
    }

    public void setBlockZ(int z) {
        this.z = z;
    }

    public float getYaw() {
        return yaw;
    }

    public void setYaw(float yaw) {
        this.yaw = yaw;
    }

    public float getPitch() {
        return pitch;
    }

    public void setPitch(float pitch) {
        this.pitch = pitch;
    }

    public AbstractBlock getBlock() {
        return getWorld().getBlockAtLocation(this);
    }

    public void setBlock(AbstractBlock abstractBlock) {
        getWorld().setBlockAtLocation(this, abstractBlock);
    }

    public Location add(double x, double y, double z) {
        if(x == 0 && y == 0 && z == 0) return null;
        this.x += x; this.y += y; this.z  += z;

        return this;
    }

    public Location subtract(double x, double y, double z) {
        if(x == 0 && y == 0 && z == 0) return null;
        this.x -= x; this.y -= y; this.z -= z;

        return this;
    }

    public void offset(Facing facing) {
        offset(facing, 1);
    }

    public void offset(Facing facing, int count) {
        Vector vector = facing.getDirectionVec();
        this.x += (vector.getX() * count);
        this.y += (vector.getY() * count);
        this.z += (vector.getZ() * count);
    }

    public long toLong() {
        long output;
        output = ((long)getBlockX() & 0x3FFFFFF) << 0x26;
        output |= ((long)getBlockY() & 0xFFF) << 0x1A;
        output |= ((long)getBlockZ() & 0x3FFFFFF);
        return output;
    }

    public static Location fromLong(long value) {
        int x = (int) (value >> 0x26);
        int y = (int) (value << 0x1A >> 0x34);
        int z = (int) (value << 0x26 >> 0x26);
        return new Location(null, x, y, z);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Double.compare(location.getX(), getX()) == 0 &&
                Double.compare(location.getY(), getY()) == 0 &&
                Double.compare(location.getZ(), getZ()) == 0 &&
                Float.compare(location.getYaw(), getYaw()) == 0 &&
                Float.compare(location.getPitch(), getPitch()) == 0 &&
                Objects.equal(getWorld(), location.getWorld());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getWorld(), getX(), getY(), getZ(), getYaw(), getPitch());
    }

}
