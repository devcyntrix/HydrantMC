package org.hydrantmc.api.utils;

public class MathUtils {

    public static int floor_double(double value) {
        int i = (int)value;
        return value < (double)i ? i - 1 : i;
    }

}
