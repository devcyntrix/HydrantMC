package org.hydrantmc.api.utils;

public enum Plane {

    HORIZONTAL, VERTICAL

}
