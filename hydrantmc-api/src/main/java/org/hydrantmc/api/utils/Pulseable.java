package org.hydrantmc.api.utils;

public interface Pulseable {

    void pulse();

}
