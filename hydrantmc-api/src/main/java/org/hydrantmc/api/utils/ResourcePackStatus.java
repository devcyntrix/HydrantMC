package org.hydrantmc.api.utils;

public enum ResourcePackStatus {

    SUCCESSFULLY_LOADED, DECLINED, FAILED_DOWNLOAD, ACCEPTED

}
