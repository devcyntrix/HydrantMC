package org.hydrantmc.api.utils;

public enum Sound {

    RANDOM_CLICK("random.click"),
    RANDOM_BREAK("random.break");

    final String name;

    Sound(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
