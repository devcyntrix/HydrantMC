package org.hydrantmc.api.utils;

public interface Tickable {

    void update();

}
