package org.hydrantmc.api.utils;

public enum TitleAction {

    TITLE, SUB_TITLE, TIMES, HIDE, RESET

}
