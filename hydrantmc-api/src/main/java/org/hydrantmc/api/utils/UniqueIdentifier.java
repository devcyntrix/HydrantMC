package org.hydrantmc.api.utils;

import java.util.UUID;

public interface UniqueIdentifier {

    /**
     *
     * @return the unique id of the command sender
     */
    UUID getUniqueId();

}
