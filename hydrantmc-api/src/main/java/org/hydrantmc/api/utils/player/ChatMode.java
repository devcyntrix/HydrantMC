package org.hydrantmc.api.utils.player;

public enum ChatMode {

    ENABLED, COMMANDS_ONLY, DISABLED

}
