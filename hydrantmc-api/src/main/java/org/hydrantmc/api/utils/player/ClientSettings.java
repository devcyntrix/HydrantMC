package org.hydrantmc.api.utils.player;

public class ClientSettings {

    private String locale;
    private byte viewDistance;
    private ChatMode chatMode;
    private boolean chatColorsEnabled;
    private SkinParts[] enabledSkinParts;

    public ClientSettings(String locale, byte viewDistance, ChatMode chatMode, boolean chatColorsEnabled, SkinParts[] enabledSkinParts) {
        this.locale = locale;
        this.viewDistance = viewDistance;
        this.chatMode = chatMode;
        this.chatColorsEnabled = chatColorsEnabled;
        this.enabledSkinParts = enabledSkinParts;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public byte getViewDistance() {
        return viewDistance;
    }

    public void setViewDistance(byte viewDistance) {
        this.viewDistance = viewDistance;
    }

    public ChatMode getChatMode() {
        return chatMode;
    }

    public void setChatMode(ChatMode chatMode) {
        this.chatMode = chatMode;
    }

    public boolean hasChatColorsEnabled() {
        return chatColorsEnabled;
    }

    public void setChatColorsEnabled(boolean chatColorsEnabled) {
        this.chatColorsEnabled = chatColorsEnabled;
    }

    public SkinParts[] getEnabledSkinParts() {
        return enabledSkinParts;
    }

    public void setEnabledSkinParts(SkinParts[] enabledSkinParts) {
        this.enabledSkinParts = enabledSkinParts;
    }
}