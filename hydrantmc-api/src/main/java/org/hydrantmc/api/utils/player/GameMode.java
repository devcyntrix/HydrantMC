package org.hydrantmc.api.utils.player;

public enum GameMode {

    SURVIVAL, CREATIVE, ADVENTURE, SPECTATOR;

    public static GameMode getGameMode(String idOrName) {
        try {
            return GameMode.valueOf(idOrName.toUpperCase());
        } catch(Exception exception1) {
            try {
                return GameMode.values()[Integer.parseInt(idOrName)];
            } catch(Exception exception2) {
                return null;
            }
        }
    }

}
