package org.hydrantmc.api.utils.player;

import com.google.common.base.MoreObjects;
import com.google.common.base.Objects;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.hydrantmc.api.utils.serializer.GameProfileSerializer;
import org.hydrantmc.api.utils.serializer.PropertyMapSerializer;
import org.hydrantmc.api.utils.serializer.PropertySerializer;

import java.util.UUID;

public class GameProfile {

    public static final Gson GSON = new GsonBuilder().registerTypeAdapter(GameProfile.class, new GameProfileSerializer()).registerTypeAdapter(PropertyMap.class, new PropertyMapSerializer()).registerTypeAdapter(Property.class, new PropertySerializer()).create();

    private final UUID uniqueId;
    private final String username;

    private PropertyMap propertyMap;

    private boolean legacy;

    public GameProfile(UUID uniqueId, String username) {
        if (uniqueId == null && (username == null || username.isEmpty())) {
            throw new IllegalArgumentException("Name and ID cannot both be blank");
        }
        this.uniqueId = uniqueId;
        this.username = username;
        propertyMap = new PropertyMap();
    }

    public UUID getUniqueId() {
        return uniqueId;
    }

    public String getUsername() {
        return username;
    }

    public PropertyMap getPropertyMap() {
        return propertyMap;
    }

    public void setPropertyMap(PropertyMap propertyMap) {
        this.propertyMap = propertyMap;
    }

    public boolean isLegacy() {
        return legacy;
    }

    public void setLegacy(boolean legacy) {
        this.legacy = legacy;
    }

    public boolean isComplete() {
        return this.uniqueId != null && (getUsername() != null && !getUsername().isEmpty());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GameProfile)) return false;
        GameProfile that = (GameProfile) o;
        return isLegacy() == that.isLegacy() &&
                Objects.equal(getUniqueId(), that.getUniqueId()) &&
                Objects.equal(getUsername(), that.getUsername()) &&
                Objects.equal(getPropertyMap(), that.getPropertyMap());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getUniqueId(), getUsername(), getPropertyMap(), isLegacy());
    }

    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("uniqueId", uniqueId)
                .add("username", username)
                .add("propertyMap", propertyMap)
                .add("legacy", legacy)
                .toString();
    }
}
