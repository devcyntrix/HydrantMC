package org.hydrantmc.api.utils.player;

import com.google.common.base.Objects;

public class PlayerAbilities {

    private boolean invulnerable;

    private boolean flying;
    private boolean allowFlying;

    private boolean creativeMode;

    private float flySpeed;
    private float walkSpeed;

    public PlayerAbilities(boolean invulnerable, boolean flying, boolean allowFlying, boolean creativeMode, float flySpeed, float walkSpeed) {
        this.invulnerable = invulnerable;
        this.flying = flying;
        this.allowFlying = allowFlying;
        this.creativeMode = creativeMode;
        this.flySpeed = flySpeed;
        this.walkSpeed = walkSpeed;
    }

    public PlayerAbilities() {
        this(false, false, false, false, 0.05F, 0.1F);
    }

    public boolean isInvulnerable() {
        return invulnerable;
    }

    public void setInvulnerable(boolean invulnerable) {
        this.invulnerable = invulnerable;
    }

    public boolean isFlying() {
        return flying;
    }

    public void setFlying(boolean flying) {
        this.flying = flying;
    }

    public boolean isAllowFlying() {
        return allowFlying;
    }

    public void setAllowFlying(boolean allowFlying) {
        this.allowFlying = allowFlying;
    }

    public boolean isCreativeMode() {
        return creativeMode;
    }

    public void setCreativeMode(boolean creativeMode) {
        this.creativeMode = creativeMode;
    }

    public float getFlySpeed() {
        return flySpeed;
    }

    public void setFlySpeed(float flySpeed) {
        this.flySpeed = flySpeed;
    }

    public float getWalkSpeed() {
        return walkSpeed;
    }

    public void setWalkSpeed(float walkSpeed) {
        this.walkSpeed = walkSpeed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PlayerAbilities that = (PlayerAbilities) o;
        return invulnerable == that.invulnerable &&
                flying == that.flying &&
                allowFlying == that.allowFlying &&
                creativeMode == that.creativeMode &&
                Float.compare(that.flySpeed, flySpeed) == 0 &&
                Float.compare(that.walkSpeed, walkSpeed) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(invulnerable, flying, allowFlying, creativeMode, flySpeed, walkSpeed);
    }
}
