package org.hydrantmc.api.utils.player;

public enum SkinParts {

    CAPE((byte)0),
    JACKET((byte)1),
    LEFT_SLEEVE((byte)2),
    RIGHT_SLEEVE((byte)3),
    LEFT_PANTS_LEG((byte)4),
    RIGHT_PANTS_LEG((byte)5),
    HAT((byte)6);

    private final byte id;
    private final short bitMask;

    SkinParts(byte id) {
        this.id = id;
        this.bitMask = (short) (1 << id);
    }

    public byte getId() {
        return id;
    }

    public short getBitMask() {
        return bitMask;
    }
}
