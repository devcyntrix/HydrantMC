package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.chat.*;
import org.hydrantmc.api.chat.event.ClickEvent;
import org.hydrantmc.api.chat.event.HoverEvent;

import java.lang.reflect.Type;
import java.util.LinkedList;
import java.util.List;

public class ChatComponentSerializer implements JsonSerializer<ChatComponent>, JsonDeserializer<ChatComponent> {

    @Override
    public ChatComponent deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!jsonElement.isJsonObject()) return null;
        JsonObject object = (JsonObject) jsonElement;

        ChatComponent component = null;

        if(object.has("text")) {
            component = new TextComponent(object.get("text").getAsString());
        }

        if(object.has("translate")) {
            JsonArray array = object.getAsJsonArray("with");
            if(array == null) array = new JsonArray();

            List<ChatComponent> list = new LinkedList<>();
            for(JsonElement element : array) {
                if(!(element instanceof JsonObject)) continue;
                list.add(context.deserialize(element, ChatComponent.class));
            }

            component = new TranslationComponent(object.get("translate").getAsString(), list.toArray(new ChatComponent[0]));
        }

        if(object.has("score")) {
            String name = object.get("name").getAsString();
            String objective = object.get("objective").getAsString();

            ScoreComponent scoreComponent = new ScoreComponent(name, objective);

            if(object.has("value")) {
                scoreComponent.setValue(object.get("value").getAsString());
            }

            component = scoreComponent;
        }

        if(object.has("selector")) {
            component = new SelectorComponent(object.get("selector").getAsString());
        }

        if(component == null) return null;

        if(object.has("color")) component.setColor(ChatColor.valueOf(object.get("color").getAsString()));
        if(object.has("bold")) component.setBold(object.get("bold").getAsBoolean());
        if(object.has("italic")) component.setItalic(object.get("italic").getAsBoolean());
        if(object.has("underlined")) component.setUnderlined(object.get("underlined").getAsBoolean());
        if(object.has("strikethrough")) component.setStrikeThrough(object.get("strikethrough").getAsBoolean());
        if(object.has("obfuscated")) component.setObfuscated(object.get("obfuscated").getAsBoolean());

        if(object.has("clickEvent")) component.setClickEvent(context.deserialize(object.getAsJsonObject("clickEvent"), ClickEvent.class));
        if(object.has("hoverEvent")) component.setHoverEvent(context.deserialize(object.getAsJsonObject("hoverEvent"), HoverEvent.class));

        if(object.has("extra")) {
            JsonArray array = object.getAsJsonArray("extra");
            if(array == null) return null;

            List<ChatComponent> components = new LinkedList<>();
            for(JsonElement element : array) {
                if(!element.isJsonObject()) continue;
                components.add(context.deserialize(element, ChatComponent.class));
            }
            component.setExtra(components);
        }

        if(object.has("insertion")) component.setInsertion(object.get("insertion").getAsString());

        return component;
    }

    @Override
    public JsonElement serialize(ChatComponent component, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();

        ChatColor color = component.getColor();
        if(color != null) object.addProperty("color", color.getName());
        if(component.isBold()) object.addProperty("bold", true);
        if(component.isItalic()) object.addProperty("italic", true);
        if(component.isUnderlined()) object.addProperty("underlined", true);
        if(component.isStrikeThrough()) object.addProperty("strikethrough", true);
        if(component.isObfuscated()) object.addProperty("obfuscated", true);

        if(component.getClickEvent() != null) object.add("clickEvent", context.serialize(component.getClickEvent(), ClickEvent.class));
        if(component.getHoverEvent() != null) object.add("hoverEvent", context.serialize(component.getHoverEvent(), HoverEvent.class));

        JsonArray array = new JsonArray();
        for(ChatComponent baseComponent : component.getExtra()) {
            array.add(context.serialize(baseComponent, baseComponent.getClass()));
        }
        if(array.size() > 0) {
            object.add("extra", array);
        }

        if(component.getInsertion() != null) object.addProperty("insertion", component.getInsertion());

        if(component instanceof TextComponent) {
            TextComponent textComponent = (TextComponent) component;
            object.addProperty("text", textComponent.getText());
        }

        if(component instanceof TranslationComponent) {
            TranslationComponent translationComponent = (TranslationComponent) component;
            object.addProperty("translate", translationComponent.getTranslationKey());

            JsonArray args = new JsonArray();
            for(ChatComponent baseComponent : translationComponent.getFormatArgs()) {
                args.add(context.serialize(baseComponent, baseComponent.getClass()));
            }
            if(args.size() > 0) {
                object.add("with", args);
            }
        }

        if(component instanceof ScoreComponent) {
            ScoreComponent scoreComponent = (ScoreComponent) component;
            object.addProperty("name", scoreComponent.getName());
            object.addProperty("objective", scoreComponent.getObjective());
            object.addProperty("value", scoreComponent.getValue());
        }

        if(component instanceof SelectorComponent) {
            SelectorComponent selectorComponent = (SelectorComponent) component;
            object.addProperty("selector", selectorComponent.getSelector());
        }

        return object;
    }
}
