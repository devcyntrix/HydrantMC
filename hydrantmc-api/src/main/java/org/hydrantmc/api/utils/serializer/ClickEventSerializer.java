package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.chat.event.ClickAction;
import org.hydrantmc.api.chat.event.ClickEvent;

import java.lang.reflect.Type;

public class ClickEventSerializer implements JsonSerializer<ClickEvent>, JsonDeserializer<ClickEvent> {

    @Override
    public ClickEvent deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!(element instanceof JsonObject)) return null;
        JsonObject object = (JsonObject) element;

        String action = object.get("action").getAsString();
        String value = object.get("value").getAsString();

        ClickAction clickAction = ClickAction.valueOf(action);
        return new ClickEvent(clickAction, value);
    }
    @Override
    public JsonElement serialize(ClickEvent clickEvent, Type type, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("action", clickEvent.getClickAction().name().toLowerCase());
        jsonObject.addProperty("value", clickEvent.getValue());
        return jsonObject;
    }
}
