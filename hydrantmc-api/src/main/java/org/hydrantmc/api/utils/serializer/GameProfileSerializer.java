package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.utils.player.GameProfile;
import org.hydrantmc.api.utils.player.PropertyMap;

import java.lang.reflect.Type;
import java.util.UUID;

public class GameProfileSerializer implements JsonSerializer<GameProfile>, JsonDeserializer<GameProfile> {

    @Override
    public JsonElement serialize(GameProfile gameProfile, Type type, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("id", gameProfile.getUniqueId().toString().replace("-", ""));
        jsonObject.addProperty("name", gameProfile.getUsername());

        if(gameProfile.getPropertyMap() != null) {
            jsonObject.add("properties", context.serialize(gameProfile.getPropertyMap(), PropertyMap.class));
        }

        if(gameProfile.isLegacy()) {
            jsonObject.addProperty("legacy", gameProfile.isLegacy());
        }
        return jsonObject;
    }

    @Override
    public GameProfile deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!(jsonElement instanceof JsonObject)) return null;
        JsonObject object = (JsonObject) jsonElement;

        String id = object.get("id").getAsString().replaceFirst("(\\w{8})(\\w{4})(\\w{4})(\\w{4})(\\w{12})", "$1-$2-$3-$4-$5");
        UUID uniqueId = UUID.fromString(id);
        String username = object.get("name").getAsString();

        GameProfile gameProfile = new GameProfile(uniqueId, username);

        if(object.has("properties")) {
            gameProfile.setPropertyMap(context.deserialize(object.getAsJsonArray("properties"), PropertyMap.class));
        }

        if(object.has("legacy")) {
            gameProfile.setLegacy(object.get("legacy").getAsBoolean());
        }

        return gameProfile;
    }
}
