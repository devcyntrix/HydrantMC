package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.chat.event.HoverAction;
import org.hydrantmc.api.chat.event.HoverEvent;

import java.lang.reflect.Type;

public class HoverEventSerializer implements JsonSerializer<HoverEvent>, JsonDeserializer<HoverEvent> {

    @Override
    public HoverEvent deserialize(JsonElement element, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!(element instanceof JsonObject)) return null;
        JsonObject object = (JsonObject) element;

        String action = object.get("action").getAsString();

        HoverAction hoverAction = HoverAction.valueOf(action);
        ChatComponent component = context.deserialize(object.get("value"), ChatComponent.class);

        return new HoverEvent(hoverAction, component);
    }

    @Override
    public JsonElement serialize(HoverEvent hoverEvent, Type type, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("action", hoverEvent.getHoverAction().name().toLowerCase());
        jsonObject.add("value", context.serialize(hoverEvent.getComponents(), ChatComponent.class));
        return jsonObject;
    }
}
