package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.utils.player.Property;
import org.hydrantmc.api.utils.player.PropertyMap;

import java.lang.reflect.Type;
import java.util.Map;

public class PropertyMapSerializer implements JsonDeserializer<PropertyMap>, JsonSerializer<PropertyMap> {

    @Override
    public JsonElement serialize(PropertyMap propertyMap, Type type, JsonSerializationContext context) {
        JsonArray array = new JsonArray();

        for(Map.Entry<String, Property> entry : propertyMap.entries()) {
            array.add(context.serialize(entry.getValue(), Property.class));
        }

        return array;
    }

    @Override
    public PropertyMap deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!(jsonElement instanceof JsonArray)) return null;

        JsonArray array = (JsonArray) jsonElement;
        PropertyMap propertyMap = new PropertyMap();

        for(int i = 0; array.size() < i; i++) {
            Property property = context.deserialize(array.get(i), Property.class);
            if(property == null) continue;
            propertyMap.put(property.getName(), property);
        }

        return propertyMap;
    }

}
