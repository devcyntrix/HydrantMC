package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.utils.player.Property;

import java.lang.reflect.Type;

public class PropertySerializer implements JsonSerializer<Property>, JsonDeserializer<Property> {

    @Override
    public JsonElement serialize(Property property, Type type, JsonSerializationContext context) {
        JsonObject object = new JsonObject();

        object.addProperty("name", property.getName());
        object.addProperty("value", property.getValue());
        if(property.hasSignature()) {
            object.addProperty("signature", property.getSignature());
        }

        return object;
    }

    @Override
    public Property deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!(jsonElement instanceof JsonObject)) return null;
        JsonObject object = (JsonObject) jsonElement;

        String name = object.get("name").getAsString();
        String value = object.get("value").getAsString();
        String signature = object.get("signature").getAsString();

        return new Property(name, value, signature);
    }

}
