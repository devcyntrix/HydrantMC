package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.utils.status.ServerStatusPlayerSample;
import org.hydrantmc.api.utils.status.ServerStatusPlayers;

import java.lang.reflect.Type;
import java.util.UUID;

public class ServerStatusPlayersSerializer implements JsonDeserializer<ServerStatusPlayers>, JsonSerializer<ServerStatusPlayers> {

    @Override
    public ServerStatusPlayers deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = jsonElement.getAsJsonObject().getAsJsonObject("players");

        int maxPlayersCount = jsonObject.get("max").getAsInt();
        int onlinePlayerCount = jsonObject.get("online").getAsInt();

        ServerStatusPlayers serverPlayers = new ServerStatusPlayers(onlinePlayerCount, maxPlayersCount);


        if (jsonObject.has("sample") && jsonObject.get("sample").isJsonArray()) {
            JsonArray jsonArray = jsonObject.get("sample").getAsJsonArray();
            if (jsonArray.size() > 0) {
                ServerStatusPlayerSample[] samples = new ServerStatusPlayerSample[jsonArray.size()];

                for (int i = 0; i < samples.length; i++) {
                    JsonObject sampleAsObject = jsonArray.get(i).getAsJsonObject();
                    String uniqueIdAsString = sampleAsObject.get("id").getAsString();
                    String name = sampleAsObject.get("name").getAsString();
                    samples[i] = new ServerStatusPlayerSample(name, UUID.fromString(uniqueIdAsString));
                }

                serverPlayers.setSamples(samples);
            }
        }

        return serverPlayers;
    }

    @Override
    public JsonElement serialize(ServerStatusPlayers serverPlayers, Type type, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("max", serverPlayers.getMaxPlayersCount());
        jsonObject.addProperty("online", serverPlayers.getOnlinePlayerCount());

        if (serverPlayers.getSamples() != null && serverPlayers.getSamples().length > 0) {
            JsonArray jsonArray = new JsonArray();
            ServerStatusPlayerSample[] samples = serverPlayers.getSamples();

            for(ServerStatusPlayerSample sample : samples) {
                JsonObject object = new JsonObject();
                object.addProperty("id", sample.getUniqueId().toString());
                object.addProperty("name", sample.getName());
                jsonArray.add(object);
            }

            jsonObject.add("sample", jsonArray);
        }

        return jsonObject;
    }
}