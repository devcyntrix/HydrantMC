package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.utils.status.ServerStatus;
import org.hydrantmc.api.utils.status.ServerStatusPlayers;
import org.hydrantmc.api.utils.status.ServerStatusVersion;

import java.lang.reflect.Type;

public class ServerStatusSerializer implements JsonSerializer<ServerStatus>, JsonDeserializer<ServerStatus> {

    @Override
    public JsonElement serialize(ServerStatus status, Type type, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();

        if(status.getDescription() != null) jsonObject.addProperty("description", status.getDescription());
        if(status.getServerVersion() != null) jsonObject.add("version", context.serialize(status.getServerVersion(), ServerStatusVersion.class));
        if(status.getServerPlayers() != null) jsonObject.add("players", context.serialize(status.getServerPlayers(), ServerStatusPlayers.class));
        if(status.getFavicon() != null) jsonObject.addProperty("favicon", status.getFavicon().toString());
        return jsonObject;
    }

    @Override
    public ServerStatus deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        if(!(jsonElement instanceof JsonObject)) return null;

        JsonObject jsonObject = jsonElement.getAsJsonObject();
        ServerStatus serverStatus = new ServerStatus();

        if(jsonObject.has("description")) serverStatus.setDescription(jsonObject.get("description").getAsString());
        if(jsonObject.has("version")) serverStatus.setServerVersion(context.deserialize(jsonObject.getAsJsonObject("version"), ServerStatusVersion.class));
        if(jsonObject.has("players")) serverStatus.setServerPlayers(context.deserialize(jsonObject.getAsJsonObject("players"), ServerStatusPlayers.class));

        return serverStatus;
    }
}
