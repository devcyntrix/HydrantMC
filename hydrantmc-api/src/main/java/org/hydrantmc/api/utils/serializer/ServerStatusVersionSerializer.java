package org.hydrantmc.api.utils.serializer;

import com.google.gson.*;
import org.hydrantmc.api.utils.status.ServerStatusVersion;

import java.lang.reflect.Type;

public class ServerStatusVersionSerializer implements JsonDeserializer<ServerStatusVersion>, JsonSerializer<ServerStatusVersion> {

    @Override
    public ServerStatusVersion deserialize(JsonElement jsonElement, Type type, JsonDeserializationContext context) throws JsonParseException {
        JsonObject jsonObject = jsonElement.getAsJsonObject().getAsJsonObject("version");

        int protocolId = jsonObject.get("protocol").getAsInt();
        String protocolName = jsonObject.get("name").getAsString();

        return new ServerStatusVersion(protocolId, protocolName);
    }

    @Override
    public JsonElement serialize(ServerStatusVersion serverVersion, Type type, JsonSerializationContext context) {
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("name", serverVersion.getProtocolName());
        jsonObject.addProperty("protocol", serverVersion.getProtocolId());
        return jsonObject;
    }
}
