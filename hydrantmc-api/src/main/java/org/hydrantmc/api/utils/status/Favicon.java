package org.hydrantmc.api.utils.status;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.Base64;

public class Favicon {

    private byte[] bytes;

    public Favicon(byte[] bytes) {
        if ( bytes.length > Short.MAX_VALUE ) {
            throw new IllegalArgumentException( "Favicon file too large for server to process" );
        }

        this.bytes = bytes;
    }

    public Favicon(BufferedImage image) {
        if (image.getWidth() != 64 || image.getHeight() != 64 ) {
            throw new IllegalArgumentException( "Server icon must be exactly 64x64 pixels" );
        }

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        try {
            ImageIO.write(image, "PNG", stream);
        } catch (IOException e) {
            e.printStackTrace();
        }

        byte[] bs = stream.toByteArray();

        if ( bs.length > Short.MAX_VALUE ) {
            throw new IllegalArgumentException( "Favicon file too large for server to process" );
        }

        this.bytes = bs;
    }

    public String getEncoding() {
        return "base64";
    }

    public String getMime() {
        return "image/png";
    }

    public byte[] getEncodedImage() {
        return Base64.getEncoder().encode(this.bytes);
    }

    @Override
    public String toString() {
        return "data:" + getMime() + ";" + getEncoding() + "," + new String(getEncodedImage());
    }
}
