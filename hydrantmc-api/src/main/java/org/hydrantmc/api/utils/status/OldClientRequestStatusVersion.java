package org.hydrantmc.api.utils.status;

public enum OldClientRequestStatusVersion {

    MINECRAFT_1_3,
    MINECRAFT_1_4_1_5_1_6

}
