package org.hydrantmc.api.utils.status;

import lombok.Data;
import org.hydrantmc.api.ServerInfoProperties;
import org.hydrantmc.api.chat.ChatColor;

import javax.imageio.ImageIO;
import java.io.File;
import java.util.UUID;

@Data
public class ServerStatus {

    private String description;
    private ServerStatusVersion serverVersion;
    private ServerStatusPlayers serverPlayers;
    private Favicon favicon;

    public ServerStatus() {
    }

    public ServerStatus(String description, ServerStatusVersion serverVersion, ServerStatusPlayers serverPlayers, Favicon favicon) {
        this.description = description;
        this.serverVersion = serverVersion;
        this.serverPlayers = serverPlayers;
        this.favicon = favicon;
    }

    public ServerStatus(int onlinePlayers, ServerInfoProperties properties) {
        this.description = String.join("\n", properties.getMotd());
        this.serverVersion = new ServerStatusVersion(properties.getProtocolId(), properties.getProtocolName());

        String[] playerInfo = properties.getPlayerInfo();
        ServerStatusPlayerSample[] samples = new ServerStatusPlayerSample[playerInfo.length];

        for (int i = 0; i < playerInfo.length; i++) {
            samples[i] = new ServerStatusPlayerSample(playerInfo[i], UUID.randomUUID());
        }

        this.serverPlayers = new ServerStatusPlayers(onlinePlayers, properties.getMaxPlayers(), samples);

        File favicon = new File("server-icon.png");
        if(favicon.exists()) {
            try {
                this.favicon = new Favicon(ImageIO.read(favicon));
            } catch (Exception exception) {
                System.err.println("Failed to load the favicon!");
                exception.printStackTrace();
            }
        }
    }

    public String toLegacyString(OldClientRequestStatusVersion version) {

        String feedback = null;
        switch (version) {
            case MINECRAFT_1_3:
                feedback = String.format("%s\u00a7%d\u00a7%d", this.description, this.serverPlayers.getOnlinePlayerCount(), this.serverPlayers.getMaxPlayersCount());
                feedback = feedback.replace("\n", "");
                feedback = ChatColor.stripColorAndFormat(feedback);
                break;
            case MINECRAFT_1_4_1_5_1_6:
                feedback = String.format("\u00a71\u0000%d\u0000%s\u0000%s\u0000%d\u0000%d", 127, getServerVersion().getProtocolName(), this.description, this.getServerPlayers().getOnlinePlayerCount(), this.serverPlayers.getMaxPlayersCount());
                feedback = feedback.replace("\n", "");
                break;
        }
        return feedback;
    }

}
