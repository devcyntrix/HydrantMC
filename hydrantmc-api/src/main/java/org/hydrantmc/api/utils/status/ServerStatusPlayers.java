package org.hydrantmc.api.utils.status;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ServerStatusPlayers {

    private int onlinePlayerCount;
    private int maxPlayersCount;
    private ServerStatusPlayerSample[] samples;

    public ServerStatusPlayers(int onlinePlayerCount, int maxPlayersCount) {
        this.onlinePlayerCount = onlinePlayerCount;
        this.maxPlayersCount = maxPlayersCount;
    }
}