package org.hydrantmc.api.utils.status;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class ServerStatusVersion {

    private int protocolId;
    private String protocolName;

}