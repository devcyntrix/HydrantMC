package org.hydrantmc.api.world;

public enum Difficulty {

    PEACEFUL("Peaceful"),
    EASY("Easy"),
    NORMAL("Normal"),
    HARD("Hard");

    private final String name;

    Difficulty(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public static Difficulty getDifficulty(String idOrName) {
        try {
            return Difficulty.valueOf(idOrName.toUpperCase());
        } catch(Exception exception1) {
            try {
                return Difficulty.values()[Integer.parseInt(idOrName)];
            } catch(Exception exception2) {
                return null;
            }
        }
    }
}
