package org.hydrantmc.api.world;

import org.hydrantmc.api.world.chunk.ChunkGenerator;
import org.hydrantmc.api.world.generator.DebugGenerator;
import org.hydrantmc.api.world.generator.FlatRoomGenerator;
import org.hydrantmc.api.world.generator.NormalGenerator;

import java.util.HashMap;
import java.util.Map;

public enum TerrainType {

    NORMAL("Default", new NormalGenerator()),
    FLAT("Flat", new FlatRoomGenerator()),
    VERSION_1_1("Default 1 1", null),
    LARGE_BIOMES("Large biomes", null),
    AMPLIFIED("Amplified", null),
    CUSTOMIZED("Customized", null),
    DEBUG("Debug", new DebugGenerator());

    private final static Map<String, TerrainType> BY_NAME = new HashMap<>();

    static {
        for (TerrainType type : values()) {
            BY_NAME.put(type.name().toLowerCase(), type);
        }
    }

    private final String name;
    private ChunkGenerator chunkGenerator;

    TerrainType(String name, ChunkGenerator chunkGenerator) {
        this.name = name;
        this.chunkGenerator = chunkGenerator;
    }

    public String getName() {
        return name;
    }

    public ChunkGenerator getChunkGenerator() {
        return chunkGenerator;
    }

    public static TerrainType getByName(String name) {
        return BY_NAME.get(name.toUpperCase());
    }

}
