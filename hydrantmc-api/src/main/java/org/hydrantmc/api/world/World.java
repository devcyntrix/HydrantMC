package org.hydrantmc.api.world;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.entity.Entity;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.Pulseable;
import org.hydrantmc.api.world.chunk.Chunk;
import org.hydrantmc.api.world.chunk.ChunkProvider;

import java.util.List;

public interface World extends Pulseable {

    Server getServer();

    /**
     * Gets the unique name of this world
     *
     * @return Name of this world
     */
    String getName();

    /**
     * Gets the Difficulty of the world.
     *
     * @return The difficulty of the world.
     */
    Difficulty getDifficulty();

    /**
     * Sets the Difficulty of the world.
     *
     * @param difficulty the new difficulty you want to set the world to
     */
    void setDifficulty(Difficulty difficulty);

    /**
     * Gets the type of this world.
     *
     * @return Type of this world.
     */
    WorldType getWorldType();

    /**
     * Sets the world type of the world.
     *
     * @param worldType the new world type you want to set the world to
     */
    void setWorldType(WorldType worldType);

    ChunkProvider getChunkProvider();

    /**
     * Gets the Block at the given {@link Location}
     *
     * @param location Location of the block
     * @return Block at the given location
     */
    AbstractBlock getBlockAtLocation(Location location);

    /**
     * Sets the Block at the given {@link Location}.
     *
     * @param location Location of the block
     * @param block The Block to set
     */
    void setBlockAtLocation(Location location, AbstractBlock block);

    /**
     * Gets the {@link Chunk} at the given {@link Location}
     *
     * @param location Location of the chunk
     * @return Chunk at the given location
     */
    Chunk getChunkAtLocation(Location location);

    /**
     * Creates a entity at the given {@link Location}
     *
     * @param entityClass The entity to spawn
     * @param location The location to spawn the entity
     * @return Resulting Entity of this method, or null if it was unsuccessful
     */
    <T extends Entity> T spawnEntityAtLocation(Class<T> entityClass, Location location);

    /**
     * Get a list of all entities in this World
     *
     * @return A List of all Entities currently residing in this world
     */
    List<Entity> getEntities();

}
