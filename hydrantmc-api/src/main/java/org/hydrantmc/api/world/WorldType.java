package org.hydrantmc.api.world;

import java.util.HashMap;
import java.util.Map;

public enum WorldType {

        NETHER(-1),
        OVERWORLD(0),
        THE_END(1);

        private final int id;
        private static final Map<Integer, WorldType> LOOKUP = new HashMap<>();

        WorldType(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public static WorldType getEnvironment(int id) {
            return LOOKUP.get(id);
        }

        static {
            for (WorldType env : values()) {
                LOOKUP.put(env.getId(), env);
            }
        }
    }