package org.hydrantmc.api.world.chunk;

import org.hydrantmc.api.world.World;

import java.util.Random;

public abstract class BlockPopulator {

    public abstract void populate(World world, ChunkColumn source, Random random);

}
