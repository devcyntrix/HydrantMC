package org.hydrantmc.api.world.chunk;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.world.World;

/**
 * Represents a region of 16x16x16 in Minecraft
 */
public class Chunk {

    private final Server server;
    private final World world;
    private final ChunkColumn parent;

    private final int chunkY;

    private int blockCount;

    private final char[] data;
    private final NibbleArray blockLightArray;
    private final NibbleArray skylightArray;

    public Chunk(ChunkColumn parent, int chunkY) {
        this.server = parent.getServer();
        this.world = parent.getWorld();
        this.parent = parent;

        this.chunkY = chunkY;

        this.blockCount = 0;

        this.data = new char[16 * 16 * 16];
        this.blockLightArray = new NibbleArray();
        this.skylightArray = new NibbleArray();
    }

    public Server getServer() {
        return server;
    }

    public World getWorld() {
        return world;
    }

    public ChunkColumn getChunkColumn() {
        return parent;
    }

    public int getChunkX() {
        return parent.getChunkColumnX();
    }

    public int getChunkY() {
        return this.chunkY;
    }

    public int getChunkZ() {
        return parent.getChunkColumnZ();
    }

    public boolean isEmpty() {
        return this.blockCount == 0;
    }

    public void setBlock(byte x, byte y, byte z, AbstractBlock block) {
        Preconditions.checkArgument(x >= 0 && x <= 15, "x cannot be larger than 15 or lower than 0.");
        Preconditions.checkArgument(y >= 0 && y <= 15, "y cannot be larger than 15 or lower than 0.");
        Preconditions.checkArgument(z >= 0 && z <= 15, "z cannot be larger than 15 or lower than 0.");

        AbstractBlock blockBefore = getBlock(x, y, z);
        if(blockBefore != null && blockBefore.getBlockId() != 0) {
            this.blockCount--;
        }

        if(block != null && block.getBlockId() != 0) {
            this.blockCount++;
        }

        if(block == null || block.getBlockId() == 0) {
            this.data[y << 8 | z << 4 | x] = 0;
            return;
        }

        this.data[y << 8 | z << 4 | x] = (char) (block.getBlockId() << 4 | block.getMetaId());
    }

    public AbstractBlock getBlock(byte x, byte y, byte z) {
        Preconditions.checkArgument(x >= 0 && x <= 15, "x cannot be larger than 15 or lower than 0.");
        Preconditions.checkArgument(y >= 0 && y <= 15, "y cannot be larger than 15 or lower than 0.");
        Preconditions.checkArgument(z >= 0 && z <= 15, "z cannot be larger than 15 or lower than 0.");

        return this.server.getBlockRegistry().getBlock(this.data[y << 8 | z << 4 | x] >> 4);
    }

    public char[] getData() {
        return data;
    }

    public NibbleArray getBlockLightArray() {
        return blockLightArray;
    }

    public NibbleArray getSkylightArray() {
        return skylightArray;
    }
}
