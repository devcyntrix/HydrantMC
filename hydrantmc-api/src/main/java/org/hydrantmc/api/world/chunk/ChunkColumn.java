package org.hydrantmc.api.world.chunk;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.world.World;

/**
 * Represents 16 chunks stacked over
 */
public class ChunkColumn {

    private final World world;

    /**
     * The x coordinate from chunk in the world
     */
    private final int chunkX;
    /**
     * The z coordinate from chunk in the world
     */
    private final int chunkZ;

    /**
     * Stacked over chunks
     */
    private final Chunk[] chunks;
    private final byte[] biomeArray;

    public ChunkColumn(World world, int chunkX, int chunkZ) {
        this.world = world;
        this.chunkX = chunkX;
        this.chunkZ = chunkZ;
        this.chunks = new Chunk[16];
        this.biomeArray = new byte[16 * 16];
    }

    public Server getServer() {
        return this.world.getServer();
    }

    /**
     * @return the world in which the chunk is located.
     */
    public World getWorld() {
        return this.world;
    }

    /**
     * @return the x-coordinate of the chunk.
     */
    public int getChunkColumnX() {
        return this.chunkX;
    }

    /**
     * @return the z-coordinate of the chunk.
     */
    public int getChunkColumnZ() {
        return this.chunkZ;
    }

    /**
     * @return the stacked over chunks
     */
    public Chunk[] getChunks() {
        return this.chunks;
    }

    /**
     * @param chunkY is the y-position is from the stacked chunks
     * @return the chunk at the position by chunkY
     */
    public Chunk getChunkByY(int chunkY) {
        Preconditions.checkArgument(chunkY <= 15 && chunkY >= 0, "Chunk Y can not be higher than 15 and not be lower than 0");

        Chunk chunk = this.chunks[chunkY];
        if(chunk == null) {
            chunk = new Chunk(this, chunkY);
            this.chunks[chunkY] = chunk;
        }
        return chunk;
    }

    public byte[] getBiomeArray() {
        return biomeArray;
    }

    public boolean isEmpty() {
        boolean empty = true;
        for (Chunk chunk : this.chunks) {
            if (!chunk.isEmpty()) {
                empty = false;
                break;
            }
        }
        return empty;
    }

    public void setBlock(int x, int y, int z, AbstractBlock block) {
        Preconditions.checkArgument(y <= 255, "Y can not be higher than 255");
        Preconditions.checkArgument(y >= 0, "Y can not be lower than 0");
        Preconditions.checkArgument(x <= 15, "X can not be higher than 15");
        Preconditions.checkArgument(x >= 0, "X can not be lower than 0");
        Preconditions.checkArgument(z <= 15, "Z can not be higher than 15");
        Preconditions.checkArgument(z >= 0, "Z can not be lower than 0");

        byte yInChunk = (byte) (y % 16);
        int chunkY = (y - yInChunk) / 16;

        Preconditions.checkArgument(chunkY <= 15 && chunkY >= 0, "Chunk Y can not be higher than 15 and not be lower than 0");

        Chunk chunk = getChunkByY(chunkY);

        byte xInChunk = (byte) (x % 16);
        byte zInChunk = (byte) (z % 16);

        chunk.setBlock(xInChunk, yInChunk, zInChunk, block);
    }

    public AbstractBlock getBlock(int x, int y, int z) {
        Preconditions.checkArgument(y <= 255, "Y can not be higher than 255");
        Preconditions.checkArgument(y >= 0, "Y can not be lower than 0");
        Preconditions.checkArgument(x <= 15, "X can not be higher than 15");
        Preconditions.checkArgument(x >= 0, "X can not be lower than 0");
        Preconditions.checkArgument(z <= 15, "Z can not be higher than 15");
        Preconditions.checkArgument(z >= 0, "Z can not be lower than 0");

        int yInChunk = y % 16;
        int chunkY = (y - yInChunk) / 16;

        Preconditions.checkArgument(chunkY <= 15 && chunkY >= 0, "Chunk Y can not be higher than 15 and not be lower than 0");

        Chunk chunk = this.chunks[chunkY];
        /* TODO: Maybe throw an exception */
        if(chunk == null) return null;

        return chunk.getBlock((byte)x, (byte)yInChunk, (byte)z);
    }
}
