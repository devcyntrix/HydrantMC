package org.hydrantmc.api.world.chunk;

public class ChunkColumnData {

    private final int chunkX;
    private final int chunkZ;

    public byte[] data;
    public int dataSize;

    public ChunkColumnData(int chunkX, int chunkZ) {
        this.chunkX = chunkX;
        this.chunkZ = chunkZ;
    }

    public int getChunkX() {
        return chunkX;
    }

    public int getChunkZ() {
        return chunkZ;
    }
}
