package org.hydrantmc.api.world.chunk;

import com.google.common.base.Objects;

public class ChunkColumnKey {

    private int x;
    private int z;

    public ChunkColumnKey(int x, int z) {
        this.x = x;
        this.z = z;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getZ() {
        return z;
    }

    public void setZ(int z) {
        this.z = z;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ChunkColumnKey)) return false;
        ChunkColumnKey that = (ChunkColumnKey) o;
        return getX() == that.getX() &&
                getZ() == that.getZ();
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getX(), getZ());
    }

    @Override
    public String toString() {
        return "ChunkColumnKey{" +
                "x=" + x +
                ", z=" + z +
                '}';
    }
}
