package org.hydrantmc.api.world.chunk;

import org.hydrantmc.api.world.World;

import java.util.Collection;
import java.util.Random;

public abstract class ChunkGenerator {

    private ChunkProvider chunkProvider;

    public abstract ChunkColumn generateChunk(World world, int chunkX, int chunkZ, Random random);

    public abstract Collection<BlockPopulator> getBlockPopulators();

    public void setChunkProvider(ChunkProvider provider) {
        this.chunkProvider = provider;
    }

    public ChunkColumn createChunkColumn(int x, int z) {
        return chunkProvider.createChunkColumn(x, z);
    }

    public ChunkProvider getChunkProvider() {
        return chunkProvider;
    }
}
