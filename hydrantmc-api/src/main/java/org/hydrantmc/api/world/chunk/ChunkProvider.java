package org.hydrantmc.api.world.chunk;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.world.World;

import java.util.Collection;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class ChunkProvider {

    private final World world;

    private long seed;
    private Random random;

    private final ChunkGenerator chunkGenerator;
    private final ConcurrentMap<ChunkColumnKey, ChunkColumn> chunks;

    public ChunkProvider(World world, long seed, ChunkGenerator chunkGenerator) {
        this.world = world;
        setSeed(seed);
        this.chunkGenerator = chunkGenerator;
        this.chunkGenerator.setChunkProvider(this);
        this.chunks = new ConcurrentHashMap<>();
    }

    public long getSeed() {
        return this.seed;
    }

    public void setSeed(long seed) {
        this.seed = seed;
        this.random = new Random(seed);
    }

    public ChunkColumn createChunkColumn(int chunkX, int chunkZ) {
        return new ChunkColumn(this.world, chunkX, chunkZ);
    }

    public ChunkColumn generateChunk(int chunkX, int chunkZ) {
        ChunkColumn column = this.chunkGenerator.generateChunk(this.world, chunkX, chunkZ, this.random);

        Collection<BlockPopulator> list = this.chunkGenerator.getBlockPopulators();
        if(list != null) {
            for(BlockPopulator populator : list) {
                populator.populate(this.world, column, this.random);
            }
        }

        return column;
    }

    public ChunkColumn provideChunkColumn(int chunkX, int chunkZ) {
        ChunkColumn column = this.chunks.get(new ChunkColumnKey(chunkX, chunkZ));
        if(column != null) return column;
        this.chunks.putIfAbsent(new ChunkColumnKey(chunkX, chunkZ), column = generateChunk(chunkX, chunkZ));
        return column;
    }

    public Chunk provideChunk(int chunkX, int chunkY, int chunkZ) {
        Preconditions.checkArgument(chunkY <= 15, "Chunk Y can not be higher than 15");
        Preconditions.checkArgument(chunkY >= 0, "Chunk Y can not be lower than 0");
        ChunkColumn column = provideChunkColumn(chunkX, chunkZ);
        return column.getChunks()[chunkY];
    }

    public int getSize() {
        return this.chunks.size();
    }
}
