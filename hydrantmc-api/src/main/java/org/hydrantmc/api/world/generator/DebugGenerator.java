package org.hydrantmc.api.world.generator;

import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.world.World;
import org.hydrantmc.api.world.chunk.BlockPopulator;
import org.hydrantmc.api.world.chunk.Chunk;
import org.hydrantmc.api.world.chunk.ChunkColumn;
import org.hydrantmc.api.world.chunk.ChunkGenerator;

import java.util.Collection;
import java.util.HashSet;
import java.util.Random;

public class DebugGenerator extends ChunkGenerator {

    @Override
    public ChunkColumn generateChunk(World world, int chunkX, int chunkZ, Random random) {
        ChunkColumn column = createChunkColumn(0, 0);

        for(int chunkY = 0; chunkY < column.getChunks().length; chunkY++) {
            Chunk chunk = column.getChunkByY(chunkY);
            for(int x = 0; x < 16; x++) {
                for(int z = 0; z < 16; z++) {
                    for(int y = 0; y < 16; y++) {
                        chunk.getSkylightArray().set(x, y, z, 15);
                        chunk.getBlockLightArray().set(x, y, z, 15);
                    }
                }
            }
        }

        column.setBlock(0, 64, 0, new AbstractBlock((byte) 20));

        return column;
    }

    @Override
    public Collection<BlockPopulator> getBlockPopulators() {
        return new HashSet<>();
    }
}
