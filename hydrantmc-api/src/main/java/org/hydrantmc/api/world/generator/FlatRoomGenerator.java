package org.hydrantmc.api.world.generator;

import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.world.World;
import org.hydrantmc.api.world.chunk.BlockPopulator;
import org.hydrantmc.api.world.chunk.Chunk;
import org.hydrantmc.api.world.chunk.ChunkColumn;
import org.hydrantmc.api.world.chunk.ChunkGenerator;

import java.util.*;

public class FlatRoomGenerator extends ChunkGenerator {

    @Override
    public ChunkColumn generateChunk(World world, int chunkX, int chunkZ, Random random) {
        ChunkColumn column = createChunkColumn(chunkX, chunkZ);

        for(int chunkY = 0; chunkY < column.getChunks().length; chunkY++) {
            Chunk chunk = column.getChunkByY(chunkY);
            for(int x = 0; x < 16; x++) {
                for(int z = 0; z < 16; z++) {
                    for(int y = 0; y < 16; y++) {
                        chunk.getSkylightArray().set(x, y, z, 0xF);
                        chunk.getBlockLightArray().set(x, y, z, 0xF);
                    }
                }
            }
        }

        for(int x = 0; x < 16; x++) {
            for(int z = 0; z < 16; z++) {
                for(int y = 0; y < 4; y++) {
                    if(y == 0) {
                        column.setBlock(x, y, z, new AbstractBlock((byte)7, (byte)0));
                        continue;
                    }

                    if(y == 3) {
                        column.setBlock(x, y, z, new AbstractBlock((byte)2, (byte)0));
                        continue;
                    }

                    column.setBlock(x, y, z, new AbstractBlock((byte)1, (byte)0));
                }
            }
        }

        return column;
    }

    @Override
    public Collection<BlockPopulator> getBlockPopulators() {
        return new HashSet<>();
    }
}
