package org.hydrantmc.api;

import org.hydrantmc.api.block.BlockRegistry;
import org.hydrantmc.api.command.CommandRegistry;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.listener.ListenerRegistry;
import org.hydrantmc.api.plugin.PluginManager;
import org.hydrantmc.api.scoreboard.ScoreboardManager;
import org.hydrantmc.api.tablist.AbstractTabList;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.status.ServerStatus;
import org.hydrantmc.api.world.World;

import java.io.File;
import java.security.KeyPair;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class TestingServer implements Server {

    public static final TestingServer INSTANCE = new TestingServer();

    @Override
    public String getVersion() {
        return null;
    }

    @Override
    public int getProtocolVersion() {
        return 0;
    }

    @Override
    public Logger getLogger() {
        return null;
    }

    @Override
    public void shutdown(int exit) {

    }

    @Override
    public File getPluginsFolder() {
        return null;
    }

    @Override
    public KeyPair getKeyPair() {
        return null;
    }

    @Override
    public boolean isOnlineMode() {
        return false;
    }

    @Override
    public ServerConfiguration getConfiguration() {
        return TestingServerConfiguration.INSTANCE;
    }

    @Override
    public NetworkProperties getNetworkProperties() {
        return TestingServerConfiguration.INSTANCE.getNetworkProperties();
    }

    @Override
    public int getMaxPlayers() {
        return 0;
    }

    @Override
    public CommandSender getConsoleCommandSender() {
        return null;
    }

    @Override
    public ServerStatus getServerStatus() {
        return null;
    }

    @Override
    public World getWorld(String name) {
        return null;
    }

    @Override
    public Map<String, World> getWorlds() {
        return null;
    }

    @Override
    public Location getSpawnLocation() {
        return null;
    }

    @Override
    public void setSpawnLocation(Location location) {

    }

    @Override
    public void addPlayer(Player player) {

    }

    @Override
    public Player getPlayer(String username) {
        return null;
    }

    @Override
    public Player getPlayer(UUID uniqueId) {
        return null;
    }

    @Override
    public Collection<Player> getOnlinePlayers() {
        return null;
    }

    @Override
    public CommandRegistry getCommandRegistry() {
        return null;
    }

    @Override
    public ListenerRegistry getListenerRegistry() {
        return null;
    }

    @Override
    public PluginManager getPluginManager() {
        return null;
    }

    @Override
    public BlockRegistry getBlockRegistry() {
        return null;
    }

    @Override
    public AbstractTabList getTabList() {
        return null;
    }

    @Override
    public void broadcast(Predicate<Player> predicate, Consumer<Player> consumer) {

    }

    @Override
    public void broadcast(Consumer<Player> consumer) {

    }

    @Override
    public void reload() {

    }

    @Override
    public void shutdown() {

    }

    @Override
    public ScoreboardManager getScoreboardManager() {
        return null;
    }
}
