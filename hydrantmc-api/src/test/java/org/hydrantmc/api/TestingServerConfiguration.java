package org.hydrantmc.api;

import org.hydrantmc.api.utils.player.GameMode;
import org.hydrantmc.api.world.Difficulty;

public class TestingServerConfiguration implements ServerConfiguration {

    public static final TestingServerConfiguration INSTANCE = new TestingServerConfiguration();


    @Override
    public ServerInfoProperties getServerInfoProperties() {
        return null;
    }

    @Override
    public NetworkProperties getNetworkProperties() {
        return new NetworkProperties();
    }

    @Override
    public GameMode getDefaultGameMode() {
        return GameMode.CREATIVE;
    }

    @Override
    public KickMessages getKickMessages() {
        return null;
    }

    @Override
    public Messages getMessages() {
        return null;
    }

    @Override
    public TabListProperties getTabListProperties() {
        return null;
    }

}
