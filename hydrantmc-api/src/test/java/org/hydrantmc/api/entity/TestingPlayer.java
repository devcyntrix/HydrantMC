package org.hydrantmc.api.entity;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.TestingServer;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.ConnectionState;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.utils.player.GameProfile;

import java.net.InetSocketAddress;
import java.security.Key;

public class TestingPlayer extends Player {

    public static final TestingPlayer INSTANCE = new TestingPlayer();

    private TestingPlayer() {
        super(null, TestingPendingConnection.INSTANCE);
    }

    private static class TestingPendingConnection implements PendingConnection {

        static final TestingPendingConnection INSTANCE = new TestingPendingConnection();

        @Override
        public AbstractPacketHandler getHandler() {
            return null;
        }

        @Override
        public String getLoginName() {
            return null;
        }

        @Override
        public void disconnect(String reason) {

        }

        @Override
        public void disconnect(ChatComponent reason) {

        }

        @Override
        public void enableCompressionThreshold(int threshold) {

        }

        @Override
        public void enableEncryption(Key key) {

        }

        @Override
        public void disableEncryption() {

        }

        @Override
        public GameProfile getGameProfile() {
            return null;
        }

        @Override
        public void setGameProfile(GameProfile profile) {

        }

        @Override
        public int getProtocolVersion() {
            return 0;
        }

        @Override
        public void setProtocolVersion(int version) {

        }

        @Override
        public Server getServer() {
            return TestingServer.INSTANCE;
        }

        @Override
        public Channel getChannel() {
            return null;
        }

        @Override
        public InetSocketAddress getAddress() {
            return null;
        }

        @Override
        public ConnectionState getConnectionState() {
            return null;
        }

        @Override
        public void setConnectionState(ConnectionState state) {

        }

        @Override
        public ChannelFuture sendPacket(OutgoingPacket packet) {
            return null;
        }

        @Override
        public ChannelFuture sendPacketAndFlush(OutgoingPacket packet) {
            return null;
        }

        @Override
        public void flush() {

        }

        @Override
        public void disconnect() {

        }
    }

}
