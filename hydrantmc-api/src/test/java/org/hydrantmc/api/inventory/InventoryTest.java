package org.hydrantmc.api.inventory;

import org.hydrantmc.api.entity.TestingPlayer;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class InventoryTest {

    @Test
    public void testInventory() {
        Inventory inventory = new Inventory(null, 18);

        assertThat(inventory.getHolder(), is(nullValue()));

        assertThat(inventory.getItem(0), is(nullValue()));
        assertThat(inventory.getContents(), is(notNullValue()));
    }

    @Test
    public void testPlayerInventory() {
        PlayerInventory inventory = new PlayerInventory(TestingPlayer.INSTANCE);

        assertThat(inventory.getHolder(), is(TestingPlayer.INSTANCE));

        assertThat(inventory.getItem(0), is(nullValue()));
        assertThat(inventory.getContents(), is(notNullValue()));

        assertThat(inventory.getHelmet(), is(nullValue()));
        assertThat(inventory.getChestplate(), is(nullValue()));
        assertThat(inventory.getLeggings(), is(nullValue()));
        assertThat(inventory.getBoots(), is(nullValue()));
        assertThat(inventory.getArmorContents(), is(notNullValue()));

        assertThat(inventory.getItemInHand(), is(sameInstance(inventory.getItem(inventory.getHeldItemSlot()))));
    }

}
