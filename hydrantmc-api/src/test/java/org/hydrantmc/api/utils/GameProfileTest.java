package org.hydrantmc.api.utils;

import org.hydrantmc.api.utils.player.GameProfile;
import org.hydrantmc.api.utils.player.Property;
import org.junit.Test;

import java.util.UUID;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertThat;

public class GameProfileTest {

    @Test
    public void testProperty() {
        String name = "test";
        String value = "value";

        Property property = new Property(name, value);

        assertThat(property.getName(), is(name));
        assertThat(property.getValue(), is(value));
        assertThat(property.getSignature(), is(nullValue()));
        assertThat(property.hasSignature(), is(false));
        assertThat(property.isSignatureValid(null), is(false));
    }

    @Test
    public void testGameProfile() {
        String userName = "testUser";
        UUID uuid = UUID.nameUUIDFromBytes(userName.getBytes());

        String userName2 = "testUser2";
        UUID uuid2 = UUID.nameUUIDFromBytes(userName2.getBytes());

        GameProfile gameProfile = new GameProfile(uuid, userName);

        assertThat(gameProfile.getUniqueId(), is(uuid));
        assertThat(gameProfile.getUsername(), is(userName));
        assertThat(gameProfile.getPropertyMap().isEmpty(), is(true));
        assertThat(gameProfile.isLegacy(), is(false));
        assertThat(gameProfile.isComplete(), is(true));

        assertThat(gameProfile.equals(new GameProfile(uuid2, userName2)), is(false));
    }

}
