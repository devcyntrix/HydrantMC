package org.hydrantmc.api.utils;

import org.junit.Test;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;

public class LocationTest {

    @Test
    public void testLocation() {
        Location location = new Location(null, 10, 50, 33, 62, 69);

        assertThat(location.getWorld(), is(nullValue()));
        assertThat(location.getX(), is(10.0));
        assertThat(location.getY(), is(50.0));
        assertThat(location.getZ(), is(33.0));
        assertThat(location.getYaw(), is(62.0F));
        assertThat(location.getPitch(), is(69.0F));

        assertThat(location.getBlockX(), is(10));
        assertThat(location.getBlockY(), is(50));
        assertThat(location.getBlockZ(), is(33));

        assertThat(location.add(0.2, 0, 0).getBlockX(), is(10));
    }

}
