package org.hydrantmc.api.utils;

import org.hydrantmc.api.utils.player.PlayerAbilities;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PlayerAbilitiesTest {

    @Test
    public void testPlayerAbilities() {
        PlayerAbilities abilities = new PlayerAbilities(false, false, false, false, 0.05F, 0.1F);

        assertThat(abilities.isInvulnerable(), is(false));
        assertThat(abilities.isFlying(), is(false));
        assertThat(abilities.isAllowFlying(), is(false));
        assertThat(abilities.isCreativeMode(), is(false));
        assertThat(abilities.getFlySpeed(), is(0.05F));
        assertThat(abilities.getWalkSpeed(), is(0.1F));
    }

}
