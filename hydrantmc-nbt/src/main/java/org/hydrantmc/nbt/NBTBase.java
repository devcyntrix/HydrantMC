package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public abstract class NBTBase<T> {

    protected T value;

    public NBTBase() { }

    public NBTBase(T value) {
        this.value = value;
    }

    public abstract int getId();

    public abstract Class<?> getValueClass();

    public abstract void write(DataOutput output) throws IOException;

    public abstract void read(DataInput input) throws IOException;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public static NBTBase createNewByType(int tagType) {

        switch (tagType) {

            case 0:
                return new NBTTagEnd();

            case 1:
                return new NBTTagByte();

            case 2:
                return new NBTTagShort();

            case 3:
                return new NBTTagInteger();

            case 4:
                return new NBTTagLong();

            case 5:
                return new NBTTagFloat();

            case 6:
                return new NBTTagDouble();

            case 7:
                return new NBTTagByteArray();

            case 8:
                return new NBTTagString();

            case 9:
                return new NBTTagList();

            case 10:
                return new NBTTagCompound();

            case 11:
                return new NBTTagIntegerArray();

            default:
                return null;
        }

    }
}
