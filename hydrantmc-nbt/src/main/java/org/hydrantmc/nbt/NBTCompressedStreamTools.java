package org.hydrantmc.nbt;

import com.google.common.base.Preconditions;

import java.io.*;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;

public class NBTCompressedStreamTools {

    public static NBTBase readCompressedStream(InputStream stream) throws IOException {
        DataInputStream inputStream = new DataInputStream(new BufferedInputStream(new GZIPInputStream(stream)));

        int tagId = inputStream.readByte();
        NBTBase base = NBTBase.createNewByType(tagId);

        if(base == null) return null;

        try {
            base.read(inputStream);
        } finally {
            inputStream.close();
        }

        return base;
    }

    public static void writeCompressedStream(NBTBase base, OutputStream stream) throws IOException {
        Preconditions.checkNotNull(base, "base cannot be null");
        Preconditions.checkNotNull(stream, "stream cannot be null");

        DataOutputStream outputStream = new DataOutputStream(new BufferedOutputStream(new GZIPOutputStream(stream)));

        outputStream.writeByte(base.getId());

        try {
            base.write(outputStream);
        } finally {
            stream.close();
        }
    }

}
