package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagByte extends NBTTagNumber {

    public NBTTagByte() { }

    public NBTTagByte(byte value) {
        super(value);
    }

    @Override
    public int getId() {
        return 1;
    }

    @Override
    public Class<?> getValueClass() {
        return byte.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeByte(getValueAsByte());
    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = input.readByte();
    }
}
