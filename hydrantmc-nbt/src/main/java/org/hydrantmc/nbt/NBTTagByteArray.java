package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagByteArray extends NBTBase<byte[]> {

    public NBTTagByteArray() {
    }

    public NBTTagByteArray(byte[] value) {
        super(value);
    }

    @Override
    public int getId() {
        return 7;
    }

    @Override
    public Class<?> getValueClass() {
        return byte[].class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeInt(super.value.length);
        output.write(super.value);
    }

    @Override
    public void read(DataInput input) throws IOException {
        int size = input.readInt();
        super.value = new byte[size];
        input.readFully(super.value);
    }
}
