package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.*;

public class NBTTagCompound extends NBTBase<Map<String, NBTBase>> {

    public NBTTagCompound() {
        super(new HashMap<>());
    }

    public NBTTagCompound(Map<String, NBTBase> value) {
        super(value);
    }

    @Override
    public int getId() {
        return 10;
    }

    @Override
    public Class<?> getValueClass() {
        return Map.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {

        for(Map.Entry<String, NBTBase> entry : super.value.entrySet()) {
            NBTBase base = entry.getValue();

            output.writeByte(base.getId());

            if(base.getId() != 0) {
                output.writeUTF(entry.getKey());
                base.write(output);
            }
        }

    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = new HashMap<>();

        byte type;

        while((type = input.readByte()) != 0) {
            String key = input.readUTF();
            NBTBase base = NBTBase.createNewByType(type);
            Objects.requireNonNull(base).read(input);
            super.value.put(key, base);
        }
    }

    public int size() {
        return value.size();
    }

    public boolean isEmpty() {
        return value.isEmpty();
    }

    public boolean containsKey(Object key) {
        return value.containsKey(key);
    }

    public boolean containsValue(Object value) {
        return this.value.containsValue(value);
    }

    public NBTBase get(Object key) {
        return value.get(key);
    }

    public NBTBase put(String key, NBTBase value) {
        return this.value.put(key, value);
    }

    public NBTBase remove(Object key) {
        return value.remove(key);
    }

    public void clear() {
        value.clear();
    }

    public Set<String> keySet() {
        return value.keySet();
    }

    public Collection<NBTBase> values() {
        return value.values();
    }

    public Set<Map.Entry<String, NBTBase>> entrySet() {
        return value.entrySet();
    }

    public NBTBase putIfAbsent(String key, NBTBase value) {
        return this.value.putIfAbsent(key, value);
    }


}
