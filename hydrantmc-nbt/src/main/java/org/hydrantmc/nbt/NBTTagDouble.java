package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagDouble extends NBTTagNumber<Double> {

    public NBTTagDouble() { }

    public NBTTagDouble(double value) {
        super(value);
    }

    @Override
    public int getId() {
        return 6;
    }

    @Override
    public Class<?> getValueClass() {
        return double.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeDouble(getValueAsDouble());
    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = input.readDouble();
    }
}
