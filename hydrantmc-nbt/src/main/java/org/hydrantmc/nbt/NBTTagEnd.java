package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;

public class NBTTagEnd extends NBTBase {

    public NBTTagEnd() {
        super();
    }

    @Override
    public int getId() {
        return 0;
    }

    @Override
    public Class<?> getValueClass() {
        return Void.class;
    }

    @Override
    public void write(DataOutput output) {}

    @Override
    public void read(DataInput input) {}
}
