package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagFloat extends NBTTagNumber<Float> {

    public NBTTagFloat() { }

    public NBTTagFloat(float value) {
        super(value);
    }

    @Override
    public int getId() {
        return 5;
    }

    @Override
    public Class<?> getValueClass() {
        return float.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeFloat(getValueAsFloat());
    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = input.readFloat();
    }
}
