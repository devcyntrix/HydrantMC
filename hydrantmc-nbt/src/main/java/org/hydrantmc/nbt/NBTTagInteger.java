package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagInteger extends NBTTagNumber<Integer> {

    public NBTTagInteger() { }

    public NBTTagInteger(int value) {
        super(value);
    }

    @Override
    public int getId() {
        return 3;
    }

    @Override
    public Class<?> getValueClass() {
        return int.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeInt(getValueAsInt());
    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = input.readInt();
    }
}
