package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagIntegerArray extends NBTBase<int[]> {

    @Override
    public int getId() {
        return 11;
    }

    @Override
    public Class<?> getValueClass() {
        return int[].class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeInt(super.value.length);
        for (int aValue : super.value) {
            output.writeInt(aValue);
        }
    }

    @Override
    public void read(DataInput input) throws IOException {
        int size = input.readInt();
        super.value = new int[size];
        for(int index = 0; index < size; index++) {
            super.value[index] = input.readInt();
        }
    }
}
