package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class NBTTagList<T extends NBTBase> extends NBTBase<List<T>> {

    private int tagType;

    public NBTTagList() {
        super(new ArrayList<>());
    }

    public NBTTagList(List<T> value) {
        super(value);
    }

    @Override
    public int getId() {
        return 9;
    }

    @Override
    public Class<?> getValueClass() {
        return List.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        List<T> value = getValue();

        if (!value.isEmpty()) {
            this.tagType = value.get(0).getId();
        } else {
            this.tagType = 0;
        }

        output.writeByte(this.tagType);
        output.writeInt(value.size());

        for (T aValue : value) {
            aValue.write(output);
        }
    }

    @Override
    public void read(DataInput input) throws IOException {
        this.tagType = input.readByte();
        int size = input.readInt();

        if (this.tagType == 0 && size > 0) {
            throw new RuntimeException("Missing type on ListTag");
        } else {
            super.value = new ArrayList<>();

            for (int j = 0; j < size; ++j) {
                NBTBase nbtbase = NBTBase.createNewByType(this.tagType);
                Objects.requireNonNull(nbtbase).read(input);
                super.value.add((T) nbtbase);
            }
        }
    }

    public int size() {
        return value.size();
    }

    public boolean isEmpty() {
        return value.isEmpty();
    }

    public Object[] toArray() {
        return value.toArray();
    }

    public <T1> T1[] toArray(T1[] a) {
        return value.toArray(a);
    }

    public boolean add(T t) {
        return value.add(t);
    }

    public boolean remove(Object o) {
        return value.remove(o);
    }

    public boolean containsAll(Collection<?> c) {
        return value.containsAll(c);
    }

    public boolean addAll(Collection<? extends T> c) {
        return value.addAll(c);
    }

    public boolean addAll(int index, Collection<? extends T> c) {
        return value.addAll(index, c);
    }

    public boolean removeAll(Collection<?> c) {
        return value.removeAll(c);
    }

    public void clear() {
        value.clear();
    }

    public T get(int index) {
        return value.get(index);
    }

    public T set(int index, T element) {
        return value.set(index, element);
    }

    public void add(int index, T element) {
        value.add(index, element);
    }

    public T remove(int index) {
        return value.remove(index);
    }

    public int indexOf(Object o) {
        return value.indexOf(o);
    }

    public int lastIndexOf(Object o) {
        return value.lastIndexOf(o);
    }

    public boolean removeIf(Predicate<? super T> filter) {
        return value.removeIf(filter);
    }

    public Stream<T> stream() {
        return value.stream();
    }

    public void forEach(Consumer<? super T> action) {
        value.forEach(action);
    }
}
