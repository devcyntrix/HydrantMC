package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagLong extends NBTTagNumber<Long> {

    public NBTTagLong() { }

    public NBTTagLong(long value) {
        super(value);
    }

    @Override
    public int getId() {
        return 4;
    }

    @Override
    public Class<?> getValueClass() {
        return long.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeLong(getValueAsLong());
    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = input.readLong();
    }
}
