package org.hydrantmc.nbt;

public abstract class NBTTagNumber<T extends Number> extends NBTBase<T> {

    public NBTTagNumber() {
        super(null);
    }

    public NBTTagNumber(T value) {
        super(value);
    }

    public byte getValueAsByte() {
        return getValue().byteValue();
    }

    public short getValueAsShort() {
        return getValue().shortValue();
    }

    public int getValueAsInt() {
        return getValue().intValue();
    }

    public long getValueAsLong() {
        return getValue().longValue();
    }

    public float getValueAsFloat() {
        return getValue().floatValue();
    }

    public double getValueAsDouble() {
        return getValue().doubleValue();
    }

}
