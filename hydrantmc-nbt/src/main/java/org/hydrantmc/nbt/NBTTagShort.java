package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagShort extends NBTTagNumber<Short> {

    public NBTTagShort() { }

    public NBTTagShort(short value) {
        super(value);
    }

    @Override
    public int getId() {
        return 2;
    }

    @Override
    public Class<?> getValueClass() {
        return short.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeShort(getValueAsShort());
    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = input.readShort();
    }
}
