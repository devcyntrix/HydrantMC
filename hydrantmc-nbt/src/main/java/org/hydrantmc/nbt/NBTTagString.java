package org.hydrantmc.nbt;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

public class NBTTagString extends NBTBase<String> {

    public NBTTagString() {
    }

    public NBTTagString(String value) {
        super(value);
    }

    @Override
    public int getId() {
        return 8;
    }

    @Override
    public Class<?> getValueClass() {
        return String.class;
    }

    @Override
    public void write(DataOutput output) throws IOException {
        output.writeUTF(super.value);
    }

    @Override
    public void read(DataInput input) throws IOException {
        super.value = input.readUTF();
    }
}
