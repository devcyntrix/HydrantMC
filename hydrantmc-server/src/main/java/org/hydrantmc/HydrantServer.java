package org.hydrantmc;

import com.google.common.util.concurrent.ThreadFactoryBuilder;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.Channel;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.epoll.Epoll;
import io.netty.channel.epoll.EpollEventLoopGroup;
import io.netty.channel.epoll.EpollServerSocketChannel;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import org.hydrantmc.api.NetworkProperties;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.block.BlockRegistry;
import org.hydrantmc.api.block.ChestBlock;
import org.hydrantmc.api.block.GrassBlock;
import org.hydrantmc.api.command.CommandRegistry;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.listener.ListenerRegistry;
import org.hydrantmc.api.network.ConnectionState;
import org.hydrantmc.api.plugin.PluginManager;
import org.hydrantmc.api.scoreboard.ScoreboardManager;
import org.hydrantmc.api.tablist.AbstractTabList;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.status.ServerStatus;
import org.hydrantmc.api.world.Difficulty;
import org.hydrantmc.api.world.TerrainType;
import org.hydrantmc.api.world.World;
import org.hydrantmc.api.world.WorldType;
import org.hydrantmc.command.HydrantConsoleCommandSender;
import org.hydrantmc.command.commands.*;
import org.hydrantmc.logging.HydrantLogger;
import org.hydrantmc.logging.LoggingFormatter;
import org.hydrantmc.logging.handler.ConsoleHandler;
import org.hydrantmc.network.HydrantChildInitializer;
import org.hydrantmc.network.handler.HydrantHandshakePacketHandler;
import org.hydrantmc.network.handler.HydrantLoginPacketHandler;
import org.hydrantmc.network.handler.HydrantPlayPacketHandler;
import org.hydrantmc.network.handler.HydrantStatusPacketHandler;
import org.hydrantmc.scoreboard.HydrantScoreboardManager;
import org.hydrantmc.tablist.HydrantTabList;
import org.hydrantmc.world.HydrantWorld;

import java.io.File;
import java.net.InetSocketAddress;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.logging.Logger;

public class HydrantServer implements Server {

    private static HydrantServer server;

    private final HydrantServerConfiguration configuration;
    private final HydrantLogger logger = new HydrantLogger();

    private final CommandSender consoleCommandSender = new HydrantConsoleCommandSender(this);

    private ExecutorService executorService;
    private Timer timer;

    private final Map<String, World> worlds = new HashMap<>();
    private Location spawnLocation;

    private final BlockRegistry blockRegistry = new BlockRegistry();
    private final PluginManager pluginManager = new PluginManager(this);

    /* Netty IO */
    private KeyPair generatedKeyPair;
    private Channel serverChannel;

    private final AbstractTabList tabList = new HydrantTabList(this);
    private final Map<UUID, Player> onlinePlayers = new ConcurrentHashMap<>();

    private ServerStatus serverStatus;
    private final File pluginsFolder = new File("plugins");

    private final ScoreboardManager scoreboardManager = new HydrantScoreboardManager();

    public HydrantServer(HydrantServerConfiguration configuration) {
        server = this;
        this.configuration = configuration;
        this.serverStatus = new ServerStatus(onlinePlayers.size(), getConfiguration().getServerInfoProperties());
    }

    public static HydrantServer getServer() {
        return server;
    }


    protected void start() {
        getLogger().info("Starting up HydrantServer...");
        /* Logger setup */
        ConsoleHandler consoleHandler = new ConsoleHandler(System.out, System.err);
        consoleHandler.setFormatter(new LoggingFormatter());

        this.logger.addHandler(consoleHandler);

        if(!configuration.getEula().isAccepted()) {
            getLogger().info("You need to accept the EULA to start the Server.");
            getLogger().info("By changing the setting \"eula.accepted\" in the hydrant.json to TRUE you are indicating your agreement to the Minecraft EULA (https://account.mojang.com/documents/minecraft_eula).");
            shutdown();
            return;
        }

        if (getNetworkProperties().getCipherProperties().isEnabled()) {
            getLogger().info("Generating key pair...");
            this.generatedKeyPair = generateKeyPair();
        }

        this.executorService = Executors.newCachedThreadPool();
        this.timer = new Timer("World updater", true);

        Random random = new Random();

        /* TODO: variable */
        this.worlds.put("world", new HydrantWorld(this, "world", Difficulty.PEACEFUL, WorldType.OVERWORLD, random.nextLong(), TerrainType.FLAT));
        this.spawnLocation = new Location(this.worlds.get("world"), 1, 6, 1, 90F, 0F);

        /* Handshake */
        ConnectionState.HANDSHAKE.setPacketHandler(HydrantHandshakePacketHandler.class);

        /* Status */
        ConnectionState.STATUS.setPacketHandler(HydrantStatusPacketHandler.class);

        /* Login */
        ConnectionState.LOGIN.setPacketHandler(HydrantLoginPacketHandler.class);

        /* Play */
        ConnectionState.PLAY.setPacketHandler(HydrantPlayPacketHandler.class);

        registerBlocks();
        registerCommands();

        getLogger().info("Loading plugins...");

        if(!pluginsFolder.exists() || !pluginsFolder.isDirectory()) {
            if(!pluginsFolder.mkdir()) {
                getLogger().severe("Failed to create plugins folder");
            }
        }

        pluginManager.detectPlugins(pluginsFolder);

        pluginManager.loadPlugins();

        pluginManager.enablePlugins();

        getLogger().info("Loaded plugins successfully.");

        int maxAcceptorThreads = getNetworkProperties().getMaxAcceptorThreads();
        ThreadFactory acceptorThreadFactory = new ThreadFactoryBuilder().setNameFormat("Network Acceptor Thread #%d").build();

        int maxWorkerThreads = getNetworkProperties().getMaxWorkerThreads();
        ThreadFactory workerThreadFactory = new ThreadFactoryBuilder().setNameFormat("Network Worker Thread #%d").build();

        InetSocketAddress address = getNetworkProperties().getAddress();


        /* Server setup */
        EventLoopGroup acceptorThreadGroup = useEpoll() ? new EpollEventLoopGroup(maxAcceptorThreads, acceptorThreadFactory)
                : new NioEventLoopGroup(maxAcceptorThreads, acceptorThreadFactory);
        EventLoopGroup workerThreadGroup = useEpoll() ? new EpollEventLoopGroup(maxWorkerThreads, workerThreadFactory)
                : new NioEventLoopGroup(maxWorkerThreads, workerThreadFactory);

        ServerBootstrap bootstrap = new ServerBootstrap();
        bootstrap.channel((useEpoll() ? EpollServerSocketChannel.class : NioServerSocketChannel.class));
        bootstrap.group(acceptorThreadGroup, workerThreadGroup);
        bootstrap.childOption(ChannelOption.TCP_NODELAY, true);
        bootstrap.childHandler(new HydrantChildInitializer(this));

        serverChannel = bootstrap.bind(address).syncUninterruptibly().channel();

        getLogger().info("Started up HydrantServer successfully.");

    }

    private void registerBlocks() {
        blockRegistry.registerBlock(0, null);
        blockRegistry.registerBlock(2, new GrassBlock());
        blockRegistry.registerBlock(54, new ChestBlock());
    }

    private void registerCommands() {
        CommandRegistry commandRegistry = getPluginManager().getCommandRegistry();
        commandRegistry.registerCommand(new StopCommand());
        commandRegistry.registerCommand(new SayCommand());
        commandRegistry.registerCommand(new TeleportCommand());
        commandRegistry.registerCommand(new KickCommand());
        commandRegistry.registerCommand(new UnloadChunkCommand());
        commandRegistry.registerCommand(new OpCommand());
        commandRegistry.registerCommand(new DeopCommand());
        commandRegistry.registerCommand(new GameModeCommand());
        commandRegistry.registerCommand(new DifficultyCommand());
        commandRegistry.registerCommand(new ReloadCommand());
        commandRegistry.registerCommand(new PluginsCommand());
        commandRegistry.registerCommand(new ListCommand());
        commandRegistry.registerCommand(new PlaySoundCommand());
        commandRegistry.registerCommand(new ClearCommand());
        commandRegistry.registerCommand(new KillCommand());
        commandRegistry.registerCommand(new HealCommand());
        commandRegistry.registerCommand(new FeedCommand());
    }

    protected void stop() {
        getLogger().info("Shutting down HydrantServer...");

        if(serverChannel != null) {
            serverChannel.closeFuture().channel().close().syncUninterruptibly();
        }

        pluginManager.disablePlugins();

        getLogger().info("HydrantServer shut down successfully.");
    }

    private KeyPair generateKeyPair() {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(getNetworkProperties().getCipherProperties().getKeySize());
            return generator.generateKeyPair();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    private boolean useEpoll() {
        return Epoll.isAvailable() && getConfiguration().getNetworkProperties().isAllowNativeTransport();
    }

    @Override
    public HydrantServerConfiguration getConfiguration() {
        return configuration;
    }

    @Override
    public NetworkProperties getNetworkProperties() {
        return this.configuration.getNetworkProperties();
    }

    @Override
    public int getMaxPlayers() {
        return getConfiguration().getServerInfoProperties().getMaxPlayers();
    }

    @Override
    public String getVersion() {
        return "Hydrant 1.8.9";
    }

    @Override
    public int getProtocolVersion() {
        return 47;
    }

    @Override
    public boolean isOnlineMode() {
        return this.getNetworkProperties().getCipherProperties().isEnabled();
    }

    @Override
    public Logger getLogger() {
        return logger;
    }

    @Override
    public CommandSender getConsoleCommandSender() {
        return consoleCommandSender;
    }

    @Override
    public ServerStatus getServerStatus() {
        return serverStatus;
    }

    @Override
    public KeyPair getKeyPair() {
        return generatedKeyPair;
    }

    @Override
    public World getWorld(String name) {
        return this.worlds.get(name.toLowerCase());
    }

    @Override
    public Map<String, World> getWorlds() {
        return this.worlds;
    }

    @Override
    public Location getSpawnLocation() {
        return spawnLocation;
    }

    @Override
    public void setSpawnLocation(Location spawnLocation) {
        this.spawnLocation = spawnLocation;
    }

    @Override
    public CommandRegistry getCommandRegistry() {
        return getPluginManager().getCommandRegistry();
    }

    @Override
    public BlockRegistry getBlockRegistry() {
        return blockRegistry;
    }

    @Override
    public ListenerRegistry getListenerRegistry() {
        return getPluginManager().getListenerRegistry();
    }

    @Override
    public PluginManager getPluginManager() {
        return pluginManager;
    }

    @Override
    public Player getPlayer(String username) {
        for (Player player : onlinePlayers.values()) {
            if(player.getName().toLowerCase().startsWith(username.toLowerCase())) {
                return player;
            }
        }
        return null;
    }

    @Override
    public Player getPlayer(UUID uniqueId) {
        return this.onlinePlayers.get(uniqueId);
    }

    @Override
    public Collection<Player> getOnlinePlayers() {
        return this.onlinePlayers.values();
    }

    public void addPlayer(Player player) {
        this.onlinePlayers.put(player.getUniqueId(), player);
    }

    @Override
    public AbstractTabList getTabList() {
        return tabList;
    }

    @Override
    public void broadcast(Predicate<Player> predicate, Consumer<Player> consumer) {
        for(Player player : getOnlinePlayers()) {
            if(!predicate.test(player)) continue;
            consumer.accept(player);
        }
    }

    @Override
    public void broadcast(Consumer<Player> consumer) {
        for(Player player : getOnlinePlayers()) {
            consumer.accept(player);
        }
    }

    @Override
    public void reload() {
        this.configuration.loadFromInfo();
        this.serverStatus = new ServerStatus(onlinePlayers.size(), getConfiguration().getServerInfoProperties());
    }

    @Override
    public void shutdown() {
        System.exit(0);
    }

    @Override
    public void shutdown(int exit) {
        System.exit(exit);
    }

    @Override
    public File getPluginsFolder() {
        return pluginsFolder;
    }

    @Override
    public ScoreboardManager getScoreboardManager() {
        return scoreboardManager;
    }

}
