package org.hydrantmc;

import de.devcyntrix.configuration.annotation.AbstractClassConfiguration;
import de.devcyntrix.configuration.annotation.ConfigurationInfo;
import de.devcyntrix.configuration.annotation.ConfigurationProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hydrantmc.api.*;
import org.hydrantmc.api.utils.Eula;
import org.hydrantmc.api.utils.player.GameMode;

@EqualsAndHashCode(callSuper = true)
@Data
@ConfigurationInfo(filename = "hydrant.json")
public class HydrantServerConfiguration extends AbstractClassConfiguration implements ServerConfiguration {

    @ConfigurationProperty(name = "eula")
    private Eula eula = new Eula();

    @ConfigurationProperty(name = "serverInfo")
    private ServerInfoProperties serverInfoProperties = new ServerInfoProperties();

    @ConfigurationProperty(name = "network")
    private NetworkProperties networkProperties = new NetworkProperties();

    @ConfigurationProperty(name = "defaultGameMode")
    private GameMode defaultGameMode = GameMode.CREATIVE;

    @ConfigurationProperty(name = "tabList")
    private TabListProperties tabListProperties = new TabListProperties();

    @ConfigurationProperty(name = "kicks")
    private KickMessages kickMessages = new KickMessages();

    @ConfigurationProperty(name = "messages")
    private Messages messages = new Messages();


}
