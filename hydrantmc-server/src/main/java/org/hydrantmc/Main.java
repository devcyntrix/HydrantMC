package org.hydrantmc;

import jline.console.ConsoleReader;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import org.hydrantmc.api.NetworkProperties;
import org.hydrantmc.api.command.CommandRegistry;

public class Main {

    public static void main(String[] args) {

        HydrantServerConfiguration configuration = new HydrantServerConfiguration();
        configuration.loadFromInfo();


        NetworkProperties properties = configuration.getNetworkProperties();

        /* TODO: Add JOpt Simple Code */
        OptionParser parser = new OptionParser();
        OptionSet options = parser.parse(args);


        HydrantServer server = new HydrantServer(configuration);
        try {
            server.start();
        } catch (Exception exception) {
            System.err.println("Failed to start the hydrant server because of an exception: ");
            exception.printStackTrace();
        } finally {
            Runtime.getRuntime().addShutdownHook(new Thread(server::stop));
        }

        try {
            ConsoleReader reader = new ConsoleReader();
            reader.setPrompt("> ");

            String commandLine;
            while((commandLine = reader.readLine()) != null) {
                CommandRegistry commandRegistry = server.getCommandRegistry();
                boolean success = commandRegistry.executeCommand(server.getConsoleCommandSender(), commandLine);
                if(!success) {
                    server.getConsoleCommandSender().sendMessage(server.getConfiguration().getMessages().getUnknownCommandMessage());
                }
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

}
