package org.hydrantmc.command;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.CommandSender;

import java.util.UUID;
import java.util.logging.Level;

public class HydrantConsoleCommandSender implements CommandSender {

    private final String name;
    private final UUID uuid;

    private final Server server;

    public HydrantConsoleCommandSender(Server server) {
        this.name = "Console";
        this.uuid = UUID.nameUUIDFromBytes(name.getBytes());

        this.server = server;
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void sendMessage(String message) {
        server.getLogger().log(Level.INFO, message);
    }

    @Override
    public boolean hasPermission(String permission) {
        return true;
    }

    @Override
    public UUID getUniqueId() {
        return uuid;
    }

    @Override
    public boolean hasOp() {
        return true;
    }

    @Override
    public void setOp(boolean op) {
        throw new UnsupportedOperationException();
    }

}
