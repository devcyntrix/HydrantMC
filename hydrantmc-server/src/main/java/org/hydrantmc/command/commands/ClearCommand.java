package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;

import java.util.List;

public class ClearCommand extends AbstractCommand {

    public ClearCommand() {
        super("clear");
        super.description = "Clears a player's inventory";
        super.permission = "hydrant.command.clear";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length != 1) {
            sender.sendMessage("§c/clear <Player>");
            return;
        }

        Server server = sender.getServer();
        Player target = server.getPlayer(args[0]);

        if(target == null) {
            sender.sendMessage("§cThe player is not online.");
            return;
        }

        target.getInventory().clear();

        server.broadcast(object -> object.hasPermission(getPermission()), player -> player.sendMessage("§5" + sender.getName() + " §7» §ccleared inventory of player §3" + target.getName()));
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
