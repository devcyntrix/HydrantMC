package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;

import java.util.List;

public class DeopCommand extends AbstractCommand {

    public DeopCommand() {
        super("deop");
        super.description = "Takes a player op";
        super.permission = "hydrant.command.deop";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length != 1) {
            sender.sendMessage("§c/deop <Player>");
            return;
        }

        Server server = sender.getServer();

        Player target = server.getPlayer(args[0]);

        target.setOp(false);

        server.broadcast((object) -> object.hasPermission(getPermission()), player -> player.sendMessage("§5" + sender.getName() + " §7» §cde-opped player §3" + target.getName()));
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
