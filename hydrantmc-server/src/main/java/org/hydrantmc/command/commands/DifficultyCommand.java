package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.world.Difficulty;

import java.util.Collection;
import java.util.HashSet;

public class DifficultyCommand extends AbstractCommand {

    public DifficultyCommand() {
        super("difficulty");
        super.description = "Sets the difficulty on the server";
        super.permission = "hydrant.command.difficulty";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length != 1) {
            sender.sendMessage("§c/difficulty <difficulty>");
            return;
        }

        Server server = sender.getServer();

        Difficulty difficulty = Difficulty.getDifficulty(args[0]);

        if(difficulty == null) {
            sender.sendMessage("§5Difficulty §7» §cThe argument §3" + args[0] + " §cisn't a valid difficulty.");
            return;
        }

        server.broadcast((object) -> object.hasPermission(getPermission()),
                player -> player.sendMessage("§5" + sender.getName() + " §7» §cchanged difficulty to §3" + difficulty.getName()));
    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        Difficulty[] difficulties = Difficulty.values();
        Collection<String> strings = new HashSet<>();
        for (Difficulty difficulty : difficulties) {
            strings.add(difficulty.name());
        }
        return args.length > 0 ? strings : null;
    }

}
