package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.utils.player.GameMode;

import java.util.Collection;
import java.util.HashSet;

public class FeedCommand extends AbstractCommand {

    public FeedCommand() {
        super("feed");
        super.description = "Feeds a player";
        super.permission = "hydrant.command.feed";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length > 1) {
            sender.sendMessage("§c/feed [Player]");
            return;
        }

        Server server = sender.getServer();

        Player target = (Player) sender;

        if(args.length == 1) {
            target = server.getPlayer(args[0]);

            if(target == null) {
                sender.sendMessage("§5Feed §7» §cThe player isn't online.");
                return;
            }
        }

        target.setFoodLevel(20);
        target.setFoodSaturation(5);

        Player finalTarget = target;
        server.broadcast((object) -> object.hasPermission(getPermission()), player -> player.sendMessage("§5" + sender.getName() + " §7» §cfeeded player §3" + finalTarget.getName()));
    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        GameMode[] gameModes = GameMode.values();
        Collection<String> strings = new HashSet<>();
        for (GameMode gameMode : gameModes) {
            strings.add(gameMode.name());
        }
        return args.length > 0 ? strings : null;
    }

}
