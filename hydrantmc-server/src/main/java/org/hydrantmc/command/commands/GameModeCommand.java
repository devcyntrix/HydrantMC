package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.utils.player.GameMode;

import java.util.Collection;
import java.util.HashSet;

public class GameModeCommand extends AbstractCommand {

    public GameModeCommand() {
        super("gamemode");
        super.description = "Sets the GameMode of a player";
        super.permission = "hydrant.command.gamemode";
        super.aliases = new String[] { "gm" };
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length < 1 || args.length > 2) {
            sender.sendMessage("§c/gamemode <Mode> [Player]");
            return;
        }

        Server server = sender.getServer();

        Player target = (Player) sender;

        if(args.length == 2) {
            target = server.getPlayer(args[1]);

            if(target == null) {
                sender.sendMessage("§5GameMode §7» §cThe player isn't online.");
                return;
            }
        }

        GameMode gameMode = GameMode.getGameMode(args[0]);

        if(gameMode == null) {
            sender.sendMessage("§5GameMode §7» §cThe argument §3" + args[0] + " §cisn't a valid gamemode.");
        }

        target.setGameMode(gameMode);

        Player finalTarget = target;
        server.broadcast((object) -> object.hasPermission(getPermission()), player -> player.sendMessage("§5" + sender.getName() + " §7» §cset gamemode of player §3" + finalTarget.getName() + " §cto §3" + gameMode.name()));
    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        GameMode[] gameModes = GameMode.values();
        Collection<String> strings = new HashSet<>();
        for (GameMode gameMode : gameModes) {
            strings.add(gameMode.name());
        }
        return args.length > 0 ? strings : null;
    }

}
