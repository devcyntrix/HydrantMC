package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;

import java.util.Arrays;
import java.util.List;

public class KickCommand extends AbstractCommand {

    public KickCommand() {
        super("kick");
        super.description = "Kicks a player from the server";
        super.permission = "hydrant.command.kick";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length < 2) {
            sender.sendMessage("§c/kick <Player> <Reason>");
            return;
        }

        Server server = sender.getServer();

        Player target = server.getPlayer(args[0]);
        args = Arrays.copyOfRange(args, 1, args.length);
        String reason = String.join(" ", args);
        target.kickPlayer(sender, reason);

        server.broadcast((object) -> object.hasPermission(getPermission()), player -> player.sendMessage("§5" + sender.getName() + " §7» §ckicked player §3" + target.getName() + " §cwith reason §3" + reason));
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
