package org.hydrantmc.command.commands;

import com.google.common.base.Function;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;

import java.util.Collection;
import java.util.stream.Collectors;

public class ListCommand extends AbstractCommand {

    public ListCommand() {
        super("list");
        super.description = "Lists all players";
        super.permission = "hydrant.command.list";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        Collection<String> players = sender.getServer().getOnlinePlayers().stream()
                .map((Function<Player, String>) CommandSender::getName)
                .collect(Collectors.toList());

        sender.sendMessage("Players (" + players.size() + "): " + String.join(", ", players));
    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
