package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;

import java.util.List;

public class OpCommand extends AbstractCommand {

    public OpCommand() {
        super("op");
        super.description = "Gives a player op";
        super.permission = "hydrant.command.op";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length != 1) {
            sender.sendMessage("§c/op <Player>");
            return;
        }

        Server server = sender.getServer();

        Player target = server.getPlayer(args[0]);

        target.setOp(true);

        server.broadcast(object -> object.hasPermission(getPermission()), player -> player.sendMessage("§5" + sender.getName() + " §7» §copped player §3" + target.getName()));
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
