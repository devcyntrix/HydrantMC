package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.network.play.out.PacketOutSoundEffect;
import org.hydrantmc.api.utils.Sound;

import java.util.Collection;
import java.util.List;

public class PlaySoundCommand extends AbstractCommand {

    public PlaySoundCommand() {
        super("playsound");
        super.description = "Sends a player a sound";
        super.permission = "hydrant.command.playsound";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length != 2) {
            sender.sendMessage("§c/playsound <sound> <pitch>");
            return;
        }

        Server server = sender.getServer();

        //server.broadcast(player -> player.sendPacket(new PacketOutSoundEffect(Sound.RANDOM_CLICK, player.getLocation(), 90, Byte.parseByte(args[0]))));
        server.broadcast(player -> player.sendPacket(new PacketOutSoundEffect(args[0], player.getLocation(), 90, Float.parseFloat(args[1]))));
    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }
}
