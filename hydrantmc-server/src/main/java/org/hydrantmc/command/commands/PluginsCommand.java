package org.hydrantmc.command.commands;

import com.google.common.base.Function;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.plugin.HydrantPlugin;

import java.util.Collection;
import java.util.stream.Collectors;

public class PluginsCommand extends AbstractCommand {

    public PluginsCommand() {
        super("plugins");
        super.description = "Lists all plugins";
        super.permission = "hydrant.command.plugins";
        super.aliases = new String[] { "pl" };
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        Collection<String> plugins = sender.getServer().getPluginManager().getPlugins().stream()
                .map((Function<HydrantPlugin, String>) key -> key.getDescription().getName())
                .collect(Collectors.toList());

        sender.sendMessage("Plugins (" + plugins.size() + "): " + String.join(", ", plugins));
    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
