package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.plugin.PluginManager;

import java.util.List;

public class ReloadCommand extends AbstractCommand {

    public ReloadCommand() {
        super("reload");
        super.description = "Reloads the Hydrant Server.";
        super.permission = "hydrant.command.reload";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        Server server = sender.getServer();
        server.reload();
        server.broadcast(object -> object.hasPermission(getPermission()), player -> player.sendMessage("§5" + sender.getName() + " §7» §creloaded server successfully"));
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
