package org.hydrantmc.command.commands;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.chat.ChatMessageType;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;

import java.util.Collection;
import java.util.HashSet;

public class SayCommand extends AbstractCommand {

    public SayCommand() {
        super("say");
        super.description = "Broadcasts something to all players";
        super.permission = "hydrant.command.say";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(args.length == 0) {
            sender.sendMessage("§c/say <message>");
            return;
        }

        Server server = sender.getServer();

/*
        ChatMessageType chatMessageType = ChatMessageType.CHAT;

        if(args.length > 1) {
            try {
                chatMessageType = ChatMessageType.valueOf(args[args.length - 1].toUpperCase());

                args = Arrays.copyOfRange(args, 0, args.length - 1);
            } catch (Exception ignored) { }
        }*/

        server.broadcast(player -> player.sendMessage(ChatMessageType.CHAT, "§5" + sender.getName() + " §7» §c" + String.join(" ", args)));
    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        ChatMessageType[] types = ChatMessageType.values();
        Collection<String> strings = new HashSet<>();
        for (ChatMessageType type : types) {
            strings.add(type.name());
        }
        return args.length > 0 ? strings : null;
    }

}
