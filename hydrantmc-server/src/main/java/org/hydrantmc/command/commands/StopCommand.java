package org.hydrantmc.command.commands;

import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;

import java.util.List;

public class StopCommand extends AbstractCommand {

    public StopCommand() {
        super("stop");
        super.description = "Stops the Hydrant Server.";
        super.permission = "hydrant.command.stop";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        String message = "Server closed";

        if(args.length > 0) {
            message = String.join(" ", args);
        }

        for(Player player : sender.getServer().getOnlinePlayers()) {
            player.kickPlayer(message);
        }

        sender.getServer().shutdown();
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
