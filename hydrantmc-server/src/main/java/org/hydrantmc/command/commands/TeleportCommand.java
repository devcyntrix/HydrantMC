package org.hydrantmc.command.commands;

import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;

import java.util.List;

public class TeleportCommand extends AbstractCommand {

    public TeleportCommand() {
        super("teleport");
        super.description = "Teleports you to a player";
        super.permission = "hydrant.command.teleport";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {

        if(args.length < 1 || args.length > 2) {
            sender.sendMessage("§c/teleport <player> [target]");
            return;
        }

        Player player = (Player) sender;
        Player target = sender.getServer().getPlayer(args[0]);

        if(args.length == 2) {
            player = target;
            target = sender.getServer().getPlayer(args[1]);
        }

        player.teleport(target);
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
