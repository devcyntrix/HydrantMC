package org.hydrantmc.command.commands;

import org.hydrantmc.api.chat.TextComponent;
import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.world.chunk.ChunkColumn;

import java.util.List;

public class UnloadChunkCommand extends AbstractCommand {

    public UnloadChunkCommand() {
        super("unloadchunk");
        super.description = "Unloads a chunk.";
        super.permission = "hydrant.command.unloadchunk";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {
        if(!(sender instanceof Player)) {
            sender.sendMessage("§cYou must be a player.");
            return;
        }

        if(args.length > 2) {
            sender.sendMessage("§c/unloadchunk [<chunk x> <chunk z>]");
            return;
        }

        Player player = (Player) sender;

        int chunkX = player.getLocation().getChunkX();
        int chunkZ = player.getLocation().getChunkZ();

        if(args.length == 2) {
            chunkX = Integer.parseInt(args[0]);
            chunkZ = Integer.parseInt(args[1]);
        }

        TextComponent title = TextComponent.fromColoredMessage("Test");
        TextComponent subTitle = TextComponent.fromColoredMessage("Test");

        player.getServer().broadcast((target) -> target.sendTitle(title, subTitle));
        ChunkColumn column = player.getWorld().getChunkProvider().provideChunkColumn(chunkX, chunkZ);
    }

    @Override
    public List<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
