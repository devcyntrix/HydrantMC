package org.hydrantmc.command.commands;

import org.hydrantmc.api.command.AbstractCommand;
import org.hydrantmc.api.command.CommandSender;

import java.util.Collection;

public class WhitelistCommand extends AbstractCommand {

    public WhitelistCommand() {
        super("whitelist");
        super.description = "Manages the whitelist";
        super.permission = "hydrant.command.whitelist";
    }

    @Override
    public void executeCommand(CommandSender sender, String[] args) {

    }

    @Override
    public Collection<String> tabCompleteCommand(CommandSender commandSender, String[] args) {
        return null;
    }

}
