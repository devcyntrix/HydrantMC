package org.hydrantmc.compressor;

import io.netty.buffer.ByteBuf;

abstract class AbstractCompressor {

    public abstract void end();

    public abstract ByteBuf compress(ByteBuf in);

    public abstract ByteBuf decompress(ByteBuf in);

    public abstract byte[] compress(byte[] in);

    public abstract byte[] decompress(byte[] in);

}
