package org.hydrantmc.compressor;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import java.io.ByteArrayOutputStream;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class JavaZLibCompressor extends AbstractCompressor {

    private final Deflater deflater;
    private final Inflater inflater;

    public JavaZLibCompressor() {
        this.deflater = new Deflater();
        this.inflater = new Inflater();
    }

    @Override
    public void end() {
        deflater.end();
        inflater.end();
    }

    @Override
    public ByteBuf compress(ByteBuf in) {
        byte[] bytes = new byte[in.readableBytes()];
        in.readBytes(bytes);

        ByteBuf byteBuf = Unpooled.buffer();
        byteBuf.writeBytes(compress(bytes));

        return byteBuf;
    }

    @Override
    public ByteBuf decompress(ByteBuf in) {
        return null;
    }

    @Override
    public byte[] compress(byte[] in) {
        byte[] buffer = new byte[8192];

        deflater.setInput(in);
        deflater.finish();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        while(!deflater.finished()) {
            int count = deflater.deflate(buffer);
            stream.write(buffer, 0, count);
        }

        deflater.reset();

        return stream.toByteArray();
    }

    @Override
    public byte[] decompress(byte[] in) {
        byte[] buffer = new byte[8192];

        inflater.setInput(in);

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        try {
            while(!inflater.finished()) {
                int count = inflater.inflate(buffer);
                stream.write(buffer, 0, count);
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        }

        inflater.reset();

        return stream.toByteArray();
    }
}
