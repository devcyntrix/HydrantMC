package org.hydrantmc.encryption;

import io.netty.buffer.ByteBuf;

public abstract class AbstractCipher {

    public abstract byte[] encryptByteArray(byte[] bytes);

    public abstract byte[] decryptByteArray(byte[] bytes);

    public abstract ByteBuf encryptByteBuf(ByteBuf byteBuf);

    public abstract ByteBuf decryptByteBuf(ByteBuf byteBuf);

}
