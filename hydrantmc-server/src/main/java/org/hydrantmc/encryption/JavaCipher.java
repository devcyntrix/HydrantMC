package org.hydrantmc.encryption;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;

import javax.crypto.Cipher;
import java.security.Key;
import java.security.spec.AlgorithmParameterSpec;

public class JavaCipher extends AbstractCipher {

    private Cipher cipher;
    private final Key key;
    private final AlgorithmParameterSpec parameterSpec;

    public JavaCipher(String algorithm, Key key) {
        this(algorithm, key, null);
    }
    public JavaCipher(String algorithm, Key key, AlgorithmParameterSpec parameterSpec) {
        try {
            this.cipher = Cipher.getInstance(algorithm);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        this.key = key;
        this.parameterSpec = parameterSpec;
    }

    @Override
    public byte[] encryptByteArray(byte[] bytes) {
        try {
            if(this.parameterSpec != null) {
                this.cipher.init(Cipher.ENCRYPT_MODE, this.key, this.parameterSpec);
            } else this.cipher.init(Cipher.ENCRYPT_MODE, this.key);

            return this.cipher.doFinal(bytes);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public byte[] decryptByteArray(byte[] bytes) {
        try {
            if(this.parameterSpec != null) {
                this.cipher.init(Cipher.DECRYPT_MODE, this.key, this.parameterSpec);
            } else this.cipher.init(Cipher.DECRYPT_MODE, this.key);
            return this.cipher.doFinal(bytes);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public ByteBuf encryptByteBuf(ByteBuf byteBuf) {
        try {
            if(this.parameterSpec != null) {
                this.cipher.init(Cipher.ENCRYPT_MODE, this.key, this.parameterSpec);
            } else this.cipher.init(Cipher.ENCRYPT_MODE, this.key);

            ByteBuf buf = Unpooled.buffer();

            int readableBytes = byteBuf.readableBytes();

            byte[] bytes = new byte[byteBuf.readableBytes()];
            byteBuf.readBytes(bytes);

            byte[] output = new byte[this.cipher.getOutputSize(readableBytes)];

            buf.writeBytes(output, 0, this.cipher.update(bytes, 0, readableBytes, output));

            return buf;
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }

    @Override
    public ByteBuf decryptByteBuf(ByteBuf byteBuf) {
        try {
            if(this.parameterSpec != null) {
                this.cipher.init(Cipher.DECRYPT_MODE, this.key, this.parameterSpec);
            } else this.cipher.init(Cipher.DECRYPT_MODE, this.key);

            byte[] readableBytes = new byte[byteBuf.readableBytes()];
            byteBuf.readBytes(readableBytes);

            return Unpooled.wrappedBuffer(this.cipher.doFinal(readableBytes));
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        return null;
    }
}
