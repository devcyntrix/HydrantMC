package org.hydrantmc.logging;

import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class HydrantLogger extends Logger {

    public HydrantLogger() {
        super("Hydrant", null);
    }

    @Override
    public void log(LogRecord record) {
        super.log(record);
    }

}
