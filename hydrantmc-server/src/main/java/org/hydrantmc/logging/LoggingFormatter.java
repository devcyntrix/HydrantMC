package org.hydrantmc.logging;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

public class LoggingFormatter extends Formatter {

    private final DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    private final DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

    @Override
    public String format(LogRecord record) {
        return "[" + dateFormat.format(new Date(record.getMillis())) + " " + timeFormat.format(new Date(record.getMillis())) + " | " + record.getLevel().getLocalizedName() + "] " + record.getMessage() + (record.getThrown() != null ? throwableToString(record.getThrown()) : "");
    }

    private String throwableToString(Throwable t) {
        StringBuilder sb = new StringBuilder(": \n").append(t.toString()).append(" ");

        for(StackTraceElement stackTraceElement : t.getStackTrace()) {
            sb.append("\n").append(stackTraceElement);
        }

        return sb.toString();
    }

}
