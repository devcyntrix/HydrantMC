package org.hydrantmc.logging.handler;

import java.io.PrintStream;
import java.util.logging.Level;

import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class ConsoleHandler extends Handler {

    private final PrintStream printStream;
    private final PrintStream errorPrintStream;

    public ConsoleHandler(PrintStream printStream, PrintStream errorPrintStream) {
        this.printStream = printStream;
        this.errorPrintStream = errorPrintStream;
    }

    @Override
    public void publish(LogRecord record) {
        if(getFilter() != null && !getFilter().isLoggable(record)) return;
        if(getFormatter() != null) {
            record.setMessage(getFormatter().formatMessage(record));
            record.setMessage(getFormatter().format(record));
        }

        if(record.getLevel() == Level.SEVERE) {
            errorPrintStream.println(record.getMessage());
            return;
        }
        printStream.println(record.getMessage());
    }

    @Override
    public void flush() {
        printStream.flush();
        errorPrintStream.flush();
    }

    @Override
    public void close() throws SecurityException {}
}
