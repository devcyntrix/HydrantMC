package org.hydrantmc.network;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.IncomingPacket;
import org.hydrantmc.api.network.PendingConnection;

public class HydrantChannelPacketReader extends SimpleChannelInboundHandler<IncomingPacket> {

    private final Server server;
    private final PendingConnection connection;

    public HydrantChannelPacketReader(Server server, PendingConnection connection) {
        this.server = server;
        this.connection = connection;
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) {
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) {
        AbstractPacketHandler packetHandler = this.connection.getHandler();

        if(packetHandler == null) {
            throw new IllegalStateException("The packet handler is null!");
        }

        packetHandler.unregister();
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, IncomingPacket packet) {
        AbstractPacketHandler packetHandler = this.connection.getHandler();

        if(packetHandler == null) {
            throw new IllegalStateException("The packet handler is null!");
        }

        packet.handlePacket(packetHandler);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        AbstractPacketHandler packetHandler = this.connection.getHandler();

        if(packetHandler == null) {
            System.err.println("The packet handler is null!");
            return;
        }

        packetHandler.caughtException(cause);
    }
}
