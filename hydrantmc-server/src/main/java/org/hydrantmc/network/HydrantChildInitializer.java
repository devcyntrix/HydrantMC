package org.hydrantmc.network;

import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.util.AttributeKey;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.ConnectionState;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.network.codec.PacketCodec;
import org.hydrantmc.network.codec.VarIntFrameCodec;
import org.hydrantmc.network.handler.LegacyPingHandler;

public class HydrantChildInitializer extends ChannelInitializer<Channel> {

    private final AttributeKey<PendingConnection> attributeKey = AttributeKey.valueOf("connection");
    private final Server server;

    public HydrantChildInitializer(Server server) {
        this.server = server;
    }

    @Override
    protected void initChannel(Channel channel) {
        PendingConnection connection = new HydrantPendingConnection(this.server, channel);
        connection.setConnectionState(ConnectionState.HANDSHAKE);

        ChannelPipeline pipeline = channel.pipeline();

        pipeline.addLast("timeout", new ReadTimeoutHandler(30));
        pipeline.addLast("legacy_query", new LegacyPingHandler(server));
        pipeline.addLast("varIntFrameCodec", new VarIntFrameCodec());
        pipeline.addLast("packetCodec", new PacketCodec(connection));
        pipeline.addLast("packetReader", new HydrantChannelPacketReader(this.server, connection));
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        PendingConnection connection = ctx.channel().attr(attributeKey).get();

        if(connection.getHandler() == null) {
            ctx.channel().close();
            cause.printStackTrace();
            return;
        }

        AbstractPacketHandler handler = connection.getHandler();
        handler.caughtException(cause);
    }

}
