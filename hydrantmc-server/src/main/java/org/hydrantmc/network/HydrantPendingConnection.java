package org.hydrantmc.network;

import com.google.common.base.Objects;
import com.google.common.base.Preconditions;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.chat.ChatComponent;
import org.hydrantmc.api.chat.TextComponent;
import org.hydrantmc.api.network.AbstractPacketHandler;
import org.hydrantmc.api.network.ConnectionState;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.network.login.out.PacketOutLoginDisconnect;
import org.hydrantmc.api.network.play.out.PacketOutDisconnect;
import org.hydrantmc.api.utils.player.GameProfile;
import org.hydrantmc.network.codec.CipherCodec;
import org.hydrantmc.network.codec.CompressionCodec;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import java.net.InetSocketAddress;
import java.security.Key;

public class HydrantPendingConnection implements PendingConnection {

    private final Server server;
    private final Channel channel;

    private ConnectionState connectionState;
    private AbstractPacketHandler handler;

    private int protocolVersion;
    private String loginName;

    private Key key;

    private GameProfile profile;

    public HydrantPendingConnection(Server server, Channel channel) {
        this.server = server;
        this.channel = channel;
    }

    @Override
    public Server getServer() {
        return this.server;
    }

    @Override
    public Channel getChannel() {
        return this.channel;
    }

    @Override
    public InetSocketAddress getAddress() {
        return (InetSocketAddress) this.channel.remoteAddress();
    }

    @SuppressWarnings("unchecked")
    @Override
    public ChannelFuture sendPacket(OutgoingPacket packet) {
        Preconditions.checkNotNull(packet, "packet cannot be null");

        if(!getChannel().isOpen()) return channel.voidPromise();

        packet.handlePacket(getHandler());
        return this.channel.write(packet).addListener(ChannelFutureListener.FIRE_EXCEPTION_ON_FAILURE);
    }

    @Override
    public ChannelFuture sendPacketAndFlush(OutgoingPacket packet) {
        Preconditions.checkNotNull(packet, "packet cannot be null");

        if(!getChannel().isOpen()) return channel.voidPromise();

        packet.handlePacket(getHandler());

        if(!getChannel().isOpen()) return channel.voidPromise();

        return this.channel.writeAndFlush(packet);
    }

    @Override
    public void flush() {
        this.channel.flush();
    }

    @Override
    public String getLoginName() {
        return this.loginName;
    }
    
    public void setLoginName(String name) {
        this.loginName = name;
    }

    @Override
    public ConnectionState getConnectionState() {
        return this.connectionState;
    }

    @Override
    public void setConnectionState(ConnectionState protocol) {
        if(protocol == null) return;
        if(this.handler != null) getHandler().unregister();
        this.connectionState = protocol;
        if(this.connectionState.getPacketHandler() == null) return;
        try {
            this.handler = this.connectionState.getPacketHandler().getConstructor(Server.class).newInstance(this.server);
            this.handler.register(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public AbstractPacketHandler getHandler() {
        return handler;
    }

    @Override
    public void disconnect() {
        this.channel.close().syncUninterruptibly();
    }

    @Override
    public void disconnect(String reason) {
        disconnect(TextComponent.fromColoredMessage(reason));
    }

    @Override
    public void disconnect(ChatComponent component) {
        OutgoingPacket packet;

        switch (this.connectionState) {
            case LOGIN:
                packet = new PacketOutLoginDisconnect(component);
                break;
            case PLAY:
                packet = new PacketOutDisconnect(component);
                break;
                default:
                    getChannel().close();
                    return;
        }

        sendPacketAndFlush(packet).addListener(ChannelFutureListener.CLOSE);
    }

    @Override
    public int getProtocolVersion() {
        return protocolVersion;
    }

    @Override
    public void setProtocolVersion(int protocolVersion) {
        this.protocolVersion = protocolVersion;
    }

    @Override
    public void enableCompressionThreshold(int threshold) {
        this.channel.pipeline().addAfter("varIntFrameCodec", "compression", new CompressionCodec(threshold));
    }

    @Override
    public void enableEncryption(Key key) {
        if(this.key != null) return;

        this.key = key;

        Cipher encryptionCipher = null;
        try {
            encryptionCipher = Cipher.getInstance("AES/CFB8/NoPadding");
            encryptionCipher.init(Cipher.ENCRYPT_MODE, this.key, new IvParameterSpec(this.key.getEncoded()));
        } catch (Exception e) {
            e.printStackTrace();
        }

        Cipher decryptionCipher = null;
        try {
            decryptionCipher = Cipher.getInstance("AES/CFB8/NoPadding");
            decryptionCipher.init(Cipher.DECRYPT_MODE, this.key, new IvParameterSpec(this.key.getEncoded()));
        } catch (Exception e) {
            e.printStackTrace();
        }
        getChannel().pipeline().addBefore("varIntFrameCodec", "EncryptionCodec",
                new CipherCodec(encryptionCipher, decryptionCipher));
    }

    @Override
    public void disableEncryption() {
        this.key = null;
        channel.pipeline().remove("EncryptionCodec");
    }

    @Override
    public GameProfile getGameProfile() {
        return this.profile;
    }

    @Override
    public void setGameProfile(GameProfile profile) {
        this.profile = profile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        HydrantPendingConnection that = (HydrantPendingConnection) o;
        return Objects.equal(getChannel(), that.getChannel()) &&
                Objects.equal(profile.getUniqueId(), that.profile.getUniqueId());
    }
}
