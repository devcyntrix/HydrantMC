package org.hydrantmc.network.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;

import javax.crypto.Cipher;
import java.util.List;

/**
 * Created by Cyntrix on 18.07.2017.
 */
public class CipherCodec extends MessageToMessageCodec<ByteBuf, ByteBuf> {

    private final Cipher encipher;
    private final ThreadLocal<byte[]> heapOutLocal = new EmptyByteThreadLocal();

    private final Cipher decipher;
    private final ThreadLocal<byte[]> heapInLocal = new EmptyByteThreadLocal();

    public CipherCodec(Cipher encipher, Cipher decipher) {
        this.encipher = encipher;
        this.decipher = decipher;
    }

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        int length = byteBuf.readableBytes();

        byte[] heapIn = this.heapInLocal.get();
        {
            if(heapIn.length < length) {
                heapIn = new byte[length];
                this.heapInLocal.set(heapIn);
            }
            byteBuf.readBytes(heapIn, 0, length);
        }

        byte[] heapOut = heapOutLocal.get();
        int outputLength = this.encipher.getOutputSize(length);

        if(heapOut.length < outputLength) {
            heapOut = new byte[outputLength];
            heapOutLocal.set(heapOut);
        }


        ByteBuf buf = channelHandlerContext.alloc().heapBuffer();
        buf.writeBytes(heapOut, 0, this.encipher.update(heapIn, 0, length, heapOut));

        list.add(buf);
    }

    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf byteBuf, List<Object> list) throws Exception {
        int length = byteBuf.readableBytes();

        byte[] heapIn = this.heapInLocal.get();
        {
            if(heapIn.length < length) {
                heapIn = new byte[length];
                this.heapInLocal.set(heapIn);
            }
            byteBuf.readBytes(heapIn, 0, length);
        }

        ByteBuf buf = channelHandlerContext.alloc().heapBuffer(this.decipher.getOutputSize(length));
        buf.writerIndex(this.decipher.update(heapIn, 0, length, buf.array(), buf.arrayOffset()));

        list.add(buf);
    }

    public class EmptyByteThreadLocal extends ThreadLocal<byte[]> {

        @Override
        protected byte[] initialValue() {
            return new byte[0];
        }
    }

}
