package org.hydrantmc.network.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import io.netty.handler.codec.DecoderException;
import org.hydrantmc.api.network.PacketBuffer;

import java.io.ByteArrayOutputStream;
import java.util.List;
import java.util.zip.Deflater;
import java.util.zip.Inflater;

public class CompressionCodec extends ByteToMessageCodec<ByteBuf> {

    private final byte[] buffer = new byte[8192];

    private final int threshold;

    private final Deflater deflater;
    private final Inflater inflater;

    public CompressionCodec(int threshold) {
        this.threshold = threshold;
        this.deflater = new Deflater();
        this.inflater = new Inflater();
    }

    @Override
    public void handlerRemoved(ChannelHandlerContext ctx) throws Exception {
        super.handlerRemoved(ctx);
        this.deflater.end();
        this.inflater.end();
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) {
        int readableBytes = msg.readableBytes();
        PacketBuffer packetBuffer = new PacketBuffer(out);

        if(readableBytes < this.threshold) {
            packetBuffer.writeVarInteger(0);
            out.writeBytes(msg);
            return;
        }

        byte[] bytes = new byte[readableBytes];
        msg.readBytes(bytes);

        packetBuffer.writeVarInteger(bytes.length);

        deflater.setInput(bytes);
        deflater.finish();

        ByteArrayOutputStream stream = new ByteArrayOutputStream();

        while(!deflater.finished()) {
            int count = deflater.deflate(buffer);
            stream.write(buffer, 0, count);
        }
        byte[] compressedBytes = stream.toByteArray();
        out.writeBytes(compressedBytes);

        deflater.reset();
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) throws Exception {
        if(in.readableBytes() == 0) return;

        PacketBuffer buffer = new PacketBuffer(in);
        int length = buffer.readVarInteger();

        if(length == 0) {
            out.add(in.readBytes(buffer.toByteBuf().readableBytes()));
            return;
        }

        if (length < this.threshold) throw new DecoderException("Badly compressed packet - size of " + length + " is below server threshold of " + this.threshold);
        if (length > 2097152) throw new DecoderException("Badly compressed packet - size of " + length + " is larger than protocol maximum of " + 2097152);

        byte[] compressedBytes = new byte[in.readableBytes()];
        in.readBytes(compressedBytes);

        this.inflater.setInput(compressedBytes);

        byte[] uncompressedBytes = new byte[length];

        this.inflater.inflate(uncompressedBytes);

        this.inflater.reset();
        out.add(Unpooled.wrappedBuffer(uncompressedBytes));
    }
}
