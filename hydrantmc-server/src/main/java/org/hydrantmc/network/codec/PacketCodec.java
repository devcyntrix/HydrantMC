package org.hydrantmc.network.codec;

import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToMessageCodec;
import org.hydrantmc.api.network.*;

import java.util.List;

public class PacketCodec extends MessageToMessageCodec<ByteBuf, OutgoingPacket<?>> {

    private final PendingConnection connection;

    public PacketCodec(PendingConnection connection) {
        this.connection = connection;
    }

    @Override
    protected void encode(ChannelHandlerContext ctx, OutgoingPacket<?> msg, List<Object> out) {
        ConnectionState protocol = this.connection.getConnectionState();

        int packetId = protocol.getIdByPacketClass(PacketDirection.CLIENT, msg.getClass());

        if(packetId < 0) {
            System.err.println("[OUT " + protocol.name() + "]: Packet with Id " + packetId + " not registered!");
            return;
        }

        PacketBuffer buffer = new PacketBuffer(ctx.alloc().buffer());

        buffer.writeVarInteger(packetId);
        msg.writePacket(buffer);

        System.out.println("[OUT " + protocol.name() + "]: Packet with Id " + packetId + " and " + buffer.toByteBuf().readableBytes());

        out.add(buffer.toByteBuf());
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf msg, List<Object> out) throws Exception {
        if(msg.readableBytes() == 0) return;

        PacketBuffer buffer = new PacketBuffer(msg);
        int packetId = buffer.readVarInteger();

        ConnectionState protocol = connection.getConnectionState();

        Class<? extends Packet> packetClass = protocol.getPacketClassById(PacketDirection.SERVER, packetId);
        if(packetClass == null) {
            System.err.println("[IN " + protocol.name() + "]: Packet with Id " + packetId + " is not registered!");
            return;
        }
        IncomingPacket packet = (IncomingPacket) packetClass.getConstructor().newInstance();

        packet.readPacket(buffer);

        if (msg.isReadable()) throw new Exception("Did not read all bytes from Packet " + packet.getClass().getSimpleName() + " " + packetId);

        out.add(packet);
    }
}
