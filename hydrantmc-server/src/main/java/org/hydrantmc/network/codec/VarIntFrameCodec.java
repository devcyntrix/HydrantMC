package org.hydrantmc.network.codec;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageCodec;
import org.hydrantmc.api.network.PacketBuffer;

import java.util.List;

public class VarIntFrameCodec extends ByteToMessageCodec<ByteBuf> {

    @Override
    protected void encode(ChannelHandlerContext ctx, ByteBuf msg, ByteBuf out) {

        int readableBytes = msg.readableBytes();
        int varIntSize = getVarIntSize(readableBytes);

        if(varIntSize > 3) {
            /* TODO: Write an own message */
            throw new IllegalArgumentException("unable to fit " + readableBytes + " into " + 3);
        } else {
            PacketBuffer packetBuffer = new PacketBuffer(out);

            out.ensureWritable(varIntSize + readableBytes);
            packetBuffer.writeVarInteger(readableBytes);
            out.writeBytes(msg);
        }
    }

    @Override
    protected void decode(ChannelHandlerContext ctx, ByteBuf in, List<Object> out) {
        in.markReaderIndex();
        byte[] bs = new byte[3];

        for(int i = 0; i < bs.length; i++) {
            if(!in.isReadable()) {
                in.resetReaderIndex();
                return;
            }

            bs[i] = in.readByte();
            if(bs[i] >= 0) {
                PacketBuffer packetBuffer = new PacketBuffer(Unpooled.wrappedBuffer(bs));

                try {
                    int length = packetBuffer.readVarInteger();
                    if (in.readableBytes() >= length) {
                        out.add(in.readBytes(length));
                        return;
                    }
                    in.resetReaderIndex();
                } finally {
                    packetBuffer.toByteBuf().release();
                }
                return;
            }
        }
    }

    private int getVarIntSize(int input) {
        for(int i = 1; i < 5; ++i) {
            if((input & -1 << i * 7) == 0) {
                return i;
            }
        }
        return 5;
    }
}
