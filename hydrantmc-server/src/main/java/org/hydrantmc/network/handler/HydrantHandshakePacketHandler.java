package org.hydrantmc.network.handler;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.network.ConnectionState;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.network.handshake.AbstractHandshakeHandler;
import org.hydrantmc.api.network.handshake.in.PacketInHandshake;

public class HydrantHandshakePacketHandler extends AbstractHandshakeHandler {

    private PendingConnection connection;

    public HydrantHandshakePacketHandler(Server server) {
        super(server);
    }

    @Override
    public void register(PendingConnection connection) {
        this.connection = connection;
    }

    @Override
    public void unregister() {}

    @Override
    public void caughtException(Throwable cause) {
        /* TODO: Write an error */
        cause.printStackTrace();
    }

    @Override
    public void handleIncomingHandshake(PacketInHandshake handshake) {
        connection.setProtocolVersion(handshake.getProtocolId());

        switch (handshake.getNextState()) {
            case 1:
                connection.setConnectionState(ConnectionState.STATUS);
                break;
            case 2:
                if(connection.getProtocolVersion() < connection.getServer().getProtocolVersion()) {
                    connection.disconnect(connection.getServer().getConfiguration().getKickMessages().getOutdatedClientKickMessage());
                    return;
                } else if(connection.getProtocolVersion() > connection.getServer().getProtocolVersion()) {
                    connection.disconnect(connection.getServer().getConfiguration().getKickMessages().getOutdatedServerKickMessage());
                    return;
                }

                connection.setConnectionState(ConnectionState.LOGIN);

                break;
            default:
                System.err.println(connection.getChannel().remoteAddress().toString() + " sent an invalid state.");
                connection.getChannel().close();
                break;
        }
    }

}
