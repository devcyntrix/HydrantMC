package org.hydrantmc.network.handler;

import com.google.gson.Gson;
import io.netty.channel.ChannelFutureListener;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.chat.TranslationComponent;
import org.hydrantmc.api.network.ConnectionState;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.network.login.AbstractLoginPacketHandler;
import org.hydrantmc.api.network.login.in.PacketInEncryptionResponse;
import org.hydrantmc.api.network.login.in.PacketInLoginRequest;
import org.hydrantmc.api.network.login.out.PacketOutCompression;
import org.hydrantmc.api.network.login.out.PacketOutEncryptionRequest;
import org.hydrantmc.api.network.login.out.PacketOutLoginDisconnect;
import org.hydrantmc.api.network.login.out.PacketOutLoginResponse;
import org.hydrantmc.api.utils.player.GameProfile;
import org.hydrantmc.encryption.AbstractCipher;
import org.hydrantmc.encryption.JavaCipher;

import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyPair;
import java.security.MessageDigest;
import java.security.PublicKey;
import java.util.Arrays;
import java.util.Random;
import java.util.UUID;

public class HydrantLoginPacketHandler extends AbstractLoginPacketHandler {

    private final Random random = new Random();
    private PendingConnection connection;

    /* Encryption */
    private String serverHash;
    private byte[] verifyToken;

    private String loginUsername;

    public HydrantLoginPacketHandler(Server server) {
        super(server);
    }

    @Override
    public void register(PendingConnection connection) {
        this.connection = connection;
    }

    @Override
    public void unregister() { }

    @Override
    public void caughtException(Throwable cause) {
        cause.printStackTrace();
    }

    @Override
    public void handleOutgoingDisconnect(PacketOutLoginDisconnect disconnect) {
        System.out.println(loginUsername + " logged out in login handling.");
    }

    @Override
    public void handleIncomingLoginRequest(PacketInLoginRequest request) {
        loginUsername = request.getUsername();

        Server server = connection.getServer();

        if(server.getOnlinePlayers().size() >= server.getMaxPlayers()) {
            connection.disconnect(server.getConfiguration().getKickMessages().getServerIsFullKickMessage());
            return;
        }

        /* TODO: Check illegal Characters */

        if(server.isOnlineMode()) {
            /* Send the server id, the verify token and the public key to the client */
            /* Generate server hash */
            serverHash = Long.toString(random.nextInt(16));

            /* Generate verify token */
            verifyToken = new byte[16];
            random.nextBytes(verifyToken);

            /* Get the public key */
            KeyPair keyPair = server.getKeyPair();
            PublicKey publicKey = keyPair.getPublic();

            /* Send the encryption request packet to the client */
            connection.sendPacketAndFlush(new PacketOutEncryptionRequest(serverHash, publicKey.getEncoded(), verifyToken));
            return;
        }

        if(server.getPlayer(loginUsername) != null) {
            connection.disconnect(server.getConfiguration().getKickMessages().getNameAlreadyInUseKickMessage());
            return;
        }

        successLogin(server, new GameProfile(UUID.nameUUIDFromBytes(("OfflinePlayer:" + loginUsername).getBytes()), loginUsername));
    }

    @Override
    public void handleOutgoingEncryptionRequest(PacketOutEncryptionRequest request) {}

    @Override
    public void handleIncomingEncryptionResponse(PacketInEncryptionResponse response) {
        Server server = connection.getServer();
        KeyPair keyPair = server.getKeyPair();

        /* TODO: Optimize the code */

        /* Check the verify token */
        try {
            AbstractCipher cipher = new JavaCipher("RSA", keyPair.getPrivate());

            byte[] decryptedVerifyToken = cipher.decryptByteArray(response.getVerifyToken());

            if(!Arrays.equals(this.verifyToken, decryptedVerifyToken)) {
                connection.disconnect(new TranslationComponent("disconnect.loginFailedInfo.invalidSession"));
                return;
            }

            byte[] decryptSharedToken = cipher.decryptByteArray(response.getSharedSecretKey());

            SecretKey secretKey = new SecretKeySpec(decryptSharedToken, "AES");
            connection.enableEncryption(secretKey);

            String encName = URLEncoder.encode(this.loginUsername,"UTF-8");

            MessageDigest messageDigest = MessageDigest.getInstance("SHA-1");
            messageDigest.update(serverHash.getBytes("ISO_8859_1"));
            messageDigest.update(secretKey.getEncoded());
            messageDigest.update(keyPair.getPublic().getEncoded());

            String encodedHash = URLEncoder.encode(new BigInteger(messageDigest.digest()).toString(16), "UTF-8");

            String authenticationURL = "https://sessionserver.mojang.com/session/minecraft/hasJoined" +
                    "?username=" + encName +
                    "&serverId=" + encodedHash +
                    "&ip=" + URLEncoder.encode(connection.getAddress().getAddress().getHostAddress(), "UTF-8");

            URL url = new URL(authenticationURL);
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

            InputStream inputStream = urlConnection.getInputStream();
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader reader = new BufferedReader(inputStreamReader);

            String httpResponse = reader.readLine();
            reader.close();

            /* TODO: Find more bugs */

            if(httpResponse == null) {
                connection.disconnect("Your game profile is invalid.");
                return;
            }

            Gson gson = GameProfile.GSON;

            GameProfile profile;
            try {
                profile = gson.fromJson(httpResponse, GameProfile.class);
            } catch (Exception exception) {
                exception.printStackTrace();
                connection.disconnect("Failed to read your game profile");
                return;
            }

            if(profile == null) {
                connection.disconnect("We couldn't read your game profile");
                return;
            }

            if(!this.loginUsername.equals(profile.getUsername())) {
                connection.disconnect("Your username is invalid");
                return;
            }

            successLogin(server, profile);

        } catch (Exception exception) {
            connection.disconnect(new TranslationComponent("disconnect.loginFailedInfo.serversUnavailable"));
        }
    }

    public void successLogin(Server server, GameProfile profile) {
        connection.setGameProfile(profile);

        int threshold = server.getNetworkProperties().getCompressionProperties().getThreshold();

        /* Send the threshold to the client */
        connection.sendPacketAndFlush(new PacketOutCompression(threshold)).addListener((ChannelFutureListener) channelFuture -> {
            if(!channelFuture.isSuccess()) return;
            /* Enable the compression */
            connection.enableCompressionThreshold(threshold);
        });

        connection.sendPacketAndFlush(new PacketOutLoginResponse(profile.getUniqueId(), profile.getUsername()));

        connection.setConnectionState(ConnectionState.PLAY);
    }

    @Override
    public void handleOutgoingPacketCompression(PacketOutCompression compression) {}

    @Override
    public void handleOutgoingLoginResponse(PacketOutLoginResponse response) {}
}
