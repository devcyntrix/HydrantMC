package org.hydrantmc.network.handler;

import com.google.gson.JsonObject;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.ServerConfiguration;
import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.block.ChestBlock;
import org.hydrantmc.api.chat.ChatColor;
import org.hydrantmc.api.chat.ChatMessageType;
import org.hydrantmc.api.chat.TextComponent;
import org.hydrantmc.api.chat.TranslationComponent;
import org.hydrantmc.api.command.CommandRegistry;
import org.hydrantmc.api.entity.Item;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.event.player.*;
import org.hydrantmc.api.item.ItemStack;
import org.hydrantmc.api.item.Material;
import org.hydrantmc.api.network.PacketBuffer;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.network.play.AbstractPlayPacketHandler;
import org.hydrantmc.api.network.play.in.*;
import org.hydrantmc.api.network.play.out.*;
import org.hydrantmc.api.network.play.out.entity.*;
import org.hydrantmc.api.network.play.out.player.PacketOutPlayerAbilities;
import org.hydrantmc.api.network.play.out.player.PacketOutPlayerPosLook;
import org.hydrantmc.api.network.play.out.player.PacketOutSpawnPlayer;
import org.hydrantmc.api.network.play.out.window.PacketOutWindowSetSlot;
import org.hydrantmc.api.network.play.out.world.PacketOutBlockChange;
import org.hydrantmc.api.network.play.out.world.PacketOutChunkColumnData;
import org.hydrantmc.api.network.play.out.world.PacketOutMapChunkBulk;
import org.hydrantmc.api.utils.Facing;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.utils.player.GameMode;
import org.hydrantmc.api.utils.player.PlayerAbilities;
import org.hydrantmc.api.world.Difficulty;
import org.hydrantmc.api.world.TerrainType;
import org.hydrantmc.api.world.World;
import org.hydrantmc.api.world.WorldType;
import org.hydrantmc.api.world.chunk.ChunkColumn;

import java.util.*;

public class HydrantPlayPacketHandler extends AbstractPlayPacketHandler {

    private final Timer timer = new Timer("Keep alive updater", true);
    private final Random random = new Random();

    private Player player;

    /*
     * Keep alive things
     */
    private long lastAliveTimestamp;
    private int lastAliveToken;

    public HydrantPlayPacketHandler(Server server) {
        super(server);
    }

    @Override
    public void register(PendingConnection connection) {
        Server server = connection.getServer();
        ServerConfiguration configuration = server.getConfiguration();

        Location spawnLocation = server.getSpawnLocation();

        player = new Player(spawnLocation, connection);

        player.setGameMode(server.getConfiguration().getDefaultGameMode());

        connection.sendPacket(new PacketOutJoinGame(player.getEntityId(), (byte) server.getConfiguration().getDefaultGameMode().ordinal(), WorldType.OVERWORLD, (byte) Difficulty.EASY.ordinal(), (byte) server.getConfiguration().getTabListProperties().getSize(), TerrainType.FLAT, false));

        PacketBuffer packetBuffer = new PacketBuffer();
        packetBuffer.writeString("vanilla");

        connection.sendPacket(new PacketOutCustomPayload("MC|Brand", packetBuffer.toByteArray()));

        connection.sendPacket(new PacketOutServerDifficulty(Difficulty.EASY));
        connection.sendPacket(new PacketOutSpawnPosition(server.getSpawnLocation()));
        server.addPlayer(player);

        player.setPlayerAbilities(player.getPlayerAbilities(), true);
        player.setHeldItemSlot(0);

        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if(!connection.getChannel().isActive()) cancel();
                connection.sendPacket(new PacketOutKeepAlive(random.nextInt()));
                connection.flush();
            }
        }, 0, 2000);

        Location location = server.getSpawnLocation();
        player.setPreviousLocation(location);
        player.setLocation(location);
        location.getWorld().getEntities().add(player);

        connection.sendPacket(new PacketOutPlayerPosLook(location));

        tryToSendChunks(location, null);

        server.getTabList().onPlayerJoin(player);

        PacketOutSpawnPlayer spawnPlayer = new PacketOutSpawnPlayer(player.getEntityId(), player.getUniqueId(), player.getLocation(), (short)0);

        server.broadcast(player1 -> {
            if(player1 != this.player) {
                PacketOutSpawnPlayer spawnPlayer2 = new PacketOutSpawnPlayer(player1.getEntityId(), player1.getUniqueId(), player1.getLocation(), (short)0);
                this.player.sendPacket(spawnPlayer2);
                player1.sendPacket(spawnPlayer);
            }
        });

        TextComponent playerName = new TextComponent(player.getName());
        playerName.setColor(ChatColor.YELLOW);
        TranslationComponent joinMessage = new TranslationComponent("multiplayer.player.joined", playerName);
        joinMessage.setColor(ChatColor.YELLOW);

        PlayerJoinEvent playerJoinEvent = new PlayerJoinEvent(player, joinMessage);

        server.getListenerRegistry().callEvent(playerJoinEvent);

        server.broadcast(player1 -> player1.sendMessage(ChatMessageType.CHAT, playerJoinEvent.getJoinMessage()));
        connection.flush();
    }

    @Override
    public void unregister() {
        getServer().getTabList().removePlayer(player);

        TextComponent playerName = new TextComponent(player.getName());
        playerName.setColor(ChatColor.YELLOW);
        TranslationComponent leaveMessage = new TranslationComponent("multiplayer.player.left", playerName);
        leaveMessage.setColor(ChatColor.YELLOW);

        timer.cancel();

        PlayerQuitEvent playerQuitEvent = new PlayerQuitEvent(player, leaveMessage);

        getServer().getListenerRegistry().callEvent(playerQuitEvent);

        getServer().broadcast(player -> player.sendMessage(ChatMessageType.CHAT, playerQuitEvent.getQuitMessage()));

        getServer().getOnlinePlayers().remove(player);

        getServer().broadcast(player1 -> player1 != this.player, player1 -> player1.sendPacket(new PacketOutDestroyEntities(this.player.getEntityId())));
    }

    @Override
    public void caughtException(Throwable cause) {
        cause.printStackTrace();
    }

    @Override
    public void handleOutgoingDisconnect(PacketOutDisconnect disconnect) { }

    @Override
    public void handleOutgoingKeepAlive(PacketOutKeepAlive keepAlive) {
        this.lastAliveToken = keepAlive.getKeepAliveId();
        this.lastAliveTimestamp = System.currentTimeMillis();
    }

    @Override
    public void handleIncomingKeepAlive(PacketInKeepAlive keepAlive) {
        if(this.lastAliveToken != keepAlive.getKeepAliveId()) return;
        player.setPing((short) (System.currentTimeMillis() - this.lastAliveTimestamp));
    }

    @Override
    public void handleIncomingPlayerDigging(PacketInPlayerDigging digging) {
        int status = digging.getStatus();
        Location location = digging.getLocation();
        location.setWorld(this.player.getWorld());

        Facing facing = digging.getFacing();

        /*
        System.out.println("Status: " + status);
        System.out.println("Location: " + location.getX() + " " + location.getY() + " " + location.getZ());
        System.out.println("Location (Chunk): " + location.getChunkX() + " " + location.getChunkY() + " " + location.getChunkZ());
        System.out.println("Face: " + facing.name());
        */

        switch(status) {
            case 0:
                //START DIGGING
                if(player.getGameMode().equals(GameMode.CREATIVE)) {
                    getServer().broadcast(player1 -> player1.sendPacket(new PacketOutBlockChange(location, (short) 0, (short) 0)));

                    location.setBlock(new AbstractBlock((byte) 0, (byte) 0));
                    return;
                }

                getServer().broadcast(players -> players.sendPacket(new PacketOutBlockBreakAnimation(0, location, (byte) 0)));

                break;
            case 1:
                //CANCEL DIGGING
                getServer().broadcast(players -> players.sendPacket(new PacketOutBlockBreakAnimation(0, location, (byte) -1)));
                break;
            case 2:
                //FINISH DIGGING - DROP ITEMSTACK
                Item item = location.getWorld().spawnEntityAtLocation(Item.class, location);
                item.setItemStack(new ItemStack(Material.getById(location.getBlock().getBlockId()), (byte) 1, location.getBlock().getMetaId()));
                location.setBlock(new AbstractBlock((byte) 0, (byte) 0));

                getServer().broadcast((players) -> players.sendPacket(new PacketOutBlockChange(location, (short) 0, (short) 0)));
                break;
            case 3:
                //DROP ITEMSTACK (SHIFT + Q)
                item = location.getWorld().spawnEntityAtLocation(Item.class, location);
                item.setItemStack(player.getInventory().getItemInHand());
                player.getInventory().setItemInHand(null);
                break;
            case 4:
                //DROP ITEM (Q)
                //TODO: Optimize
                ItemStack itemStack = player.getInventory().getItemInHand();
                ItemStack newItemStack = new ItemStack(itemStack.getMaterial(), (byte) (itemStack.getAmount() - 1), itemStack.getDamage(), itemStack.getTagCompound());
                newItemStack.setItemMeta(itemStack.getItemMeta());
                item = location.getWorld().spawnEntityAtLocation(Item.class, location);
                item.setItemStack(new ItemStack(itemStack.getMaterial(), (byte) 1, itemStack.getDamage(), itemStack.getTagCompound()));
                player.getInventory().setItemInHand(newItemStack);
                break;
            case 5:
                //SHOOT ARROW / FINISH EATING
                break;
        }
    }

    @Override
    public void handleIncomingClientSettings(PacketInClientSettings clientSettings) {
        getServer().getListenerRegistry().callEvent(new PlayerClientSettingsUpdateEvent(player, clientSettings.getClientSettings()));

        player.setClientSettings(clientSettings.getClientSettings());
    }

    @Override
    public void handleOutgoingPlayerJoinGame(PacketOutJoinGame joinGame) {}

    @Override
    public void handleIncomingEntityAction(PacketInEntityAction entityAction) {
        boolean self = entityAction.getEntityId() == player.getEntityId();
        if(!self) return;

        switch (entityAction.getActionId()) {
            case 0:
                getServer().getListenerRegistry().callEvent(new PlayerToggleSneakingEvent(player, true));

                player.setSneaking(true);
                break;
            case 1:
                getServer().getListenerRegistry().callEvent(new PlayerToggleSneakingEvent(player, false));

                player.setSneaking(false);
                break;
            case 2:
                /* TODO: Leave bed */
                break;
            case 3:
                getServer().getListenerRegistry().callEvent(new PlayerToggleSprintingEvent(player, true));

                player.setSprinting(true);
                break;
            case 4:
                getServer().getListenerRegistry().callEvent(new PlayerToggleSprintingEvent(player, false));

                player.setSprinting(false);
                break;
            case 5:
                /* TODO: Jump with hose */
                /* Action Parameter Horse Jump Boost: 0 to 100 */
                break;
            case 6:
                /* TODO: Open ridden horse inventory */
                break;
        }
    }

    @Override
    public void handleOutgoingSpawnPosition(PacketOutSpawnPosition spawnPosition) { }

    @Override
    public void handleOutgoingServerDifficulty(PacketOutServerDifficulty serverDifficulty) { }

    @Override
    public void handleOutgoingCustomPayload(PacketOutCustomPayload pluginMessage) { }

    @Override
    public void handleIncomingCustomPayload(PacketInCustomPayload packetInPluginMessage) {
        /* TODO: Check the channel */

        String channel = packetInPluginMessage.getChannel();
        PacketBuffer data = packetInPluginMessage.getData();

        String dataString = data.readString();

        System.out.println("[IN] [" + channel + "] " + dataString);

        if(channel.equals("MC|Brand")){
            if(dataString.equals("fml,forge")){
                player.setForgeUser(true);
            }
        }
    }

    @Override
    public void handleOutgoingHeldItemChange(PacketOutHeldItemChange packetOutHeldItemChange) { }

    @Override
    public void handleIncomingHeldItemChange(PacketInHeldItemChange packetInHeldItemChange) {
        player.setHeldItemSlot(packetInHeldItemChange.getSlot(), false);
    }

    @Override
    public void handleOutgoingMapChunkBulk(PacketOutMapChunkBulk mapChunkBulk) { }

    @Override
    public void handleOutgoingChunkColumnData(PacketOutChunkColumnData packetOutChunkColumnData) { }

    @Override
    public void handleIncomingChatMessage(PacketInChatMessage chatMessage) {
        if(chatMessage.getMessage().length() > 100) {
            this.player.kickPlayer("Your chat message cannot be longer than 100 letters!");
            return;
        }

        String message = chatMessage.getMessage().trim();

        /* Command execution */
        if(message.charAt(0) == '/') {
            CommandRegistry commandRegistry = getServer().getCommandRegistry();

            message = message.substring(1);

            boolean successFound = commandRegistry.executeCommand(player, message);
            if(!successFound) {
                player.sendMessage(new TranslationComponent("commands.generic.notFound"));
            }
            return;
        }

        this.player.getServer().broadcast(player -> player.sendMessage(ChatMessageType.CHAT, this.player.getName() + ": " + chatMessage.getMessage()));
    }

    @Override
    public void handleOutgoingChatMessage(PacketOutChatMessage chatMessage) { }

    @Override
    public synchronized void handleIncomingPlayerPosLook(PacketInPlayerPosLook packet) {
        Location location = new Location(player.getWorld(), packet.getX(), packet.getY(), packet.getZ(), packet.getYaw(), packet.getPitch());
        /* TODO: Speed check */

        getServer().broadcast(player1 -> player1 != player, player1 -> {
            player1.sendPacket(new PacketOutEntityHeadRotation(player.getEntityId(), packet.getPitch()));
            player1.sendPacket(new PacketOutEntityLook(player.getEntityId(), packet.getYaw(), packet.getPitch(), packet.isOnGround()));
        });

        double deltaX = location.getX() - player.getPreviousLocation().getX();
        double deltaY = location.getY() - player.getPreviousLocation().getY();
        double deltaZ = location.getZ() - player.getPreviousLocation().getZ();

        t += deltaY;
        System.out.println(t + " » " + player.getName());

        player.setLocation(location);
        player.setOnGround(packet.isOnGround());

        player.setPreviousLocation(location);

        if(Math.abs(location.getX()) > 3.0E7D || Math.abs(location.getX()) > 3.0E7D) {
            player.kickPlayer("Illegal position");
            return;
        }

        boolean teleport = deltaX > Byte.MAX_VALUE || deltaY > Byte.MAX_VALUE || deltaZ > Byte.MAX_VALUE ||
                deltaX < Byte.MIN_VALUE || deltaY < Byte.MIN_VALUE || deltaZ < Byte.MIN_VALUE;

        getServer().broadcast(player1 -> player1 != player, player1 -> {
            if (teleport) {
                player1.sendPacket(new PacketOutEntityTeleport(player.getEntityId(), player.getLocation(), player.isOnGround()));
            } else {
                player1.sendPacket(new PacketOutEntityRelativeMove(player.getEntityId(), deltaX, deltaY, deltaZ, player.isOnGround()));
            }
            player.setPreviousLocation(location);
        });

        //tryToSendChunks(location, lastLocation);
    }

    private double t;

    @Override
    public synchronized void handleIncomingPlayerPosition(PacketInPlayerPosition packet) {
        Location location = new Location(player.getWorld(), packet.getX(), packet.getY(), packet.getZ(), player.getYaw(), player.getPitch());
        /* TODO: Speed check */

        double deltaX = location.getX() - player.getPreviousLocation().getX();
        double deltaY = location.getY() - player.getPreviousLocation().getY();
        double deltaZ = location.getZ() - player.getPreviousLocation().getZ();

        t += deltaY;

        System.out.println(t + " » " + player.getName());

        player.setLocation(location);
        player.setOnGround(packet.isOnGround());

        player.setPreviousLocation(location);

        if(Math.abs(location.getX()) > 3.0E7D || Math.abs(location.getX()) > 3.0E7D) {
            player.kickPlayer("Illegal position");
            return;
        }

        boolean teleport = deltaX > Byte.MAX_VALUE || deltaY > Byte.MAX_VALUE || deltaZ > Byte.MAX_VALUE ||
                deltaX < Byte.MIN_VALUE || deltaY < Byte.MIN_VALUE || deltaZ < Byte.MIN_VALUE;

        getServer().broadcast(player1 -> player1 != player, player1 -> {
                if (teleport) {
                    player1.sendPacket(new PacketOutEntityTeleport(player.getEntityId(), player.getLocation(), player.isOnGround()));
                } else {
                    player1.sendPacket(new PacketOutEntityRelativeMove(player.getEntityId(), deltaX, deltaY, deltaZ, player.isOnGround()));
                }
        });

        //tryToSendChunks(location, lastLocation);
    }

    public void movePlayer(double posX, double posY, double posZ, boolean ground) {

    }

    private void tryToSendChunks(Location location, Location lastLocation) {
        ChunkColumn column;
        if(lastLocation == null) {
            for(int chunkX = -8; chunkX != 8; chunkX++) {
                for(int chunkZ = -8; chunkZ != 8; chunkZ++) {
                    column = location.getWorld().getChunkProvider().provideChunkColumn(chunkX, chunkZ);
                    if(column == null) continue;
                    this.player.sendPacket(new PacketOutChunkColumnData(column, location.getWorld().getWorldType() == WorldType.OVERWORLD, true));
                }
            }
            return;
        }

        int lastXInsideOfChunk = lastLocation.getBlockX() % 16;
        int lastZInsideOfChunk = lastLocation.getBlockZ() % 16;

        int lastChunkX = (lastLocation.getBlockX() - lastXInsideOfChunk) / 16;
        int lastChunkZ = (lastLocation.getBlockZ() - lastZInsideOfChunk) / 16;

        player.setLocation(location);

        int newXInsideOfChunk = location.getBlockX() % 16;
        int newZInsideOfChunk = location.getBlockZ() % 16;

        int newChunkX = (location.getBlockX() - newXInsideOfChunk) / 16;
        int newChunkZ = (location.getBlockZ() - newZInsideOfChunk) / 16;

        if(lastChunkX == newChunkX && lastChunkZ == newChunkZ) return;

        int defChunkX = lastChunkX - newChunkX;
        int defChunkZ = lastChunkZ - newChunkZ;

        System.out.println("Def Chunk X: " + defChunkX);
        System.out.println("Def Chunk Z: " + defChunkZ);

        if(defChunkX > 1 || defChunkX < -1 || defChunkZ > 1 || defChunkZ < -1) {
            tryToSendChunks(location, null);
            return;
        }

        //UNLOADING WORKS!!! LOADING NEEDS TO BE FIXED.

        for(int x = lastChunkX-8; x < lastChunkX+8; x++) {
            for(int z = lastChunkZ-8; z < lastChunkZ+8; z++) {
                if(newChunkX-8 < x || newChunkX+8 < x || newChunkZ-8 < z || newChunkZ+8 < z) {
                    column = location.getWorld().getChunkProvider().provideChunkColumn(x, z);
                    //this.player.sendPacket(new PacketOutChunkColumnData(column, true, true));

                    //this.player.sendMessage("Loaded Chunk! X: " + x + " | Z: " + z);
                } else if(newChunkX-8 > x || newChunkX+8 > x || newChunkZ-8 > z || newChunkZ+8 > z) {
                    column = location.getWorld().getChunkProvider().provideChunkColumn(x, z);
                    this.player.sendPacket(new PacketOutChunkColumnData(column, true, false));

                    this.player.sendMessage("Unloaded Chunk! X: " + x + " | Z: " + z);
                }
            }
        }

        for(int x = newChunkX-8; x < newChunkX+8; x++) {
            for(int z = newChunkZ-8; z < newChunkZ+8; z++) {
                if(lastChunkX-8 < x || lastChunkX+8 < x || lastChunkZ-8 < z || lastChunkZ+8 < z) {
                    column = location.getWorld().getChunkProvider().provideChunkColumn(x, z);
                    this.player.sendPacket(new PacketOutChunkColumnData(column, true, true));

                    //this.player.sendMessage("Loaded Chunk! X: " + x + " | Z: " + z);
                } else if(lastChunkX-8 > x || lastChunkX+8 > x || lastChunkZ-8 > z || lastChunkZ+8 > z) {
                    column = location.getWorld().getChunkProvider().provideChunkColumn(x, z);
                    this.player.sendPacket(new PacketOutChunkColumnData(column, true, false));

                    this.player.sendMessage("Unloaded Chunk! X: " + x + " | Z: " + z);
                }
            }
        }
    }

    @Override
    public void handleIncomingPlayerLook(PacketInPlayerLook playerLook) {

        float yaw = playerLook.getYaw();
        float pitch = playerLook.getPitch();

        Location location = player.getLocation();

        location.setYaw(yaw);
        location.setPitch(pitch);
        player.setOnGround(playerLook.isOnGround());

        getServer().broadcast(player1 -> player1 != player, player1 -> {
            player1.sendPacket(new PacketOutEntityHeadRotation(player.getEntityId(), yaw));
            player1.sendPacket(new PacketOutEntityLook(player.getEntityId(), yaw, pitch, playerLook.isOnGround()));
        });

    }



    @Override
    public void handleIncomingResourcePackStatus(PacketInResourcePackStatus resourcePackStatus) {
        PlayerResourcePackStatusEvent playerResourcePackStatusEvent = new PlayerResourcePackStatusEvent(player, resourcePackStatus.getResult());

        getServer().getListenerRegistry().callEvent(playerResourcePackStatusEvent);
    }

    @Override
    public void handleOutgoingSendResourcePack(PacketOutSendResourcePack packetOutSendResourcePack) { }

    @Override
    public void handleIncomingAnimation(PacketInAnimation animation) {
        for(Player players : getServer().getOnlinePlayers()) {
            players.sendPacket(new PacketOutAnimation(player.getEntityId(), (byte) 2));
        }
    }

    @Override
    public void handleOutgoingAnimation(PacketOutAnimation animation) { }

    @Override
    public void handleOutgoingPlayerListHeaderAndFooter(PacketOutPlayerListHeaderAndFooter headerAndFooter) { }

    @Override
    public void handleOutgoingEntityStatus(PacketOutEntityStatus packetOutEntityStatus) { }

    @Override
    public void handleOutgoingEntitySpawn(PacketOutSpawnEntity packetOutSpawnEntity) { }

    @Override
    public void handleOutgoingPlayerListItem(PacketOutPlayerListItem packetOutPlayerListItem) { }

    @Override
    public void handleOutgoingPlayerSpawn(PacketOutSpawnPlayer packetOutSpawnPlayer) { }

    @Override
    public void handleIncomingBlockPlace(PacketInPlaceBlock placeBlock) {
        //TODO: Handle it

        if(placeBlock.getFacing() == null) {
            //Special Case: Update state (eating food, using buckets etc.)
            return;
        }

        //Normal Case: Placed Block

        ItemStack itemStack = placeBlock.getItemStack();
        Material material = itemStack.getMaterial();

        if(material != null) {
            World world = player.getWorld();
            placeBlock.getLocation().setWorld(world);

            System.out.println(itemStack);

            Facing facing = placeBlock.getFacing();

            Location location = placeBlock.getLocation();
            location.offset(facing);

            world.setBlockAtLocation(location, new AbstractBlock((byte)material.getId(), (byte)itemStack.getDamage()));

            player.getServer().broadcast(player1 -> {
                player1.sendPacket(new PacketOutBlockChange(location, material.getId(), itemStack.getDamage()));

                switch(material.getId()) {
                    case 1:
                    case 22:
                    case 41:
                    case 42:
                    case 57:
                    case 133:
                    case 152:
                        player1.sendPacket(new PacketOutSoundEffect("dig.stone", player.getLocation(), 90, 1));
                        break;
                    case 2:
                        player1.sendPacket(new PacketOutSoundEffect("dig.grass", player.getLocation(), 90, 1));
                        break;
                    case 3:
                        player1.sendPacket(new PacketOutSoundEffect("dig.gravel", player.getLocation(), 90, 1));
                        break;

                }
            });
        } else {
            //Clicked on Block

            Location location = placeBlock.getLocation();
            location.setWorld(player.getWorld());

            System.out.println(location.getBlock());

            AbstractBlock block = location.getBlock();
            if(location.getBlock().getBlockId() == (byte) 54 || location.getBlock().getBlockId() == (byte) 146) {
                ChestBlock chestBlock = (ChestBlock) block;
                chestBlock.setPlayersViewingInventory(chestBlock.getPlayersViewingInventory() + 1);

                player.getServer().broadcast(player1 -> {
                    player1.sendPacket(new PacketOutBlockAction(location, (byte) 1, (byte) chestBlock.getPlayersViewingInventory(), location.getBlock().getBlockId()));
                    player1.sendPacket(new PacketOutSoundEffect("random.chestopen", player.getLocation(), 90, 1));
                });
            }
        }
    }

    @Override
    public void handleOutgoingBlockChange(PacketOutBlockChange packetOutBlockChange) { }

    @Override
    public void handleOutgoingEntityTeleport(PacketOutEntityTeleport packetOutEntityTeleport) { }

    @Override
    public void handleOutgoingEntityRelativeMove(PacketOutEntityRelativeMove packetOutEntityRelativeMove) { }

    @Override
    public void handleIncomingTabCompleteRequest(PacketInTabCompleteRequest packetInTabCompleteRequest) {
        String message = packetInTabCompleteRequest.getText();

        String[] suggestions;

        if(message.charAt(0) == '/') {
            message = message.substring(1);

            CommandRegistry commandRegistry = getServer().getCommandRegistry();
            Collection<String> list = commandRegistry.executeTabComplete(this.player, message);
            if(list == null) list = getOnlinePlayers(this.player);
            suggestions = list.toArray(new String[0]);
        } else {
            suggestions = getOnlinePlayers(this.player).toArray(new String[0]);
        }

        PacketOutTabCompleteResponse packet = new PacketOutTabCompleteResponse(suggestions);
        player.sendPacket(packet);
    }

    private Collection<String> getOnlinePlayers(Player player) {
        Collection<String> players = new HashSet<>();
        for(Player online : getServer().getOnlinePlayers()) {
            // TODO: Check hidden
            players.add(online.getName());
        }
        return players;
    }

    @Override
    public void handleOutgoingTabCompleteResponse(PacketOutTabCompleteResponse packetOutTabCompleteResponse) { }

    @Override
    public void handleOutgoingGameStateChange(PacketOutGameStateChange packetOutGameStateChange) { }

    @Override
    public void handleIncomingCreativeInventoryAction(PacketInCreativeInventoryAction packetInCreativeInventoryAction) {
        short slot = packetInCreativeInventoryAction.getSlot();
        ItemStack itemStack = packetInCreativeInventoryAction.getItemStack();

        if(slot == -1) {
            Item item = player.getWorld().spawnEntityAtLocation(Item.class, player.getLocation());
            item.setItemStack(itemStack);
            return;
        }

        player.getInventory().setItem(slot, itemStack);
        player.sendPacket(new PacketOutWindowSetSlot((byte) 0, slot, itemStack));
    }

    @Override
    public void handleOutgoingPlayerAbilities(PacketOutPlayerAbilities packetOutPlayerAbilities) { }

    @Override
    public void handleIncomingPlayerAbilities(PacketInPlayerAbilities packetInPlayerAbilities) {
        PlayerAbilities playerAbilities = packetInPlayerAbilities.getPlayerAbilities();
        playerAbilities.setAllowFlying(player.getGameMode() == GameMode.CREATIVE);
        playerAbilities.setFlying(playerAbilities.isAllowFlying() && playerAbilities.isFlying());
        playerAbilities.setCreativeMode(player.getGameMode() == GameMode.CREATIVE);
        playerAbilities.setInvulnerable(false);

        /* TODO: Make a check for this */

        this.player.setPlayerAbilities(playerAbilities, false);
    }

    @Override
    public void handleIncomingEntityUse(PacketInUseEntity packetInUseEntity) {
        int target = packetInUseEntity.getTarget();
        int type = packetInUseEntity.getType();

        float x = packetInUseEntity.getX();
        float y = packetInUseEntity.getY();
        float z = packetInUseEntity.getZ();
    }

}
