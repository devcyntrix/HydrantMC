package org.hydrantmc.network.handler;

import io.netty.channel.ChannelFutureListener;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.event.server.ServerListPingEvent;
import org.hydrantmc.api.network.PendingConnection;
import org.hydrantmc.api.network.status.AbstractStatusPacketHandler;
import org.hydrantmc.api.network.status.in.PacketInPing;
import org.hydrantmc.api.network.status.in.PacketInStatusRequest;
import org.hydrantmc.api.network.status.out.PacketOutPong;
import org.hydrantmc.api.network.status.out.PacketOutStatusResponse;
import org.hydrantmc.api.utils.status.ServerStatus;

public class HydrantStatusPacketHandler extends AbstractStatusPacketHandler {

    private PendingConnection connection;

    public HydrantStatusPacketHandler(Server server) {
        super(server);
    }

    @Override
    public void register(PendingConnection connection) {
        this.connection = connection;
    }

    @Override
    public void unregister() { }

    @Override
    public void caughtException(Throwable cause) {
        cause.printStackTrace();
    }

    @Override
    public void handleIncomingStatusRequest(PacketInStatusRequest request) {

        ServerStatus status = getServer().getServerStatus();
        status.getServerPlayers().setOnlinePlayerCount(getServer().getOnlinePlayers().size());
        ServerListPingEvent event = new ServerListPingEvent(false, this.connection, status);
        getServer().getListenerRegistry().callEvent(event);

        connection.sendPacketAndFlush(new PacketOutStatusResponse(status));
    }

    @Override
    public void handleIncomingPing(PacketInPing ping) {
        this.connection.sendPacketAndFlush(new PacketOutPong(ping.getTime())).addListener(ChannelFutureListener.CLOSE);
    }
}
