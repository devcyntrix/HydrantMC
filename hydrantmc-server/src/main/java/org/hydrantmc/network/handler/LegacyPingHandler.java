package org.hydrantmc.network.handler;

import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import org.hydrantmc.api.Server;
import org.hydrantmc.api.event.server.ServerListPingEvent;
import org.hydrantmc.api.utils.status.OldClientRequestStatusVersion;
import org.hydrantmc.api.utils.status.ServerStatus;
import org.hydrantmc.network.HydrantPendingConnection;

public class LegacyPingHandler extends ChannelInboundHandlerAdapter {

    private final Server server;

    public LegacyPingHandler(Server server) {
        this.server = server;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object object) {
        ByteBuf byteBuf = (ByteBuf) object;
        byteBuf.markReaderIndex();

        boolean removeLegacyHandler = true;

        int id = byteBuf.readUnsignedByte();

        try {
            if (id == 254) {
                int readableBytes = byteBuf.readableBytes();

                ServerStatus status = this.server.getServerStatus();
                ServerListPingEvent event = new ServerListPingEvent(true, new HydrantPendingConnection(this.server, ctx.channel()), status);
                server.getListenerRegistry().callEvent(event);

                String packet;
                switch (readableBytes) {
                    case 0:
                        packet = status.toLegacyString(OldClientRequestStatusVersion.MINECRAFT_1_3);
                        ctx.writeAndFlush(toByteBuf(packet)).addListener(ChannelFutureListener.CLOSE);
                        break;
                    case 1:
                        if (byteBuf.readUnsignedByte() != 1) return;
                    default:
                        packet = status.toLegacyString(OldClientRequestStatusVersion.MINECRAFT_1_4_1_5_1_6);
                        ctx.writeAndFlush(toByteBuf(packet)).addListener(ChannelFutureListener.CLOSE);
                        break;
                }

                byteBuf.release();
                removeLegacyHandler = false;
            }
        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            if(removeLegacyHandler) {
                byteBuf.resetReaderIndex();
                ctx.pipeline().remove("legacy_query");
                ctx.fireChannelRead(byteBuf);
            }
        }
    }

    private ByteBuf toByteBuf(String message) {
        ByteBuf byteBuf = Unpooled.buffer();
        byteBuf.writeByte(255);

        char[] cs = message.toCharArray();
        byteBuf.writeShort(cs.length);

        for(char c : cs) {
            byteBuf.writeChar(c);
        }
        return byteBuf;
    }
}
