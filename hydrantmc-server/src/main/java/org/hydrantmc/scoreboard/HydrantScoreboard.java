package org.hydrantmc.scoreboard;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.network.play.out.scoreboard.PacketOutDisplayScoreboard;
import org.hydrantmc.api.scoreboard.Scoreboard;
import org.hydrantmc.api.scoreboard.ScoreboardDisplay;
import org.hydrantmc.api.scoreboard.ScoreboardObjective;
import org.hydrantmc.api.scoreboard.ScoreboardTeam;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class HydrantScoreboard implements Scoreboard {

    private final Map<String, ScoreboardObjective> objectives;
    private final Map<String, ScoreboardTeam> teams;

    HydrantScoreboard() {
        this.objectives = new HashMap<>();
        this.teams = new HashMap<>();
    }

    @Override
    public ScoreboardObjective registerNewObjective(String name) throws NullPointerException, IllegalArgumentException {
        Preconditions.checkNotNull(name);
        Preconditions.checkArgument(objectives.containsKey(name));

        return objectives.put(name, new HydrantScoreboardObjective(this, name));
    }

    @Override
    public ScoreboardObjective getObjective(String name) throws NullPointerException {
        Preconditions.checkNotNull(name);
        return objectives.get(name);
    }

    @Override
    public ScoreboardObjective getObjective(ScoreboardDisplay display) throws NullPointerException {
        Preconditions.checkNotNull(display);
        return null;
    }

    @Override
    public Set<ScoreboardObjective> getObjectives() {
        return ImmutableSet.copyOf(objectives.values());
    }

    @Override
    public ScoreboardTeam getTeam(String name) throws NullPointerException {
        Preconditions.checkNotNull(name);
        return teams.get(name);
    }

    @Override
    public Set<ScoreboardTeam> getTeams() {
        return ImmutableSet.copyOf(teams.values());
    }

    @Override
    public ScoreboardTeam registerNewTeam(String name) throws NullPointerException, IllegalArgumentException {
        Preconditions.checkNotNull(name);
        Preconditions.checkArgument(teams.containsKey(name));

        return teams.put(name, new HydrantScoreboardTeam(this, name));
    }

    @Override
    public void clearSlot(ScoreboardDisplay display) throws NullPointerException {

    }

    @Override
    public void sendToPlayer(Player player) {
        for(ScoreboardObjective objective : getObjectives()) {
            objective.sendToPlayer(player);
        }
    }
}
