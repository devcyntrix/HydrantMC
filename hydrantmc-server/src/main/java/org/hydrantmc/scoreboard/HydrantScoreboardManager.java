package org.hydrantmc.scoreboard;

import org.hydrantmc.api.scoreboard.Scoreboard;
import org.hydrantmc.api.scoreboard.ScoreboardManager;

public class HydrantScoreboardManager implements ScoreboardManager {

    private static final Scoreboard MAIN_SCOREBOARD = new HydrantScoreboard();

    @Override
    public Scoreboard getMainScoreboard() {
        return MAIN_SCOREBOARD;
    }

    @Override
    public Scoreboard getNewScoreboard() {
        return new HydrantScoreboard();
    }

}
