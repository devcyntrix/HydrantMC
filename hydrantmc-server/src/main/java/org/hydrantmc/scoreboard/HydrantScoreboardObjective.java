package org.hydrantmc.scoreboard;

import com.google.common.base.Preconditions;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.network.play.out.scoreboard.PacketOutDisplayScoreboard;
import org.hydrantmc.api.network.play.out.scoreboard.PacketOutScoreboardObjective;
import org.hydrantmc.api.scoreboard.Scoreboard;
import org.hydrantmc.api.scoreboard.ScoreboardDisplay;
import org.hydrantmc.api.scoreboard.ScoreboardObjective;
import org.hydrantmc.api.scoreboard.ScoreboardScore;

public class HydrantScoreboardObjective implements ScoreboardObjective {

    private final Scoreboard scoreboard;

    private final String name;

    private String displayName;
    private ScoreboardDisplay displaySlot;

    HydrantScoreboardObjective(Scoreboard scoreboard, String name) {
        this.scoreboard = scoreboard;
        this.name = name;
    }

    @Override
    public String getName() throws IllegalStateException {
        return name;
    }

    @Override
    public String getDisplayName() throws IllegalStateException {
        return displayName;
    }

    @Override
    public void setDisplayName(String displayName) throws IllegalStateException, NullPointerException, IllegalArgumentException {
        Preconditions.checkNotNull(displayName);
        Preconditions.checkArgument(displayName.length() > 32);

        this.displayName = displayName;
    }

    @Override
    public ScoreboardDisplay getDisplay() throws IllegalStateException {
        return displaySlot;
    }

    @Override
    public void setDisplay(ScoreboardDisplay display) throws IllegalStateException {
        this.displaySlot = display;
    }

    @Override
    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    @Override
    public void unregister() throws IllegalStateException {

    }

    @Override
    public ScoreboardScore getScore(String entry) throws IllegalArgumentException, IllegalStateException {
        Preconditions.checkNotNull(entry);
        return new HydrantScoreboardScore(this, entry);
    }

    @Override
    public void sendToPlayer(Player player) {
        player.sendPacket(new PacketOutDisplayScoreboard(getDisplay(), getName()));
    }

}
