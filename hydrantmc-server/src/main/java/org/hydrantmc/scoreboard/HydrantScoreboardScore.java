package org.hydrantmc.scoreboard;

import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.network.play.out.scoreboard.PacketOutScoreboardObjective;
import org.hydrantmc.api.scoreboard.Scoreboard;
import org.hydrantmc.api.scoreboard.ScoreboardObjective;
import org.hydrantmc.api.scoreboard.ScoreboardScore;

public class HydrantScoreboardScore implements ScoreboardScore {

    private final ScoreboardObjective objective;
    private final String entry;

    private int score;

    HydrantScoreboardScore(ScoreboardObjective objective, String entry) {
        this.objective = objective;
        this.entry = entry;
    }

    @Override
    public String getEntry() {
        return entry;
    }

    @Override
    public ScoreboardObjective getObjective() {
        return objective;
    }

    @Override
    public int getScore() throws IllegalStateException {
        return score;
    }

    @Override
    public void setScore(int score) throws IllegalStateException {
        this.score = score;
    }

    @Override
    public boolean isScoreSet() throws IllegalStateException {
        return false;
    }

    @Override
    public Scoreboard getScoreboard() {
        return objective.getScoreboard();
    }

    @Override
    public void sendToPlayer(Player player) {
    }

}
