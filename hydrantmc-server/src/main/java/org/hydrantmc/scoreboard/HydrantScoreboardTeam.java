package org.hydrantmc.scoreboard;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import org.hydrantmc.api.scoreboard.Scoreboard;
import org.hydrantmc.api.scoreboard.ScoreboardTeam;

import java.util.HashSet;
import java.util.Set;

public class HydrantScoreboardTeam implements ScoreboardTeam {

    private final Scoreboard scoreboard;
    private final String name;
    private final Set<String> entries;

    private String displayName;
    private String prefix;
    private String suffix;

    HydrantScoreboardTeam(Scoreboard scoreboard, String name) {
        this.scoreboard = scoreboard;
        this.name = name;
        this.entries = new HashSet<>();
    }

    @Override
    public String getName() throws IllegalStateException {
        return name;
    }

    @Override
    public String getDisplayName() throws IllegalStateException {
        return displayName;
    }

    @Override
    public void setDisplayName(String displayName) throws IllegalStateException, IllegalArgumentException {
        Preconditions.checkArgument(displayName.length() > 32);

        this.displayName = displayName;
    }

    @Override
    public String getPrefix() throws IllegalStateException {
        return prefix;
    }

    @Override
    public void setPrefix(String prefix) throws NullPointerException, IllegalStateException, IllegalArgumentException {
        Preconditions.checkNotNull(prefix);
        Preconditions.checkArgument(prefix.length() > 16);

        this.prefix = prefix;
    }

    @Override
    public String getSuffix() throws IllegalStateException {
        return suffix;
    }

    @Override
    public void setSuffix(String suffix) throws NullPointerException, IllegalStateException, IllegalArgumentException {
        Preconditions.checkNotNull(suffix);
        Preconditions.checkArgument(suffix.length() > 16);

        this.suffix = suffix;
    }

    @Override
    public Set<String> getEntries() throws IllegalStateException {
        return ImmutableSet.copyOf(entries);
    }

    @Override
    public int getSize() throws IllegalStateException {
        return entries.size();
    }

    @Override
    public Scoreboard getScoreboard() {
        return scoreboard;
    }

    @Override
    public void addEntry(String entry) throws NullPointerException, IllegalStateException {
        Preconditions.checkNotNull(entry);

        entries.add(entry);
    }

    @Override
    public boolean removeEntry(String entry) throws NullPointerException, IllegalStateException {
        Preconditions.checkNotNull(entry);

        return entries.remove(entry);
    }

    @Override
    public void unregister() throws IllegalStateException {

    }

    @Override
    public boolean hasEntry(String entry) throws NullPointerException, IllegalStateException {
        Preconditions.checkNotNull(entry);

        return entries.contains(entry);
    }

}
