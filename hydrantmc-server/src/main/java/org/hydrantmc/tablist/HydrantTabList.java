package org.hydrantmc.tablist;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.network.play.out.PacketOutPlayerListItem;
import org.hydrantmc.api.tablist.AbstractTabList;
import org.hydrantmc.api.tablist.TabListItem;

public class HydrantTabList extends AbstractTabList {

    private final Server server;

    public HydrantTabList(Server server) {
        this.server = server;
    }

    @Override
    public void onPreInsertItems(TabListItem... items) {
        for(Player player : this.server.getOnlinePlayers()) {
            player.sendPacket(new PacketOutPlayerListItem(PacketOutPlayerListItem.Action.ADD_PLAYER, items));
        }
    }

    @Override
    public void onPlayerJoin(Player player) {
        player.sendPacket(new PacketOutPlayerListItem(PacketOutPlayerListItem.Action.ADD_PLAYER, getItems().toArray(new TabListItem[0])));
        insertPlayer(player);
    }

    @Override
    public void onPostInsertItems(TabListItem... items) {
    }

    @Override
    public void onPreRemoveItems(TabListItem... items) {
        for(Player player : this.server.getOnlinePlayers()) {
            player.sendPacket(new PacketOutPlayerListItem(PacketOutPlayerListItem.Action.REMOVE_PLAYER, items));
        }
    }

    @Override
    public void onPostRemoveItems(TabListItem... items) {

    }
}
