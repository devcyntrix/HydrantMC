package org.hydrantmc.world;

import org.hydrantmc.api.Server;
import org.hydrantmc.api.block.AbstractBlock;
import org.hydrantmc.api.entity.Entity;
import org.hydrantmc.api.entity.Item;
import org.hydrantmc.api.entity.Player;
import org.hydrantmc.api.network.OutgoingPacket;
import org.hydrantmc.api.network.play.out.PacketOutSpawnObject;
import org.hydrantmc.api.utils.Location;
import org.hydrantmc.api.world.Difficulty;
import org.hydrantmc.api.world.TerrainType;
import org.hydrantmc.api.world.World;
import org.hydrantmc.api.world.WorldType;
import org.hydrantmc.api.world.chunk.Chunk;
import org.hydrantmc.api.world.chunk.ChunkGenerator;
import org.hydrantmc.api.world.chunk.ChunkProvider;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class HydrantWorld implements World {

    private final Server server;
    private final String name;
    private Difficulty difficulty;
    private WorldType worldType;

    private final List<Entity> entities;
    private final ChunkProvider chunkProvider;

    public HydrantWorld(Server server, String name, Difficulty difficulty, WorldType worldType, long seed, ChunkGenerator generator) {
        this.server = server;
        this.name = name;
        this.difficulty = difficulty;
        this.worldType = worldType;

        this.entities = new ArrayList<>();
        this.chunkProvider = new ChunkProvider(this, seed, generator);
    }

    public HydrantWorld(Server server, String name, Difficulty difficulty, WorldType worldType, long seed, TerrainType terrainType) {
        this(server, name, difficulty, worldType, seed, terrainType.getChunkGenerator());
    }

    @Override
    public void pulse() {
        for(Entity entity : this.entities) {
            entity.pulse();
        }
    }

    @Override
    public Server getServer() {
        return server;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public Difficulty getDifficulty() {
        return difficulty;
    }

    @Override
    public void setDifficulty(Difficulty difficulty) {
        this.difficulty = difficulty;
    }

    @Override
    public WorldType getWorldType() {
        return worldType;
    }

    @Override
    public void setWorldType(WorldType worldType) {
        this.worldType = worldType;
    }

    @Override
    public ChunkProvider getChunkProvider() {
        return chunkProvider;
    }

    public static int floor(double num) {
        final int floor = (int) num;
        return floor == num ? floor : floor - (int) (Double.doubleToRawLongBits(num) >>> 63);
    }

    @Override
    public AbstractBlock getBlockAtLocation(Location location) {
        int blockX = floor(location.getX());
        int blockY = (int) location.getY();
        int blockZ = floor(location.getZ());

        int chunkX = blockX >> 4;
        int chunkY = blockY >> 4;
        int chunkZ = blockZ >> 4;

        Chunk chunk = this.chunkProvider.provideChunk(chunkX, chunkY, chunkZ);

        return chunk.getBlock((byte) (blockX & 0xF), (byte) (blockY & 0xF), (byte) (blockZ & 0xF));
    }

    @Override
    public void setBlockAtLocation(Location location, AbstractBlock block) {
        int blockX = floor(location.getX());
        int blockY = (int) location.getY();
        int blockZ = floor(location.getZ());

        int chunkX = blockX >> 4;
        int chunkY = blockY >> 4;
        int chunkZ = blockZ >> 4;

        Chunk chunk = this.chunkProvider.provideChunk(chunkX, chunkY, chunkZ);

        chunk.setBlock((byte) (blockX & 0xF), (byte) (blockY & 0xF), (byte) (blockZ & 0xF), block);
    }

    @Override
    public Chunk getChunkAtLocation(Location location) {
        return location.getChunk();
    }

    @Override
    public <T extends Entity> T spawnEntityAtLocation(Class<T> entityClass, Location location) {
        //TODO: Optimize this (Add EntityType etc)

        Entity entity = createEntity(entityClass, location);

        entities.add(entity);

        OutgoingPacket packet = null;

        if(entity instanceof Item) {
            packet = new PacketOutSpawnObject(entity.getEntityId(), (byte) 1, entity.getLocation());
        }

        final OutgoingPacket finalPacket = packet;
        getServer().broadcast(player -> player.sendPacket(finalPacket));

        return (T) entity;
    }

    private Entity createEntity(Class<? extends Entity> entityClass, Location location) {
        try {
            return entityClass.getConstructor(Server.class, Location.class).newInstance(getServer(), location);
        } catch(NoSuchMethodException | IllegalAccessException | InstantiationException | InvocationTargetException exception) {
            return null;
        }
    }

    @Override
    public List<Entity> getEntities() {
        return entities;
    }

}
